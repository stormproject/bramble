macro_rules! is_variant {
    ($e:expr, $p:path) => {
        match $e {
            $p => true,
            _ => false,
        }
    };
}
macro_rules! is_tup_variant {
    ($e:expr, $p:path) => {
        match $e {
            $p(_) => true,
            _ => false,
        }
    };
}
macro_rules! extract_variant {
    ($e:expr, $p:path) => {
        match $e {
            $p(value) => value,
            _ => panic!("expected {}", stringify!($p)),
        }
    };
}
macro_rules! null_if {
    ($e:expr, $f:expr) => {
        match $e {
            Some(value) => value,
            None => $f,
        }
    };
}

macro_rules! storage_op {
    ($sync:expr, $operation:path, $($arg:tt)+) => {
        {
        use StorageOperation::*;
        let (send, recv) = channel();
        $sync.borrow_mut().send_db_req($operation($($arg)+, send)).unwrap();
        recv.await.unwrap()
        }
    };
    ($sync:expr, $operation:path) => {
        {
        use StorageOperation::*;
        let (send, recv) = channel();
        $sync.borrow_mut().send_db_req( $operation(send)).unwrap();
        recv.await.unwrap()
        }
    };
}

macro_rules! send_client_req {
    ($sync:expr, $client:expr, $operation:path, $($arg:tt)+) => {
        {
        use ClientRequest::*;
        let (send, recv) = channel();
        $sync.borrow_mut().send_client_request($client, $operation($($arg)+, send));
        recv.await.unwrap()
        }
    }
}
// macro_rules! send_client_output {
//     ($sync:expr, $client:expr, $operation:path, $($arg:tt)+) => {
//         {
//         use self::SyncOutput::*;
//         use ClientInput::*;
//         let (send, recv) = channel();
//         $sync.borrow_mut().send_client_sync_output($client,SyncOutput($operation($($arg)+)));
//         recv.await
//         }
//     }
// }
macro_rules! test_storage_op {
    ($storage:expr, $operation:path, $($arg:tt)+) => {
        {
        use StorageOperation::*;
        let (send, recv) = channel();
        $storage.sender.unbounded_send($operation($($arg)+, send)).unwrap();
        executor::block_on(recv).unwrap()
        }
    };
    ($storage:expr, $operation:path) => {
        {
        use StorageOperation::*;
        let (send, recv) = channel();
        $storage.sender.unbounded_send($operation(send)).unwrap();
        executor::block_on(recv).unwrap()
        }
    };
}
