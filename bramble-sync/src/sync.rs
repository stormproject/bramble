use crate::storage::*;
use async_recursion::async_recursion;
use bisetmap::BisetMap;
use bramble_common::transport::Latency;
use bramble_common::Error;
use bramble_common::Result;
use bramble_crypto::PublicKey;
use bramble_crypto::{hash, Hash as ID, HASH_LEN};
use broadcaster::BroadcastChannel;
use futures::channel::mpsc::unbounded;
use futures::channel::mpsc::{TrySendError, UnboundedReceiver, UnboundedSender};
use futures::channel::oneshot::Sender;
use futures::channel::oneshot::{channel, Receiver};
use futures::future::Fuse;
use futures::future::FutureExt;
use futures::future::LocalBoxFuture;
use futures::io::AsyncReadExt;
use futures::io::AsyncWriteExt;
use futures::io::{ReadHalf, WriteHalf};
use futures::stream::Peekable;
use futures::Future;
use futures::StreamExt;
use futures::{AsyncRead, AsyncWrite};
use futures_lite::Stream;
use futures_timer::Delay;
use futures_util::future::join_all;
use futures_util::future::FusedFuture;
use futures_util::select;
use futures_util::stream::FuturesUnordered;
use futures_util::stream::Next;
use indexmap::set::IndexSet;
use log::{debug, error, trace, warn};
use mapped_futures::mapped_futures::BiMultiMapFutures;
use mapped_futures::mapped_futures::MappedFutures;
use partial_borrow::prelude::*;
use slice_as_array::{slice_as_array, slice_as_array_transmute};
use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt::{self, Display};
use std::hash::{Hash, Hasher};
use std::ops::Deref;
use std::ops::DerefMut;
use std::pin::Pin;
use std::rc::Rc;
use std::sync::Arc;
use std::task::ready;
use std::task::Context;
use std::task::Poll;
use std::task::Waker;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use std::u8;

fn next_if<'a, S: Stream + std::marker::Unpin>(
    condition: bool,
    stream: &'a mut S,
) -> Fuse<Next<'a, S>> {
    if condition {
        stream.next().fuse()
    } else {
        Fuse::terminated()
    }
}

fn next_if_not_empty<'a, F: Future>(
    stream: &'a mut FuturesUnordered<F>,
) -> Fuse<Next<'a, FuturesUnordered<F>>> {
    if stream.is_empty() {
        Fuse::terminated()
    } else {
        stream.next().fuse()
    }
}

pub fn time_since_epoch() -> Duration {
    SystemTime::now().duration_since(UNIX_EPOCH).unwrap()
}
pub fn secs_since_epoch() -> u64 {
    time_since_epoch().as_secs()
}

#[derive(Hash, PartialEq, Eq, Clone, Debug)]
pub struct ClientId {
    pub identifier: String,
    pub version: u32,
}

impl Display for ClientId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.identifier)
    }
}

impl ClientId {
    pub fn new(identifier: String, version: u32) -> Self {
        Self {
            identifier,
            version,
        }
    }
}

const GROUP_FORMAT_VERSION: u8 = 1;

#[derive(Debug)]
pub struct Group {
    pub id: ID,
    pub descriptor: Vec<u8>,
    pub client: ClientId,
    // storing what peers the device is sharing the group with will be handled by the client
}

impl Group {
    pub fn new(descriptor: &[u8], client: ClientId) -> Self {
        let id_hasher = hash(
            b"org.briarproject.bramble/group",
            &[
                &[GROUP_FORMAT_VERSION],
                client.identifier.as_bytes(),
                &client.version.to_be_bytes(),
                &descriptor[..],
            ],
        );
        Group {
            id: (*slice_as_array!(id_hasher.as_ref(), [u8; 32]).unwrap()).into(),
            descriptor: descriptor.to_vec(),
            client,
        }
    }
}

#[derive(Debug)]
pub enum MessageDestiny {
    Share,
    Delete,
    Neither,
}

#[derive(Debug)]
pub struct Message {
    // TODO: Separate this struct into metadata and data, so that just the sharing/delivered status
    // can be retrieved without getting the body
    pub id: ID,
    pub body: Vec<u8>,
    pub time: Duration,
    pub group: ID,
    pub sharing: Option<bool>,
    pub delivered: bool,
}

#[derive(Clone)]
pub struct ClientMessage {
    pub message: Arc<Message>,
    pub client: ClientId,
}

impl core::fmt::Debug for ClientMessage {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        std::fmt::Debug::fmt(&self.message, f)
    }
}

impl Display for ClientMessage {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.message.fmt(f)
    }
}

impl ClientMessage {
    pub fn new(message: Arc<Message>, client: ClientId) -> Self {
        Self { message, client }
    }

    fn msg(&self) -> Arc<Message> {
        (**self).clone()
    }
}

impl Deref for ClientMessage {
    type Target = Arc<Message>;

    fn deref(&self) -> &Self::Target {
        &self.message
    }
}

impl Hash for Message {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state)
    }
}

impl PartialEq for Message {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for Message {}

impl Message {
    fn new_from_client(body: Vec<u8>, group: ID) -> Arc<Message> {
        let time = time_since_epoch();
        let id = Message::calc_id(&group, &time.as_secs().to_be_bytes(), &body[..]);
        Arc::new(Message {
            group,
            time,
            body,
            id,
            sharing: Some(false),
            delivered: false,
        })
    }
    pub fn calc_id(group: &ID, time: &[u8], body: &[u8]) -> ID {
        let body_hash = hash(b"org.briarproject.bramble/MESSAGE_BLOCK", &[&[1], body]);
        let id_hasher = hash(
            b"org.briarproject.bramble/MESSAGE_ID",
            &[&[1], group.as_ref(), time, body_hash.as_ref()],
        );
        let id = (*slice_as_array!(id_hasher.as_ref(), [u8; 32]).unwrap()).into();
        id
    }
    fn new_from_peer(data: &[u8]) -> Arc<Message> {
        //TODO: Error handling
        let group_data = &data[0..32];
        let time_data = &data[32..40];
        let body_data = &data[40..];
        let group = (*slice_as_array!(group_data, [u8; HASH_LEN]).unwrap()).into();
        let id = Message::calc_id(&group, time_data, body_data);
        let time = Duration::from_secs(u64::from_be_bytes(
            *slice_as_array!(time_data, [u8; 8]).unwrap(),
        ));
        Arc::new(Message {
            group,
            time,
            body: body_data.to_vec(),
            id,
            sharing: None,
            delivered: false,
        })
    }

    fn to_bytes(&self) -> Vec<u8> {
        let mut buf = vec![];
        buf.extend_from_slice(self.group.as_ref());
        buf.extend_from_slice(&self.time.as_secs().to_be_bytes());
        buf.extend_from_slice(&self.body[..]);
        buf
    }

    fn size(&self) -> u16 {
        let base = HASH_LEN as u16 + 8; // length of groupID plus timestamp (u64)
        base + self.body.len() as u16
    }
}

impl Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Message {}, body: {:?}, time: {:?}, group: {}",
            self.id, self.body, self.time, self.group
        )
    }
}

// #[async_trait::async_trait]
pub trait Client {
    type Message: From<Arc<Message>>;
    type Group: Into<Group> + From<Arc<Group>>;
    fn parse_dependencies(&self, msg: &Arc<Message>) -> Result<Vec<ID>>;
    // async fn parse_dependencies(&self, msg: Arc<Message>) -> Result<Vec<ID>>;
    fn get_sharing_peers(&self, group: &ID) -> Vec<PublicKey>;
    fn get_sharing_groups(&self, peer: &PublicKey) -> Vec<ID>;
    fn is_sharing_peer_with_group(&self, peer: &PublicKey, group: &ID) -> bool;
    fn add_peer_to_group(&mut self, peer: &PublicKey, group: &ID);
    fn remove_peer_from_group(&mut self, peer: &PublicKey, group: &ID);
    fn get_message_destiny(&self, msg: &Arc<Message>) -> MessageDestiny;
    fn validate_message(&self, msg: &Arc<Message>) -> bramble_common::Result<()>;
    // fn get_id(&self) -> ClientId;
    fn get_id() -> ClientId;
    // fn static_get_id() -> ClientId;
}

#[derive(Debug)]
pub enum ClientInput {
    Request(ClientRequest),
    Output(SyncOutput),
}

#[derive(Debug)]
pub enum SyncOutput {
    Group(Arc<Group>),
    Message(Arc<Message>),
}

#[derive(Debug)]
pub enum ClientRequest {
    ParseDependencies(Arc<Message>, Sender<bramble_common::Result<Vec<ID>>>),
    GetMessageDestiny(Arc<Message>, Sender<MessageDestiny>),
    Deliver(Arc<Message>, Sender<bramble_common::Result<()>>),
    GetSharingGroups(PublicKey, Sender<Vec<ID>>),
    GetSharingPeers(ID, Sender<Vec<PublicKey>>),
    IsSharingPeerGroup(PublicKey, ID, Sender<bool>),
}

pub struct SyncClient<C: Client> {
    client: Box<C>,
    sender: UnboundedSender<ClientCommand>,
    receiver: Peekable<UnboundedReceiver<ClientInput>>,
    cmd_sent_callback: Option<Box<dyn Fn(ClientId)>>,
    creating_group_queued_messages: HashMap<ID, Vec<String>>,
    // parsing: FuturesUnordered<LocalBoxFuture<'static, ()>>,
}

impl<C: Client> Deref for SyncClient<C> {
    type Target = C;
    fn deref(&self) -> &Self::Target {
        &self.client
    }
}

impl<C: Client> DerefMut for SyncClient<C> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.client
    }
}

impl<C: Client> SyncClient<C> {
    // Idea for async group creation
    //      Either succeed immediately, because group creation can never fail, but keep a list of
    //      messages for that group that will only be sent to sync once we know that the group has been
    //      created
    // Or
    //      groups creation returns a future that completes when we know the group is created
    //      still need a message list, because other output may have been received since the group
    //      creation was started
    // Think I prefer first option. It is clients responsibility to know what groups exist
    fn new(
        client: C,
        sender: UnboundedSender<ClientCommand>,
        receiver: UnboundedReceiver<ClientInput>,
    ) -> Self {
        Self {
            client: Box::new(client),
            sender,
            receiver: receiver.peekable(),
            cmd_sent_callback: None,
            creating_group_queued_messages: HashMap::new(),
            // parsing: FuturesUnordered::new(),
        }
    }
    fn process_input(mut self: std::pin::Pin<&mut Self>, input: ClientInput) -> Option<C::Message> {
        trace!(target: "client", "Processing Client Input: {:?}", &input);

        use ClientInput::*;
        use ClientRequest::*;
        match input {
            Request(request) => match request {
                ParseDependencies(msg, sender) => {
                    // self.parsing
                    //     .push(Box::pin(self.parse_dependencies(msg).map(|parsed| {
                    //         sender.send(parsed).unwrap();
                    //     })));
                    sender.send(self.parse_dependencies(&msg)).unwrap();
                }
                GetMessageDestiny(msg, sender) => {
                    sender.send(self.get_message_destiny(&msg)).unwrap();
                }
                Deliver(msg, sender) => match self.validate_message(&msg) {
                    Ok(_) => {
                        sender.send(Ok(())).unwrap();
                        return Some(msg.into());
                    }
                    Err(e) => {
                        sender.send(Err(e)).unwrap();
                    }
                },
                GetSharingGroups(key, sender) => {
                    sender.send(self.get_sharing_groups(&key)).unwrap();
                }
                GetSharingPeers(id, sender) => {
                    sender.send(self.get_sharing_peers(&id)).unwrap();
                }
                IsSharingPeerGroup(key, id, sender) => {
                    sender
                        .send(self.is_sharing_peer_with_group(&key, &id))
                        .unwrap();
                }
            },
            Output(output) => match output {
                SyncOutput::Group(group) => {
                    if let Some(queued_messages) =
                        self.creating_group_queued_messages.remove(&group.id)
                    {
                        queued_messages
                            .into_iter()
                            .for_each(|msg| self.send_message(group.id, msg));
                    }
                }
                SyncOutput::Message(msg) => return Some(msg.into()),
            },
        };
        None
    }
    pub fn done(&mut self) {
        self.command(Command::Done);
        self.sender.close_channel();
    }
    pub fn add_group(&mut self, client_group: C::Group) {
        let group: Group = client_group.into();
        self.command(Command::NewGroup(group.descriptor));
        self.creating_group_queued_messages.insert(group.id, vec![]);
    }
    pub fn group_from_name(&self, name: &str) -> Group {
        let bytes = name.as_bytes();
        Group::new(bytes, C::get_id())
    }
    pub fn send_message(&mut self, group: ID, body: String) {
        if let Some(queue) = self.creating_group_queued_messages.get_mut(&group) {
            queue.push(body);
        } else {
            self.command(Command::NewMessage(body.as_bytes().to_vec(), group));
        }
    }
    pub fn add_peer_to_group(&mut self, peer: &PublicKey, group: &ID) {
        C::add_peer_to_group(self, peer, group);
        self.command(Command::StartSharingGroup(*peer, *group));
    }
    pub fn remove_peer_from_group(&mut self, peer: &PublicKey, group: &ID) {
        C::remove_peer_from_group(self, peer, group);
        self.command(Command::StopSharingGroup(*peer, *group));
    }
    pub fn create_group(&mut self, desc: String) {
        self.command(Command::NewGroup(desc.into_bytes()));
    }
    fn command(&mut self, command: Command) {
        let client = C::get_id();
        if let Some(callback) = &self.cmd_sent_callback {
            callback(client.clone());
        }
        self.sender
            .unbounded_send(ClientCommand { client, command })
            .unwrap();
    }
    pub fn register_cmd_sent_callback(&mut self, callback: Box<dyn Fn(ClientId)>) {
        self.cmd_sent_callback = Some(callback);
    }
    pub fn is_active(&mut self) -> bool {
        Pin::new(&mut self.receiver).peek().now_or_never().is_some()
    }
}

impl<C: Client> Stream for SyncClient<C> {
    type Item = C::Message;
    fn poll_next(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Option<Self::Item>> {
        // self.parsing.poll_next_unpin(cx);
        match unsafe {
            self.as_mut()
                .map_unchecked_mut(|client| &mut client.receiver)
        }
        .poll_next(cx)
        {
            Poll::Ready(Some(input)) => match self.process_input(input) {
                None => Poll::Pending,
                Some(output) => Poll::Ready(Some(output)),
            },
            Poll::Ready(None) => Poll::Ready(None),
            Poll::Pending => Poll::Pending,
        }
    }
}

pub enum Mode {
    Interactive,
    Batch,
    Eager,
}

#[derive(Debug, Clone, Copy)]
pub struct SyncState {
    pub message: ID,
    pub peer: PublicKey,
    pub seen: bool,      // The peer has seen this message
    pub ack: bool,       // We need to inform the peer that we have this message
    pub requested: bool, // The peer has requested this message and we have not sent it yet
    pub send_count: i32,
    pub send_time: u64,
    pub max_latency: u32,
}

impl SyncState {
    pub fn new(msg: &ID, peer: &PublicKey) -> Self {
        SyncState {
            message: *msg,
            peer: *peer,
            seen: false,
            ack: false,
            requested: false,
            send_count: 0,
            send_time: 0,
            max_latency: 0,
        }
    }

    pub fn get_key(&self) -> (PublicKey, ID) {
        (self.peer, self.message)
    }

    fn inc_send_count_and_time(&mut self, base: f64) {
        // proof of correctness:
        // Dt_x = base^x; Dt = t_x - t_0 = base^x
        // t_n - t_0 = base^n
        // t_n+1 - t_0 = base^(n+1)
        // t_n - base^n = t_n+1 - base^(n+1)
        // t_n+1 = t_n - base^n + base^(n+1)

        let n = self.send_count;
        let time_n;
        if n == 0 {
            time_n = secs_since_epoch();
        } else {
            time_n = self.send_time;
        }
        let m = n + 1;
        let time_m = time_n + (base.powi(m) - base.powi(n)) as u64;

        self.send_count = m;
        self.send_time = time_m;
    }

    fn update_expected_arrival(&mut self, latency: u32) {
        self.max_latency = latency;
    }

    pub fn update_send_data(&mut self, base: f64, latency: u32) {
        self.inc_send_count_and_time(base);
        self.update_expected_arrival(latency);
    }

    pub fn is_ready(&self, latency: u32) -> bool {
        time_since_epoch() >= self.next_ready(latency)
    }

    pub fn next_ready(&self, latency: u32) -> Duration {
        // This seems bizarre to me, but this is what the spec calls for.
        //
        // Ready now if latency is less than on the previously used transport, or if send_time has
        // passed
        if latency < self.max_latency {
            time_since_epoch()
        } else {
            Duration::from_secs(self.send_time)
        }
    }

    fn wait_for_ready(&self, latency: u32) -> Fuse<Delay> {
        let next_ready = self.next_ready(latency);
        let now = time_since_epoch();
        if next_ready <= now {
            Delay::new(Duration::from_secs(0)).fuse()
        } else {
            Delay::new(next_ready - now).fuse()
        }
    }
}

pub type SyncStateKey = (PublicKey, ID);
impl From<SyncState> for SyncStateKey {
    fn from(value: SyncState) -> Self {
        (value.peer, value.message)
    }
}

impl Display for SyncState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut string = format!("SyncState msg {}, peer {}", &self.message, &self.peer);
        if self.ack {
            string.push_str(", ack");
        }
        if self.requested {
            string.push_str(", reqd");
        }
        if self.seen {
            string.push_str(", seen");
        }
        let now = secs_since_epoch();
        if self.send_time <= now {
            string.push_str(&", ready to send".to_string());
        } else {
            string.push_str(&format!(
                ", next send in {} secs",
                self.send_time - secs_since_epoch()
            ));
        }
        write!(f, "{}", &string)
    }
}

impl PartialEq for SyncState {
    fn eq(&self, other: &Self) -> bool {
        self.message == other.message && self.peer == other.peer
    }
}

impl Eq for SyncState {}
impl Hash for SyncState {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.message.hash(state);
        self.peer.hash(state);
    }
}

pub enum SyncMode {
    Interactive,
    Batch,
    Eager,
}

pub enum ProtocolState {
    Active,
    AwaitingCommands,
}

pub enum ControllerNotification {
    Disconnected(PublicKey),
}

#[derive(Debug)]
pub struct ClientCommand {
    pub client: ClientId,
    pub command: Command,
}
#[derive(Debug)]
pub enum Command {
    NewMessage(Vec<u8>, ID),
    NewGroup(Vec<u8>),
    // TODO: Include a boolean to indicate whether dependents should also be deleted or should
    // simply not be shared
    DeleteMessage(ID),
    // TODO: Implement the following commands
    DeleteGroup(ID),
    StartSharingGroup(PublicKey, ID),
    StopSharingGroup(PublicKey, ID),
    Done,
}

// fn peer_label(name: &Option<String>, key: &PublicKey) -> String {
//     name.clone().unwrap_or(format!("{}", key))
// }

async fn read_one_record(mut conn: ReadConnection) -> (ReadConnection, Result<usize>) {
    // read the header (4 bytes)
    // from header, find payload length (u16 of 3rd and 4th bytes)
    // read to end of payload
    // trace!(target: "sync_io", "{} reading record header from {}", conn.device ,peer_label(&conn.peer_name, &conn.key));
    trace!(target: "sync_io", "{} reading record header from {}", conn.device ,&conn.peer_name);
    let mut header = vec![0; 4];
    let res = conn.conn.read_exact(&mut header).await;
    trace!(target: "sync_io", "{} read record header from {}", conn.device, &conn.peer_name);
    let res = match res {
        // Will always have read 4 bytes, unless EOF error
        Ok(_) => {
            let payload_size: u16 =
                u16::from_be_bytes(*slice_as_array!(&header[2..4], [u8; 2]).unwrap());
            let record_size: usize = payload_size as usize + 4;
            conn.buff = vec![0; record_size];
            conn.buff[..4].copy_from_slice(&header);
            trace!(target: "sync_io", "{} reading record from {}",conn.device, &conn.peer_name );
            let res = conn.conn.read_exact(&mut conn.buff[4..]).await;

            trace!(target: "sync_io", "{} read record from {}", conn.device, &conn.peer_name);
            match res {
                // Will have read payload_size + 4 bytes
                Ok(_) => Ok(record_size),
                Err(e) => Err(bramble_common::Error::Io(e)),
            }
        }
        Err(e) => {
            if e.kind() == std::io::ErrorKind::UnexpectedEof {
                error!(target: "sync_io", "UnexpectedEof in {} reading from {}",conn.device ,&conn.peer_name);
                Ok(0)
            } else {
                Err(bramble_common::Error::Io(e))
            }
        }
    };
    (conn, res)
}

async fn write_one_record(
    mut conn: WriteConnection,
    record: Record,
) -> (WriteConnection, Result<Record>) {
    trace!(target: "sync_io", "{} writing record to {}. Record: {}", conn.device, &conn.peer_name, record.short_desc());
    conn.buff = record.to_buffer();
    let res = conn.conn.write_all(&conn.buff[..]).await;
    let flush_res = conn.conn.flush().await;
    trace!(target: "sync_io", "{} done writing record to {}", conn.device, &conn.peer_name);
    let res = match (res, flush_res) {
        (Ok(_), Ok(_)) => Ok(record),
        (Err(e), _) | (_, Err(e)) => Err(bramble_common::Error::Io(e)),
    };
    (conn, res)
}

#[derive(Debug)]
enum RecordPayload {
    Ack(Vec<ID>),
    Message(Arc<Message>),
    Offer(Vec<ID>),
    Request(Vec<ID>),
    Versions(Vec<u8>),
}

pub type Connection = Pin<Box<dyn SyncConnection>>;

struct ReadConnection {
    conn: ReadHalf<Connection>,
    buff: Vec<u8>,
    device: String,
    peer_name: String,
}
struct WriteConnection {
    conn: WriteHalf<Connection>,
    buff: Vec<u8>,
    device: String,
    peer_name: String,
}

impl ReadConnection {
    fn new(
        _peer: PublicKey,
        conn: ReadHalf<Connection>,
        device: String,
        peer_name: String,
    ) -> Self {
        Self {
            conn,
            buff: vec![],
            device,
            peer_name,
        }
    }
}

impl WriteConnection {
    fn new(
        _key: PublicKey,
        conn: WriteHalf<Connection>,
        device: String,
        peer_name: String,
    ) -> Self {
        Self {
            conn,
            buff: vec![],
            device,
            peer_name,
        }
    }
}

struct SyncData {
    name: String,
    storage_sender: UnboundedSender<StorageOperation>,
    clients: HashMap<ClientId, UnboundedSender<ClientInput>>,
    written_record_callback: Option<Box<dyn Fn()>>,
    read_record_callback: Option<Box<dyn Fn()>>,
    base_send_time: f64,
    peers: HashMap<PublicKey, Peer>,
}

impl SyncData {
    fn client_ids(&mut self) -> Vec<ClientId> {
        self.clients.keys().cloned().collect()
    }
    fn send_db_req(
        &mut self,
        op: StorageOperation,
    ) -> core::result::Result<(), TrySendError<StorageOperation>> {
        self.storage_sender.unbounded_send(op)
    }
    fn send_client_request(&mut self, client: &ClientId, request: ClientRequest) {
        let input = ClientInput::Request(request);
        self.send_client(client, input)
    }
    fn send_client_sync_output(&mut self, client: &ClientId, output: SyncOutput) {
        let input = ClientInput::Output(output);
        self.send_client(client, input)
    }
    fn send_client(&mut self, client: &ClientId, input: ClientInput) {
        self.clients
            .get_mut(client)
            .unwrap()
            .unbounded_send(input)
            .unwrap();
    }
}

async fn execute_client_command(
    sync: Rc<RefCell<SyncData>>,
    cmd: ClientCommand,
) -> Vec<(PublicKey, ClientMessage)> {
    let client = &cmd.client;
    // if let Some(callback) = &self.client_cmd_recv_callback {
    //     callback(client.clone());
    // }
    match cmd.command {
        Command::NewMessage(body, group) => {
            if let Some(msg) = process_message_from_client(&sync, body, group).await {
                return get_peer_message_pairs_to_share(&sync, vec![msg]).await;
            }
        }
        Command::NewGroup(desc) => new_group(&sync, &desc, client.clone()).await,
        Command::DeleteMessage(id) => delete_valid_message(&sync, id).await,
        Command::DeleteGroup(id) => delete_group(&sync, id).await,
        Command::StartSharingGroup(peer, group) => {
            return sharing_group_with_peer(sync, peer, group)
                .await
                .into_iter()
                .map(|msg| (peer, msg))
                .collect();
        }
        Command::StopSharingGroup(peer, id) => {
            // This won't delete the ongoing shares, which is fine
            storage_op!(sync, DeleteGroupSyncStatesForPeer, id, peer);
        }
        Command::Done => {
            // self.active_clients.remove(&client_command.client);
            // todo!();
        }
    };
    vec![]
}

async fn sharing_group_with_peer(
    sync: Rc<RefCell<SyncData>>,
    peer: PublicKey,
    group: ID,
) -> Vec<ClientMessage> {
    // get all the messages in this group that I don't know if the peer has seen
    storage_op!(
        sync,
        GetGroupsSharingMessagesUnseenByPeer,
        vec![group],
        peer
    )
}

async fn get_group(sync: &Rc<RefCell<SyncData>>, id: &ID) -> Option<Arc<Group>> {
    storage_op!(sync, GetGroup, *id)
}

async fn process_message_from_client(
    sync: &Rc<RefCell<SyncData>>,
    body: Vec<u8>,
    group: ID,
) -> Option<ClientMessage> {
    let group = get_group(sync, &group).await;
    if let Some(group) = group {
        let message = ClientMessage::new(
            Message::new_from_client(body, group.id),
            group.client.clone(),
        );
        trace!(target: "processing_messages_from_client", "Message from client {} on device {}",message.id, sync.borrow_mut().name);
        let client = &group.client;
        let parse_deps_result =
            request_client_parse_dependencies(&sync, client, message.msg()).await;
        match parse_deps_result {
            Ok(ids) => {
                if let Err(e) = storage_op!(sync, InsertMessage, message.clone(), ids, false) {
                    error!(
                        "{}: Client {} produced a duplicate message {}.",
                        e, client, message
                    );
                }
                set_message_delivered(&sync, &message).await;
                sync.borrow_mut()
                    .send_client_sync_output(client, SyncOutput::Message(message.msg()));
                let destiny = request_client_message_destiny(&sync, &message).await;
                let sharing = handle_message_destiny(&sync, &destiny, message.id).await;
                if sharing {
                    storage_op!(sync, SetMsgSharing, message.id, true);
                    return Some(message);
                }
            }
            Err(_) => error!("Client {} produced an invalid message {}", client, message),
        }
    }
    None
}

async fn handle_message_destiny(
    sync: &Rc<RefCell<SyncData>>,
    destiny: &MessageDestiny,
    message: ID,
) -> bool {
    match destiny {
        MessageDestiny::Share => return true,
        MessageDestiny::Delete => delete_valid_message(sync, message).await,
        MessageDestiny::Neither => set_msg_not_sharing(sync, message).await,
    };
    false
}

async fn delete_valid_message(sync: &Rc<RefCell<SyncData>>, message: ID) {
    storage_op!(sync, ValidDelete, message);
}

async fn set_msg_not_sharing(sync: &Rc<RefCell<SyncData>>, message: ID) {
    storage_op!(sync, SetMsgSharing, message, false);
}

async fn request_client_parse_dependencies(
    sync: &Rc<RefCell<SyncData>>,
    client: &ClientId,
    message: Arc<Message>,
) -> bramble_common::Result<Vec<ID>> {
    send_client_req!(sync, client, ParseDependencies, message)
}

async fn get_all_sharing_groups(sync: &Rc<RefCell<SyncData>>, peer: PublicKey) -> Vec<ID> {
    let clients = sync.borrow_mut().client_ids();
    let mut ids = vec![];
    for client in clients {
        ids.append(&mut send_client_req!(sync, &client, GetSharingGroups, peer))
    }
    ids
}

async fn is_sharing_peer_with_group(
    sync: &Rc<RefCell<SyncData>>,
    client: &ClientId,
    peer: PublicKey,
    group: ID,
) -> bool {
    send_client_req!(sync, client, IsSharingPeerGroup, peer, group)
}

async fn get_all_sharing_peers(sync: &Rc<RefCell<SyncData>>, group: ID) -> Vec<PublicKey> {
    let clients = sync.borrow_mut().client_ids();
    let mut keys = vec![];
    for client in clients {
        keys.append(&mut send_client_req!(sync, &client, GetSharingPeers, group))
    }
    keys
}

async fn request_client_message_destiny(
    sync: &Rc<RefCell<SyncData>>,
    message: &ClientMessage,
) -> MessageDestiny {
    send_client_req!(sync, &message.client, GetMessageDestiny, message.msg())
}

async fn get_peer_message_pairs_to_share(
    sync: &Rc<RefCell<SyncData>>,
    messages: Vec<ClientMessage>,
) -> Vec<(PublicKey, ClientMessage)> {
    let mut pairs = vec![];
    for msg in messages {
        pairs.append(
            &mut get_all_sharing_peers(sync, msg.group)
                .await
                .into_iter()
                .map(|peer| (peer, msg.clone()))
                .collect(),
        );
    }
    pairs
}

async fn process_new_message(
    sync: Rc<RefCell<SyncData>>,
    msg: ID,
) -> Vec<(PublicKey, ClientMessage)> {
    let sharing_messages = process_message(&sync, msg).await;
    get_peer_message_pairs_to_share(&sync, sharing_messages).await
}

#[async_recursion(?Send)]
async fn process_message(sync: &Rc<RefCell<SyncData>>, msg: ID) -> Vec<ClientMessage> {
    // check for delivery
    // after delivery, get dependents that can be delivered and call this function on them
    // return list of messages to share

    let all_delivered = has_all_dependencies_delivered(&sync, msg).await;
    trace!(target: "processing_messages", "Message deps all delivered: {}", all_delivered);
    if all_delivered {
        let message = get_message(&sync, msg).await.unwrap();
        let valid = deliver_message(&sync, &message).await;
        set_message_delivered(&sync, &message).await;
        match valid {
            Ok(_) => {
                let destiny = request_client_message_destiny(&sync, &message).await;
                let sharing = handle_message_destiny(&sync, &destiny, message.id).await;
                if sharing {
                    storage_op!(sync, SetMsgSharing, message.id, true);
                }
                let mut sharing_messages = vec![];
                let will_share = matches!(destiny, MessageDestiny::Share)
                    && storage_op!(&sync, AllDependenciesShared, msg);
                if will_share {
                    sharing_messages.push(message.clone());
                }
                let dependents = storage_op!(&sync, GetDependents, message.id);
                let mut deps_sharing =
                    join_all(dependents.into_iter().map(|id| process_message(&sync, id)))
                        .await
                        .into_iter()
                        .flatten()
                        .collect();
                sharing_messages.append(&mut deps_sharing);
                return sharing_messages;
                // if sharing, return vec containing this message and sharing dependents
            }
            Err(_) => invalidating_delete(&sync, message.id).await,
        }
    }
    vec![]
}

async fn has_all_dependencies_delivered(sync: &Rc<RefCell<SyncData>>, message: ID) -> bool {
    storage_op!(sync, HasAllDependenciesDelivered, message)
}

async fn get_message(sync: &Rc<RefCell<SyncData>>, message: ID) -> Option<ClientMessage> {
    storage_op!(sync, GetMessage, message)
}

async fn set_message_delivered(sync: &Rc<RefCell<SyncData>>, message: &ClientMessage) {
    storage_op!(sync, SetMessageDelivered, message.id);
}

async fn deliver_message(
    sync: &Rc<RefCell<SyncData>>,
    message: &ClientMessage,
) -> bramble_common::Result<()> {
    send_client_req!(sync, &message.client, Deliver, message.msg())
}

async fn invalidating_delete(sync: &Rc<RefCell<SyncData>>, message: ID) {
    storage_op!(sync, InvalidatingDelete, message);
}

async fn new_group(sync: &Rc<RefCell<SyncData>>, descriptor: &[u8], client: ClientId) {
    let group = Arc::new(Group::new(descriptor, client));
    storage_op!(sync, NewGroup, group.clone());
    sync.borrow_mut()
        .send_client_sync_output(&group.client.clone(), SyncOutput::Group(group));
}

async fn delete_group(sync: &Rc<RefCell<SyncData>>, group: ID) {
    storage_op!(sync, DeleteGroup, group);
}

async fn start_peer(
    sync: Rc<RefCell<SyncData>>,
    peer: PublicKey,
) -> (PublicKey, Vec<ClientMessage>) {
    // for all the sharing groups, get all the messages that I don't know that this peer has seen
    let groups = get_all_sharing_groups(&sync, peer).await;
    (
        peer,
        storage_op!(sync, GetGroupsSharingMessagesUnseenByPeer, groups, peer),
    )
}

async fn sync_receive(
    sync: Rc<RefCell<SyncData>>,
    offered_id: ID,
    peer: PeerData,
    mut incoming_wire: UnboundedReceiver<RecvWire>,
    coordinator: UnboundedSender<CoordinatorMessage>,
) -> Option<ClientMessage> {
    use SyncReceiveMessageState::*;
    use Wire::*;
    let mut state = Offered;
    let PeerData { key, name, .. } = peer;
    trace!(target: "receive_message", "{} started receiving message {} from {}",sync.borrow_mut().name, offered_id, name);
    loop {
        match state {
            Offered => {
                let msg_opt = storage_op!(sync, GetMessage, offered_id);
                if msg_opt.is_some() {
                    coordinator
                        .unbounded_send(CoordinatorMessage::send_ack(key, offered_id))
                        .unwrap();
                    return None;
                }
                trace!(target: "receive_message", "{} does not already have message {} from {}",sync.borrow_mut().name, offered_id, name);
                coordinator
                    .unbounded_send(CoordinatorMessage::send_req(key, offered_id))
                    .unwrap();
                while let Some(wire) = incoming_wire.next().await {
                    match wire.wire {
                        Off(_) => {
                            coordinator
                                .unbounded_send(CoordinatorMessage::send_req(key, offered_id))
                                .unwrap();
                        }
                        Msg(wire_message) => {
                            state = MessageReceived(wire_message);
                            break;
                        }
                        _ => panic!(),
                    }
                }
            }
            MessageReceived(msg) => {
                coordinator
                    .unbounded_send(CoordinatorMessage::send_ack(key, offered_id))
                    .unwrap();
                let group = storage_op!(sync, GetGroup, msg.group)?;
                let client = &group.client;
                let message = ClientMessage::new(msg, client.clone());
                let deps_res = send_client_req!(sync, client, ParseDependencies, message.msg());
                if let Ok(ids) = deps_res && let Ok(_) = storage_op!(sync, InsertMessage, message.clone(), ids.clone(), false) {
                    return Some(message);
                } else {
                    return None;
                }
            }
        }
    }
}

#[derive(Clone)]
struct PeerData {
    name: String,
    key: PublicKey,
    latency: u32,
}

// Passes messages between peers, receiving and sending futures, and streams futures to the
// protocol, which is what actually runs the futures
struct Coordinator {
    sync: Rc<RefCell<SyncData>>,
    peer_data: HashMap<PublicKey, PeerData>,
    latency: HashMap<PublicKey, BroadcastChannel<u32>>,
    peers: HashMap<PublicKey, UnboundedSender<SendWire>>,
    receiving: BisetMap<PublicKey, ID>,
    receiving_senders: HashMap<(PublicKey, ID), UnboundedSender<RecvWire>>,
    sending: BisetMap<PublicKey, ID>,
    sending_senders: HashMap<(PublicKey, ID), UnboundedSender<RecvWire>>,
    coordinator: UnboundedReceiver<CoordinatorMessage>,

    // For copying
    coordinator_send: UnboundedSender<CoordinatorMessage>,
}

#[derive(Debug, PartialEq, Eq)]
enum Wire {
    Ack(ID),
    Req(ID),
    Off(ID),
    Msg(Arc<Message>),
}
impl Hash for Wire {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id().hash(state)
    }
}

#[derive(Debug)]
struct SendWire {
    wire: Wire,
    peer: PublicKey,
    sender: Option<Sender<()>>,
}
impl PartialEq for SendWire {
    fn eq(&self, other: &Self) -> bool {
        // Only comparable for a particular peer
        self.wire == other.wire
    }
}
impl Eq for SendWire {}
impl Borrow<ID> for SendWire {
    fn borrow(&self) -> &ID {
        self.wire.id()
    }
}
impl Hash for SendWire {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.wire.hash(state)
    }
}

#[derive(Debug)]
struct RecvWire {
    wire: Wire,
    peer: PublicKey,
}

#[derive(Debug)]
enum CoordinatorMessage {
    SendWire(SendWire),
    RecvWire(RecvWire),
}

impl CoordinatorMessage {
    fn recv_msg(peer: PublicKey, msg: Arc<Message>) -> Self {
        Self::RecvWire(RecvWire {
            wire: Wire::Msg(msg),
            peer,
        })
    }
    fn recv_ack(peer: PublicKey, id: ID) -> Self {
        Self::RecvWire(RecvWire {
            wire: Wire::Ack(id),
            peer,
        })
    }
    fn recv_off(peer: PublicKey, id: ID) -> Self {
        Self::RecvWire(RecvWire {
            wire: Wire::Off(id),
            peer,
        })
    }
    fn recv_req(peer: PublicKey, id: ID) -> Self {
        Self::RecvWire(RecvWire {
            wire: Wire::Req(id),
            peer,
        })
    }
    fn send_msg(peer: PublicKey, message: Arc<Message>, sender: Sender<()>) -> Self {
        Self::SendWire(SendWire {
            wire: Wire::Msg(message),
            peer,
            sender: Some(sender),
        })
    }
    fn send_off(peer: PublicKey, id: ID, sender: Sender<()>) -> Self {
        Self::SendWire(SendWire {
            wire: Wire::Off(id),
            peer,
            sender: Some(sender),
        })
    }
    fn send_ack(peer: PublicKey, id: ID) -> Self {
        Self::SendWire(SendWire {
            wire: Wire::Ack(id),
            peer,
            sender: None,
        })
    }
    fn send_req(peer: PublicKey, id: ID) -> Self {
        Self::SendWire(SendWire {
            wire: Wire::Req(id),
            peer,
            sender: None,
        })
    }
}

impl Coordinator {
    fn new(sync: &Rc<RefCell<SyncData>>) -> Self {
        let (send, recv) = unbounded();
        Self {
            sync: sync.clone(),
            peer_data: HashMap::new(),
            peers: HashMap::new(),
            receiving: BisetMap::new(),
            receiving_senders: HashMap::new(),
            sending: BisetMap::new(),
            coordinator: recv,
            coordinator_send: send,
            latency: HashMap::new(),
            sending_senders: HashMap::new(),
        }
    }
    fn receive_done(&mut self, peer: &PublicKey, id: &ID) {
        self.receiving_senders.remove(&(*peer, *id));
        self.receiving.remove(peer, id);
    }
    fn sending_done(&mut self, peer: &PublicKey, id: &ID) {
        self.sending_senders.remove(&(*peer, *id));
        self.sending.remove(peer, id);
    }
    fn remove_peer(&mut self, peer: &PublicKey) {
        self.peer_data.remove(peer);
        self.peers.remove(peer);
        self.receiving.delete(peer).into_iter().for_each(|id| {
            let key = (*peer, id);
            self.receiving_senders.remove(&key);
        });
        self.sending.delete(peer).into_iter().for_each(|id| {
            let key = (*peer, id);
            self.sending_senders.remove(&key);
        });
    }
    fn add_peer(&mut self, key: PublicKey, conn: Connection, name: String) -> Peer {
        let (send, recv) = unbounded();
        self.peers.insert(key, send);
        self.peer_data.insert(
            key,
            PeerData {
                key,
                name: name.clone(),
                latency: conn.max_latency(),
            },
        );
        self.latency.insert(key, BroadcastChannel::new());
        let (read_half, write_half) = conn.split();
        let device = RefCell::borrow(&self.sync).name.clone();
        let read_conn = ReadConnection::new(key, read_half, device.clone(), name.clone());
        let write_conn = WriteConnection::new(key, write_half, device, name.clone());
        let reader = Reader::new(read_conn);
        let writer = Writer::new(write_conn);
        Peer {
            name,
            key,
            sync: self.sync.clone(),
            reader,
            writer,
            acks: HashSet::new(),
            reqs: HashSet::new(),
            offs: HashSet::new(),
            msgs: IndexSet::new(),
            coordinator_recv: recv,
            coordinator_send: self.coordinator_send.clone(),
        }
    }
    fn update_peer_latency(
        &mut self,
        peer: &PublicKey,
        latency: u32,
    ) -> LocalBoxFuture<'static, ()> {
        self.peer_data.get_mut(peer).unwrap().latency = latency;
        let channel = self.latency.get_mut(peer).unwrap().clone();
        Box::pin(async move {
            channel.send(&latency).await.unwrap();
        })
    }
    fn create_receiving(&mut self, peer: PublicKey, id: ID) -> Receiving {
        let (send, recv) = unbounded();
        self.receiving.insert(peer, id);
        self.receiving_senders.insert((peer, id), send);
        Box::pin(sync_receive(
            self.sync.clone(),
            id,
            self.peer_data.get(&peer).unwrap().clone(),
            recv,
            self.coordinator_send.clone(),
        ))
    }
    fn send_all(&mut self, peer_messages: Vec<(PublicKey, ClientMessage)>) -> Vec<Sending> {
        peer_messages
            .into_iter()
            .filter_map(|(peer, msg)| self.create_sending(peer, msg))
            .collect()
    }
    fn create_sending(&mut self, peer: PublicKey, message: ClientMessage) -> Option<Sending> {
        if self.sending.contains(&peer, &message.id) {
            return None;
        }
        let peer_data = self.peer_data.get(&peer)?;
        let (send, recv) = unbounded();
        self.sending.insert(peer, message.id);
        self.sending_senders.insert((peer, message.id), send);
        Some(Box::pin(sync_message(
            self.sync.clone(),
            peer_data.clone(),
            SyncMessageFrom::Sharing(message),
            recv,
            self.coordinator_send.clone(),
            self.latency.get(&peer).unwrap().clone(),
        )))
    }
    fn create_sending_from_request(&mut self, peer: PublicKey, id: ID) -> Option<Sending> {
        if self.sending.contains(&peer, &id) {
            return None;
        }
        let (send, recv) = unbounded();
        self.sending.insert(peer, id);
        self.sending_senders.insert((peer, id), send);
        Some(Box::pin(sync_message(
            self.sync.clone(),
            self.peer_data.get(&peer)?.clone(),
            SyncMessageFrom::Requested(id),
            recv,
            self.coordinator_send.clone(),
            self.latency.get(&peer).unwrap().clone(),
        )))
    }
}

enum SyncTask {
    Receiving(PublicKey, ID, Receiving),
    Sending(PublicKey, ID, Sending),
}

impl Stream for Coordinator {
    type Item = SyncTask;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        trace!(target: "coordinator", "polling coordinator {}", self.sync.borrow_mut().name);
        if let Some(msg) = ready!(self.coordinator.poll_next_unpin(cx)) {
            trace!(target: "coordinator", "coordinator {} found {:?}", self.sync.borrow_mut().name, msg);
            match msg {
                CoordinatorMessage::SendWire(wire) => {
                    if let Some(sender) = self.peers.get_mut(&wire.peer) {
                        sender.unbounded_send(wire).unwrap();
                    }
                }
                CoordinatorMessage::RecvWire(wire) => match wire.wire {
                    Wire::Ack(id) => {
                        // send to sending future
                        // If no sending future, then no need to create one
                        if let Some(sender) = self.sending_senders.get_mut(&(wire.peer, id)) {
                            sender.unbounded_send(wire.into()).unwrap();
                        }
                    }
                    Wire::Req(id) => {
                        // Send to sending future or create it
                        if let Some(sender) = self.sending_senders.get_mut(&(wire.peer, id)) {
                            sender.unbounded_send(wire.into()).unwrap();
                        } else {
                            if let Some(fut) = self.create_sending_from_request(wire.peer, id) {
                                return Poll::Ready(Some(SyncTask::Sending(wire.peer, id, fut)));
                            }
                        }
                    }
                    Wire::Off(id) => {
                        // send receiving future or create it
                        if let Some(sender) = self.receiving_senders.get_mut(&(wire.peer, id)) {
                            sender.unbounded_send(wire.into()).unwrap();
                        } else {
                            return Poll::Ready(Some(SyncTask::Receiving(
                                wire.peer,
                                id,
                                self.create_receiving(wire.peer, id),
                            )));
                        }
                    }
                    Wire::Msg(ref message) => {
                        // send to receiving future; dont create one
                        if let Some(sender) =
                            self.receiving_senders.get_mut(&(wire.peer, message.id))
                        {
                            sender.unbounded_send(wire).unwrap();
                        }
                    }
                },
            }
            Poll::Pending
        } else {
            Poll::Ready(None)
        }
    }
}

struct SyncStateUpdates {
    state: SyncState,
    db_last_updated: Duration,
    state_last_updated: Duration,
    db_is_updating_from_time: Option<Duration>,
    sync: Rc<RefCell<SyncData>>,
    receiver: Receiver<Option<()>>,
    deleted: bool,
}

impl SyncStateUpdates {
    fn new(sync: Rc<RefCell<SyncData>>, state: SyncState) -> Self {
        let time = time_since_epoch();
        let (sender, receiver) = channel();
        sender.send(Some(())).unwrap();
        Self {
            state,
            db_last_updated: time,
            state_last_updated: time,
            db_is_updating_from_time: None,
            sync,
            receiver,
            deleted: false,
        }
    }
    fn name(&self) -> String {
        self.sync.borrow_mut().name.clone()
    }
    fn update(&mut self) {
        if !self.deleted {
            let time = time_since_epoch();
            self.state_last_updated = time;
            if self.db_is_updating_from_time.is_none() {
                trace!(target: "sync_state", "{} ({}, {}) Starting update", self.name(), self.state.peer, self.state.message);
                let (send, recv) = channel();
                self.sync
                    .borrow_mut()
                    .send_db_req(StorageOperation::UpdateSyncState(self.state, send))
                    .unwrap();
                self.db_is_updating_from_time = Some(time);
                self.receiver = recv;
            }
        }
    }
    fn update_with(&mut self, state: SyncState) {
        self.state = state;
        self.update();
    }
    fn deleted(&mut self) {
        self.deleted = true;
    }
    fn should_update(&self) -> bool {
        // should update if the state has been updated since the db was last updated
        self.db_last_updated < self.state_last_updated && !self.deleted
    }
    fn updating(&mut self) -> &mut Receiver<Option<()>> {
        if self.receiver.is_terminated() && self.should_update() {
            self.update();
        }
        &mut self.receiver
    }
    fn updated(&mut self) {
        if let Some(time) = self.db_is_updating_from_time.take() {
            self.db_last_updated = time;
        }
    }
    fn is_terminated(&self) -> bool {
        self.receiver.is_terminated() && !self.should_update()
    }
}

enum SyncMessageFrom {
    Sharing(ClientMessage),
    Requested(ID),
}

enum SyncReceiveMessageState {
    Offered,
    MessageReceived(Arc<Message>),
}

fn sync_msg_desc(sync: &Rc<RefCell<SyncData>>, state: &SyncState) -> String {
    format!(
        "{} ({}, {})",
        sync.borrow_mut().name,
        state.peer,
        state.message
    )
}

// Info needed from protocol:
// - new wire events received from peer
// - whether the peer closes, just close the channel
async fn sync_message(
    sync: Rc<RefCell<SyncData>>,
    peer: PeerData,
    from: SyncMessageFrom,
    mut wire: UnboundedReceiver<RecvWire>,
    coordinator: UnboundedSender<CoordinatorMessage>,
    mut latency_update: BroadcastChannel<u32>,
) {
    // fetch sync_state
    // if none, create
    //
    // keep track of last change to ensure db update correctness
    // in loop, check for:
    //  - db update complete
    //  - off or msg send complete
    //  - new wire from peer
    //  - if waiting, change to latency or for msg to be ready
    // exit when
    //  - ack received
    //  - req is false
    //  - db up-to-date

    // get sync_state
    // loop
    //  - take action based on sync state
    //  - wait for new input

    let PeerData {
        name,
        key,
        mut latency,
    } = peer;
    let message;
    let mut from_requested = false;
    match from {
        SyncMessageFrom::Sharing(client_message) => message = client_message,
        SyncMessageFrom::Requested(id) => {
            from_requested = true;
            if let Some(client_message) = storage_op!(sync, GetMessage, id) {
                if !is_sharing_peer_with_group(
                    &sync,
                    &client_message.client,
                    peer.key,
                    client_message.group,
                )
                .await
                {
                    return;
                } else {
                    message = client_message;
                }
            } else {
                return;
            }
        }
    }
    let id = message.id;
    trace!(target: "sending_message", "Getting sync state");
    let state_opt = storage_op!(&sync, GetSyncState, id, key);
    let mut state = match state_opt {
        Some(state) => state,
        None => storage_op!(&sync, CreateSyncState, SyncState::new(&id, &key)),
    };
    trace!(target: "sync_state", "starting share {}", sync_msg_desc(&sync, &state));
    let mut updates = SyncStateUpdates::new(sync.clone(), state);
    if from_requested {
        state.requested = true;
        updates.update_with(state);
    }
    // let mut sending_ack = Fuse::terminated();
    let mut sending_off = Fuse::terminated();
    let mut sending_msg = Fuse::terminated();
    let mut waiting_for_ready = Fuse::terminated();
    // after updating state, generate a db update if db_last_updated == state_last_updated, then
    // update state_last_updated
    loop {
        // if state.ack && sending_ack.is_terminated() {
        // Currently offers are handled by the sync_receive async fn
        // But that function does not handle updating the sync state
        // ack field. It probably isn't necessary to do anyway
        // In the future, to be fully compliant, move this kind of logic
        // to that function

        // let (send, recv) = channel();
        // sending_ack = recv.fuse();
        // outgoing_wire.unbounded_send(Wire::ack(id, Some(send)));
        // todo!();
        // }
        let ready = state.is_ready(latency);
        if state.requested && ready && sending_msg.is_terminated() {
            let (send, recv) = channel();
            sending_msg = recv.fuse();
            coordinator
                .unbounded_send(CoordinatorMessage::send_msg(key, message.msg(), send))
                .unwrap();
        }
        if !state.requested && state.seen && updates.is_terminated() {
            trace!(target: "sync_state", "Sync complete, {}", sync_msg_desc(&sync, &state));
            break;
        }
        if !ready && waiting_for_ready.is_terminated() {
            waiting_for_ready = state.wait_for_ready(latency);
        }
        if ready && !state.seen && sending_off.is_terminated() {
            trace!(target: "sync_state", "sending offer {}", name);
            let (send, recv) = channel();
            sending_off = recv.fuse();
            coordinator
                .unbounded_send(CoordinatorMessage::send_off(key, id, send))
                .unwrap();
        }

        select! {
            // _ = sending_ack => {
            //     state.ack = false;
            //     updates.update(state);
            // }
            res = sending_msg => { // TODO: What if the peer is dropped? This will cancel and we will never send/receive more messages
                res.unwrap();
                trace!(target: "sync_state", "Message sent {}", sync_msg_desc(&sync, &state));
                state.requested = false;
                state.update_send_data(sync.borrow_mut().base_send_time, latency);
                updates.update_with(state);
            }
            res = sending_off => {
                res.unwrap();
                trace!(target: "sync_state", "Offer sent {}", sync_msg_desc(&sync, &state));
                state.requested = false;
                state.update_send_data(sync.borrow_mut().base_send_time, latency);
                updates.update_with(state);
            }
            opt = latency_update.next().fuse() => {
                trace!(target: "sync_state", "Latency updated");
                if let Some(new_latency) = opt {
                    latency = new_latency;
                    if !waiting_for_ready.is_terminated() {
                        waiting_for_ready = state.wait_for_ready(latency);
                    }
                }
            },
            _ = waiting_for_ready => {
                trace!(target: "sync_state", "Sync is ready {}", sync_msg_desc(&sync, &state));
            }
            opt = wire.next().fuse() => {
                if let Some(wire) = opt {
                    match wire.wire {
                        Wire::Ack(_) => {
                            trace!(target: "sync_state", "Message is seen {}", sync_msg_desc(&sync, &state));
                            state.seen = true;
                            updates.update_with(state);
                        },
                        Wire::Req(_) => {
                            trace!(target: "sync_state", "Message is requested {}", sync_msg_desc(&sync, &state));
                            state.requested = true;
                            updates.update_with(state);
                        },
                        _ => {panic!()}
                    }
                }
            }
            res = updates.updating() => {
                if let Some(_) =  res.unwrap() {
                     updates.updated();
                }
                trace!(target: "sync_state", "Update complete {}", sync_msg_desc(&sync, &state));
            }
        }
    }
}

impl Display for Wire {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Wire::Ack(_) => write!(f, "Ack {}", self.id()),
            Wire::Req(_) => write!(f, "Off {}", self.id()),
            Wire::Off(_) => write!(f, "Off {}", self.id()),
            Wire::Msg(_) => write!(f, "Msg {}", self.id()),
        }
    }
}

impl Wire {
    fn id(&self) -> &ID {
        use Wire::*;
        match self {
            Ack(id) | Req(id) | Off(id) => &id,
            Msg(msg) => &msg.id,
        }
    }
}

struct Writer {
    inner: WriterState,
}

enum WriterState {
    Idle(WriteConnection), // not doing anything, but there's no waker to give
    Writing(LocalBoxFuture<'static, (WriteConnection, Result<Record>)>),
    Done(WriteConnection, Waker), // needs to be woken when a new record is ready
    Transition,
}

impl Default for WriterState {
    fn default() -> Self {
        WriterState::Transition
    }
}

impl Writer {
    fn new(writer: WriteConnection) -> Self {
        Self {
            inner: WriterState::Idle(writer),
        }
    }
    fn write_record(&mut self, record: Record) {
        match std::mem::take(&mut self.inner) {
            WriterState::Idle(conn) => {
                self.inner = WriterState::Writing(Box::pin(write_one_record(conn, record)));
            }
            WriterState::Done(conn, waker) => {
                waker.wake();
                self.inner = WriterState::Writing(Box::pin(write_one_record(conn, record)));
            }
            _ => panic!(),
        }
    }
    fn can_write(&self) -> bool {
        matches!(self.inner, WriterState::Idle(_) | WriterState::Done(_, _))
    }
}

impl Display for Writer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.inner {
            WriterState::Idle(_) => write!(f, "Idle Writer"),
            WriterState::Writing(_) => write!(f, "Writing Writer"),
            WriterState::Done(_, _) => write!(f, "Done Writer"),
            _ => panic!(),
        }
    }
}

impl Future for Writer {
    type Output = Result<Record>;

    fn poll(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Self::Output> {
        use std::task::Poll::*;
        match &mut self.inner {
            WriterState::Writing(ref mut fut) => {
                let (conn, record) = futures::ready!(fut.poll_unpin(cx));
                self.inner = WriterState::Done(conn, cx.waker().clone());
                Ready(record)
            }
            WriterState::Idle(_) => {
                let conn = extract_variant!(std::mem::take(&mut self.inner), WriterState::Idle);
                self.inner = WriterState::Done(conn, cx.waker().clone());
                Pending
            }
            WriterState::Done(_, ref mut waker) => {
                *waker = cx.waker().clone();
                Pending
            }
            _ => panic!(),
        }
    }
}

impl FusedFuture for Writer {
    fn is_terminated(&self) -> bool {
        is_tup_variant!(self.inner, WriterState::Idle)
    }
}

struct Reader {
    inner: LocalBoxFuture<'static, (ReadConnection, Result<usize>)>,
}

impl Reader {
    fn new(reader: ReadConnection) -> Reader {
        Reader {
            inner: Box::pin(read_one_record(reader)),
        }
    }
}

impl Stream for Reader {
    type Item = Record;

    fn poll_next(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        use core::task::Poll::*;
        let (mut conn, res) = futures::ready!(self.inner.poll_unpin(cx));
        let name = conn.peer_name.clone();
        match res {
            Ok(0) => {
                trace!(target: "network", "Peer {} was closed", name);
                Ready(None)
            }
            Ok(_size) => {
                let buff: Vec<u8> = std::mem::take(&mut conn.buff);
                match Record::new(&buff) {
                    Ok(rec) => {
                        self.inner = Box::pin(read_one_record(conn));
                        Ready(Some(rec))
                    }
                    Err(e) => {
                        warn!("Bad data received from peer {}: {:?}", name, e);
                        Ready(None)
                    }
                }
            }
            Err(e) => {
                warn!("Network I/O error in connection to peer {}: {:?}", name, e);
                Ready(None)
            }
        }
    }
}

fn is_longest<K, V>(first: &HashSet<K, V>, second: &HashSet<K, V>, third: &HashSet<K, V>) -> bool {
    first.len() >= second.len() && first.len() >= third.len()
}

macro_rules! remove_and_notify_sent {
    ($struct:ident, $field:ident, $ids:expr) => {
        $ids.into_iter().for_each(|id| {
            if let Some(item) = $struct.$field.take(&id) {
                if let Some(sender) = item.sender {
                    sender.send(()).unwrap();
                }
            }
        })
    };
}

struct Peer {
    name: String,
    key: PublicKey,
    sync: Rc<RefCell<SyncData>>,
    reader: Reader,
    writer: Writer,
    acks: HashSet<SendWire>,
    reqs: HashSet<SendWire>,
    offs: HashSet<SendWire>,
    msgs: IndexSet<SendWire>,
    coordinator_send: UnboundedSender<CoordinatorMessage>,
    coordinator_recv: UnboundedReceiver<SendWire>,
}

macro_rules! get_ids {
    ($struct:ident, $field:ident) => {
        $struct
            .$field
            .iter()
            .map(|send_wire| *send_wire.wire.id())
            .collect()
    };
}

impl Peer {
    fn handle_record_written(&mut self, record: Record) {
        use RecordPayload::*;
        let sync = self.sync.borrow_mut();
        trace!(target: "wire_events", "{} sent {} to {}",
            sync.name,
             record.short_desc(),
             self.name,
        );
        match record.payload {
            Ack(ids) => remove_and_notify_sent!(self, acks, ids),
            Request(ids) => remove_and_notify_sent!(self, reqs, ids),
            Offer(ids) => remove_and_notify_sent!(self, offs, ids),
            Message(msg) => remove_and_notify_sent!(self, msgs, vec![msg.id]),
            Versions(_) => {}
        }
        if let Some(callback) = &sync.written_record_callback {
            callback();
        }
    }
    fn is_writing(&self) -> bool {
        is_tup_variant!(self.writer.inner, WriterState::Writing)
    }
    fn device_name(&self) -> String {
        self.sync.borrow_mut().name.clone()
    }
    fn handle_record_read(&mut self, record: Record) {
        use RecordPayload::*;
        debug!(target: "wire_events", "{} recv {} from {}",
            self.device_name(),
             record.short_desc(),
             self.name,
        );
        match record.payload {
            Message(msg) => {
                self.coordinator_send
                    .unbounded_send(CoordinatorMessage::recv_msg(self.key, msg))
                    .unwrap();
            }
            Versions(_versions) => todo!(),
            Ack(ids) => ids.into_iter().for_each(|id| {
                self.coordinator_send
                    .unbounded_send(CoordinatorMessage::recv_ack(self.key, id))
                    .unwrap()
            }),
            Offer(ids) => ids.into_iter().for_each(|id| {
                self.coordinator_send
                    .unbounded_send(CoordinatorMessage::recv_off(self.key, id))
                    .unwrap()
            }),
            Request(ids) => ids.into_iter().for_each(|id| {
                self.coordinator_send
                    .unbounded_send(CoordinatorMessage::recv_req(self.key, id))
                    .unwrap()
            }),
        }
        if let Some(callback) = &self.sync.borrow_mut().read_record_callback {
            callback();
        }
    }
    fn try_write(&mut self) {
        if self.writer.can_write() && let Some(record) = self.prepare_next_record() {
            self.writer.write_record(record);
        }
    }
    fn nothing_to_write(&self) -> bool {
        self.acks.is_empty() && self.reqs.is_empty() && self.offs.is_empty() && self.msgs.is_empty()
    }
    fn prepare_next_record(&mut self) -> Option<Record> {
        if self.nothing_to_write() {
            None
        } else if let Some(wire) = self.msgs.first() {
            Some(Record::new_msg(extract_variant!(&wire.wire, Wire::Msg)))
        } else {
            let record;
            let ids;
            if self.is_acks_longest() {
                ids = get_ids!(self, acks);
                record = Record::new_ack(ids);
            } else if self.is_reqs_longest() {
                ids = get_ids!(self, reqs);
                record = Record::new_request(ids);
            } else {
                ids = get_ids!(self, offs);
                record = Record::new_offer(ids);
            }
            Some(record)
        }
    }
    fn is_acks_longest(&self) -> bool {
        is_longest(&self.acks, &self.reqs, &self.offs)
    }
    fn is_reqs_longest(&self) -> bool {
        is_longest(&self.reqs, &self.acks, &self.offs)
    }
}

impl Future for Peer {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        use Poll::*;
        use Wire::*;
        while let Ready(Some(wire)) = self.coordinator_recv.poll_next_unpin(cx) {
            match wire.wire {
                Ack(_) => {
                    self.acks.insert(wire);
                }
                Req(_) => {
                    self.reqs.insert(wire);
                }
                Off(_) => {
                    self.offs.insert(wire);
                }
                Wire::Msg(_) => {
                    self.msgs.insert(wire);
                }
            };
        }
        self.try_write();
        if self.is_writing() {
            while let Ready(res) = self.writer.poll_unpin(cx) {
                match res {
                    Ok(record) => {
                        self.handle_record_written(record);
                    }
                    Err(_) => {
                        return Ready(());
                    }
                }
            }
        }
        if let Ready(res) = self.reader.poll_next_unpin(cx) {
            match res {
                Some(record) => {
                    trace!(target: "peer", "running peer {} received record", self.name);
                    self.handle_record_read(record);
                }
                _ => {
                    return Ready(());
                }
            };
            cx.waker().wake_by_ref();
        }
        Pending
    }
}

pub trait SyncConnection
where
    Self: AsyncRead + AsyncWrite + Latency,
{
}
impl<T: ?Sized> SyncConnection for T where Self: AsyncRead + AsyncWrite + Latency {}

type Receiving = LocalBoxFuture<'static, Option<ClientMessage>>;
type Sending = LocalBoxFuture<'static, ()>;

pub struct SyncProtocolBuilder {
    mode: SyncMode,
    client_commands: UnboundedReceiver<ClientCommand>,
    command_sender: UnboundedSender<ClientCommand>,
    clients: HashMap<ClientId, UnboundedSender<ClientInput>>,
    base_send_time: f64,
    storage: SqliteSyncStorage,
}

impl SyncProtocolBuilder {
    pub fn new(file: &str) -> Self {
        let (command_sender, client_commands) = unbounded();
        // let (sync_storage_sender, client_storage_sender, _handle) =
        //     SqliteSyncStorage::init(file, true);
        let storage = SqliteSyncStorage::new(file, true).unwrap();
        Self {
            mode: SyncMode::Interactive,
            client_commands,
            command_sender,
            clients: HashMap::new(),
            base_send_time: 2.0,
            storage,
        }
    }
    pub fn mode(mut self, mode: SyncMode) -> Self {
        self.mode = mode;
        self
    }
    pub fn base_send_time(mut self, time: f64) -> Self {
        self.base_send_time = time;
        self
    }
    pub fn client_storage<C: Client + 'static>(&mut self) -> ClientStorage<C>
    where
        <C as Client>::Group: Send,
        <C as Client>::Message: Send,
    {
        self.storage.init_client()
    }
    pub fn register_client<C: Client>(&mut self, client: C) -> SyncClient<C> {
        let (send, recv) = unbounded();
        self.clients.insert(C::get_id(), send);
        SyncClient::new(client, self.command_sender.clone(), recv)
    }
    pub fn build(
        self,
        name: &str,
        conns: Vec<(PublicKey, Pin<Box<dyn SyncConnection>>, String)>,
    ) -> SyncProtocol {
        let (sender, _handle) = self.storage.init();
        SyncProtocol::new(
            name.to_owned(),
            self.clients,
            self.client_commands,
            conns,
            self.mode,
            self.base_send_time,
            sender,
        )
        .unwrap()
    }
}
pub struct SyncProtocol {
    name: String,
    coordinator: Coordinator,
    peers: MappedFutures<PublicKey, Peer>,
    processing_messages: FuturesUnordered<LocalBoxFuture<'static, Vec<(PublicKey, ClientMessage)>>>,
    starting_peers: FuturesUnordered<LocalBoxFuture<'static, (PublicKey, Vec<ClientMessage>)>>,
    sending_messages: BiMultiMapFutures<PublicKey, ID, Sending>,
    receiving_messages: BiMultiMapFutures<PublicKey, ID, Receiving>,
    client_commands: UnboundedReceiver<ClientCommand>,
    clients_closed: bool,
    executing_commands: FuturesUnordered<LocalBoxFuture<'static, Vec<(PublicKey, ClientMessage)>>>,
    executing_controller_commands: FuturesUnordered<LocalBoxFuture<'static, ()>>,

    data: Rc<RefCell<SyncData>>,
    mode: SyncMode,
    terminating: bool,
    client_cmd_recv_callback: Option<Box<dyn Fn(ClientId)>>,
    count: u32,
}
impl Drop for SyncProtocol {
    fn drop(&mut self) {
        self.data.borrow_mut().peers.drain(); // Because of the circular Rc<RefCell<>> References
    }
}
impl SyncProtocol {
    // Sync State management
    // Seen: Set true for a message when the peer is known to have seen it
    //      - When receiving the message from the peer
    //      - When the peer offers the message
    //      - When the peer Acks the message
    // Ack: Set tru if the peer has sent or offered the message, and we still have to ack it (if we
    //      have it
    //      - Set true once a msg is received, or offered and we have it
    //      - Set false once the ack is sent
    // Request: Set true when the peer has requested the message since we last sent it
    //      - Set true when request is received
    //      - set false when sending a message
    pub fn add_conn(&mut self, peer: (PublicKey, Connection, String)) {
        self.peers
            .insert(peer.0, self.coordinator.add_peer(peer.0, peer.1, peer.2));
        self.share_unseen_messages(peer.0);
    }
    fn add_conns(&mut self, peers: Vec<(PublicKey, Connection, String)>) {
        for peer in peers {
            self.add_conn(peer);
        }
    }

    fn new(
        name: String,
        clients: HashMap<ClientId, UnboundedSender<ClientInput>>,
        client_commands: UnboundedReceiver<ClientCommand>,
        conns: Vec<(PublicKey, Pin<Box<dyn SyncConnection>>, String)>,
        mode: SyncMode,
        base_send_time: f64,
        storage: UnboundedSender<StorageOperation>,
    ) -> Option<Self> {
        if base_send_time < 1.1 {
            return None;
        }
        let data = Rc::new(RefCell::new(SyncData {
            name: name.clone(),
            storage_sender: storage,
            clients,
            written_record_callback: None,
            read_record_callback: None,
            base_send_time,
            peers: HashMap::new(),
        }));
        let coordinator = Coordinator::new(&data);
        let mut prot = Self {
            name,
            coordinator,
            peers: MappedFutures::new(),
            processing_messages: FuturesUnordered::new(),
            starting_peers: FuturesUnordered::new(),
            sending_messages: BiMultiMapFutures::new(),
            receiving_messages: BiMultiMapFutures::new(),
            client_commands,
            clients_closed: false,
            executing_commands: FuturesUnordered::new(),
            executing_controller_commands: FuturesUnordered::new(),
            data,
            mode,
            terminating: false,
            client_cmd_recv_callback: None,
            count: 0,
        };

        prot.add_conns(conns);
        futures::executor::block_on(prot.process_deliverable_messages()); // TODO: do this on struct creation
        Some(prot)
    }

    #[allow(dead_code)]
    fn validate_msg_id(_msg: ClientMessage) -> bool {
        true
    }

    pub fn update_peer_latency(&mut self, peer: &PublicKey, latency: u32) {
        self.coordinator.update_peer_latency(peer, latency);
        // Runs in O(nlogn) time. Not that great. Probably not a problem, this operations shouldn't
        // occur often.
        self.executing_controller_commands
            .push(self.coordinator.update_peer_latency(peer, latency));
    }

    fn process_message(&mut self, message: ClientMessage) {
        self.processing_messages
            .push(Box::pin(process_new_message(self.data.clone(), message.id)));
    }

    async fn process_deliverable_messages(&mut self) {
        // get the messages that have all deps delivered but are not delivered
        let deliverables = storage_op!(self.data, GetDeliverableMessages);
        deliverables.into_iter().for_each(|message| {
            self.process_message(message);
        });
    }

    fn share_unseen_messages(&mut self, peer: PublicKey) {
        self.starting_peers
            .push(Box::pin(start_peer(self.data.clone(), peer)));
    }

    pub fn terminate(&mut self) {
        self.terminating = true;
    }

    pub fn register_written_record_callback(&mut self, callback: Box<dyn Fn()>) {
        self.data.borrow_mut().written_record_callback = Some(callback);
    }
    pub fn register_read_record_callback(&mut self, callback: Box<dyn Fn()>) {
        self.data.borrow_mut().read_record_callback = Some(callback);
    }
    pub fn register_client_cmd_recv_callback(&mut self, callback: Box<dyn Fn(ClientId)>) {
        self.client_cmd_recv_callback = Some(callback);
    }

    fn sync_message(&mut self, peer: &PublicKey, msg: ClientMessage) {
        trace!(target: "sync_message", "syncing message {} with peer {:?} in device {}", msg.id, self.try_get_peer_name(peer), self.name);
        let id = msg.id;
        let fut_opt = self.coordinator.create_sending(*peer, msg);
        if let Some(fut) = fut_opt {
            self.sending_messages.insert(*peer, id, fut);
        };
    }
    fn sync_all(&mut self, peer_messages: Vec<(PublicKey, ClientMessage)>) {
        peer_messages.into_iter().for_each(|(peer, msg)| {
            self.sync_message(&peer, msg);
        });
    }

    fn get_peer_name(&mut self, key: &PublicKey) -> String {
        self.peers.get(key).unwrap().name.clone()
    }
    fn try_get_peer_name(&mut self, key: &PublicKey) -> Option<String> {
        self.peers.get(key).map(|peer| peer.name.clone())
    }
    fn poll_next_interactive(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Poll<Option<ControllerNotification>> {
        use Poll::*;
        trace!(target: "interactive", "polling interactive {}", self.name);
        if self.terminating {
            return Ready(None);
        }
        if !self.starting_peers.is_empty() {
            if let Ready(Some((peer, messages))) = self.starting_peers.poll_next_unpin(cx) {
                // debug!(target: "interactive","peer {} started", self.get_peer_name(&peer));
                debug!(target: "starting_peers","peer {} started in device {}", self.get_peer_name(&peer), self.name);
                messages
                    .into_iter()
                    .for_each(|message| self.sync_message(&peer, message));
                cx.waker().wake_by_ref();
            }
        }

        if !self.receiving_messages.is_empty() {
            if let Ready(Some((peer, id, message_opt))) =
                self.receiving_messages.poll_next_unpin(cx)
            {
                self.coordinator.receive_done(&peer, &id);
                if let Some(message) = message_opt {
                    debug!(target: "interactive","message received");
                    self.process_message(message);
                }
                cx.waker().wake_by_ref();
            }
        }

        if !self.sending_messages.is_empty() {
            if let Ready(Some((peer, id, _))) = self.sending_messages.poll_next_unpin(cx) {
                self.coordinator.sending_done(&peer, &id);
            }
        }

        if !self.processing_messages.is_empty() {
            if let Ready(Some(peer_messages)) = self.processing_messages.poll_next_unpin(cx) {
                debug!(target: "interactive","msg processing outputs {:?}", peer_messages);
                self.sync_all(peer_messages);
                cx.waker().wake_by_ref();
            }
        }

        if !self.clients_closed {
            if let Ready(next_cmd) = self.client_commands.poll_next_unpin(cx) {
                match next_cmd {
                    Some(cmd) => {
                        if let Some(callback) = &self.client_cmd_recv_callback {
                            callback(cmd.client.clone());
                        }
                        self.executing_commands
                            .push(Box::pin(execute_client_command(self.data.clone(), cmd)));
                    }
                    None => self.clients_closed = true,
                }
                cx.waker().wake_by_ref();
            }
        }

        if !self.executing_commands.is_empty() {
            if let Ready(Some(peer_messages)) = self.executing_commands.poll_next_unpin(cx) {
                debug!(target: "interactive","Client command execution outputs {:?}", peer_messages);
                self.sync_all(peer_messages);
                cx.waker().wake_by_ref();
            }
        }

        if !self.executing_controller_commands.is_empty() {
            let _ = self.executing_controller_commands.poll_next_unpin(cx);
            debug!(target: "interactive_iteration","executing_controller_commands future completed");
            cx.waker().wake_by_ref();
        }

        if let Ready(sync_task) = self.coordinator.poll_next_unpin(cx) {
            match sync_task.unwrap() {
                SyncTask::Receiving(key, id, fut) => self.receiving_messages.insert(key, id, fut),
                SyncTask::Sending(key, id, fut) => self.sending_messages.insert(key, id, fut),
            };
            cx.waker().wake_by_ref();
        }

        if !self.peers.is_empty() {
            if let Ready(Some((key, _))) = self.peers.poll_next_unpin(cx) {
                self.coordinator.remove_peer(&key);
                let mut data = self.data.borrow_mut();
                let peer = data.peers.remove(&key).unwrap();
                trace!(target: "network", "Peer {} was closed", peer.name);
                cx.waker().wake_by_ref();
                return Ready(Some(ControllerNotification::Disconnected(key)));
            }
        }
        Pending
    }
    pub fn is_done(&self) -> bool {
        // check if has any active peers or open connections with clients
        trace!(
            target: "interactive",
            "Clients closed: {}, Num peers: {}, activity: {}",
            self.clients_closed,
            self.num_peers(),
            self.is_active()
        );
        self.clients_closed && self.is_lonely() && !self.is_active()
    }

    fn is_lonely(&self) -> bool {
        self.data.borrow_mut().peers.is_empty()
    }

    fn num_peers(&self) -> usize {
        self.data.borrow_mut().peers.len()
    }

    pub fn print_activity(&mut self) {
        println!(
            "{} Sync Active
            receiving_messages: {}
            processing_messages: {}
            starting_peers: {}
            sharing_messages: {}
            executing_controller_commands: {}
            executing_client_commands: {}
                ",
            self.name,
            self.receiving_messages.len(),
            self.processing_messages.len(),
            self.starting_peers.len(),
            self.sending_messages.len(),
            self.executing_controller_commands.len(),
            self.executing_commands.len()
        );
        println!(
            "Syncing Messages {}\n{}",
            self.name.clone(),
            self.list_syncing_messages()
        );
        println!(
            "Receiving Messages {}\n{}",
            self.name.clone(),
            self.list_receiving_messages()
        );
    }

    pub fn is_active(&self) -> bool {
        trace!(
            target: "sync_active",
            "{} Sync Active
            receiving_messages: {}
            processing_messages: {}
            starting_peers: {}
            sharing_messages: {}
            executing_controller_commands: {}
            executing_client_commands: {}
                ",
            self.name,
            self.receiving_messages.len(),
            self.processing_messages.len(),
            self.starting_peers.len(),
            self.sending_messages.len(),
            self.executing_controller_commands.len(),
            self.executing_commands.len()
        );
        !(self.processing_messages.is_empty()
            && self.receiving_messages.is_empty()
            && self.starting_peers.is_empty()
            && self.sending_messages.is_empty()
            && self.executing_commands.is_empty()
            && self.executing_controller_commands.is_empty())
    }

    fn list_receiving_messages(&mut self) -> String {
        let mut s = String::new();
        self.receiving_messages
            .collect_keys()
            .into_iter()
            .for_each(|(peer, id)| {
                s.push_str(&format!("({}, {})", peer, id));
            });
        s
    }
    fn list_syncing_messages(&mut self) -> String {
        let mut s = String::new();
        self.sending_messages
            .collect_keys()
            .into_iter()
            .for_each(|(peer, id)| {
                s.push_str(&format!("({}, {})", peer, id));
            });
        s
    }

    async fn batch_sync_session(&mut self) -> Option<ControllerNotification> {
        // read from readers until they are closed
        // read from clients until they are closed
        // then write to writers until all messages are exhausted
        async {}.await;
        todo!();
        // keep track of messages sent in the session, to not repeat messages
    }
    async fn eager_sync_session(&mut self) -> Option<ControllerNotification> {
        async {}.await;
        todo!();
        // keep track of messages sent in the session, to not repeat messages
    }
}

impl Stream for SyncProtocol {
    type Item = ControllerNotification;
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        match self.mode {
            SyncMode::Interactive => self.poll_next_interactive(cx),
            SyncMode::Batch => {
                todo!()
            }
            SyncMode::Eager => {
                todo!()
            }
        }
    }
}

#[derive(Debug)]
struct Record {
    version: u8,
    payload_len: u16,
    payload: RecordPayload,
}
impl Record {
    fn short_id_list(ids: &Vec<ID>) -> String {
        let mut s = String::new();
        if !ids.is_empty() {
            s.push_str(&format!("{}", ids[0]));
            ids[1..]
                .iter()
                .for_each(|id| s.push_str(&format!(",{}", id)));
        }
        s
    }
    fn short_desc(&self) -> String {
        match &self.payload {
            RecordPayload::Ack(ids) => format!("Ack {}", Record::short_id_list(ids)),
            RecordPayload::Message(msg) => format!("Msg {}", hex::encode(&msg.id.as_ref()[0..2])),
            RecordPayload::Offer(ids) => format!("Off {}", Record::short_id_list(ids)),
            RecordPayload::Request(ids) => format!("Req {}", Record::short_id_list(ids)),
            RecordPayload::Versions(_) => "Versions".to_string(),
        }
    }
    fn new(data: &Vec<u8>) -> Result<Record> {
        //Self::validate_record(&data)?;
        let version: u8 = data[0];
        let record_type = data[1];
        debug!("Parsing peer data, record type: {}", record_type);
        // Because of how data is read from peers, by the time this function is called, there is
        // guaranteed to be the correct number of bytes in the data, so the below operation should
        // never panic
        let payload_len: u16 = u16::from_be_bytes(*slice_as_array!(&data[2..4], [u8; 2]).unwrap());
        let payload_data: &[u8] = &data[4..];
        let res = match record_type {
            0 => Self::ids_from_bytes(payload_data).map(RecordPayload::Ack),
            1 => {
                let msg = Message::new_from_peer(payload_data);
                Ok(RecordPayload::Message(msg))
            }
            2 => Self::ids_from_bytes(payload_data).map(RecordPayload::Offer),
            3 => Self::ids_from_bytes(payload_data).map(RecordPayload::Request),
            4 => Ok(RecordPayload::Versions(payload_data.to_vec())),
            _ => return Err(Error::InvalidRecord),
        };
        if let Ok(payload) = res {
            Ok(Record {
                version,
                payload_len,
                payload,
            })
        } else {
            Err(Error::InvalidRecord)
        }
    }

    fn ids_from_bytes(data: &[u8]) -> Result<Vec<ID>> {
        match data.len().rem_euclid(HASH_LEN) {
            0 => Ok(data.array_chunks().map(|arr| (*arr).into()).collect()),
            _ => Err(bramble_common::Error::InvalidLength),
        }
    }

    fn new_offer(ids: Vec<ID>) -> Self {
        Record {
            version: 0,
            payload_len: (HASH_LEN * ids.len()) as u16,
            payload: RecordPayload::Offer(ids),
        }
    }

    fn new_request(ids: Vec<ID>) -> Self {
        Record {
            version: 0,
            payload_len: (HASH_LEN * ids.len()) as u16,
            payload: RecordPayload::Request(ids),
        }
    }

    fn new_ack(ids: Vec<ID>) -> Self {
        Record {
            version: 0,
            payload_len: (HASH_LEN * ids.len()) as u16,
            payload: RecordPayload::Ack(ids),
        }
    }

    fn new_msg(msg: &Arc<Message>) -> Self {
        Self {
            version: 0,
            payload_len: msg.size(),
            // TODO: Don't clone the message, instead have each record use the same
            // message struct, probably with Box and RefCounts
            payload: RecordPayload::Message(Arc::clone(msg)),
        }
    }

    fn to_buffer(&self) -> Vec<u8> {
        let mut buf = vec![];
        buf.push(self.version);
        match &self.payload {
            RecordPayload::Ack(ids) => {
                buf.push(0);
                buf.extend_from_slice(&self.payload_len.to_be_bytes());
                ids.iter().for_each(|id| buf.extend_from_slice(id.as_ref()));
            }
            RecordPayload::Message(msg) => {
                buf.push(1);
                buf.extend_from_slice(&self.payload_len.to_be_bytes());
                buf.extend_from_slice(&msg.to_bytes());
            }
            RecordPayload::Offer(ids) => {
                buf.push(2);
                buf.extend_from_slice(&self.payload_len.to_be_bytes());
                ids.iter().for_each(|id| buf.extend_from_slice(id.as_ref()));
            }
            RecordPayload::Request(ids) => {
                buf.push(3);
                buf.extend_from_slice(&self.payload_len.to_be_bytes());
                ids.iter().for_each(|id| buf.extend_from_slice(id.as_ref()));
            }
            RecordPayload::Versions(ids) => {
                buf.push(4);
                buf.extend_from_slice(&self.payload_len.to_be_bytes());
                ids.iter().for_each(|id| buf.push(*id));
            }
        }
        buf
    }
}

#[cfg(test)]
pub mod tests {
    use crate::simple_client::*;
    use crate::simple_controller::*;
    use crate::storage::tests::*;

    use crate::sync::*;
    use crate::test::*;
    use bramble_common::{make_duplex, Duplex};

    use bramble_crypto::KeyPair;
    use futures::executor::block_on;
    use futures::future::{join, join_all};

    use futures_timer::Delay;
    use serde_json::to_string;
    use std::cell::RefCell;

    use backtrace_on_stack_overflow::enable;
    use std::rc::Rc;

    #[test]
    #[ignore]
    fn sync_create_protocol() {
        init();
        let file = wipe("./tests/SyncCreateProtocol.db");
        let name = "Alice".to_string();
        let mut controller: SimpleController = SimpleController::new(&name, &file, vec![]);
        block_on(join(
            Delay::new(Duration::from_millis(500)),
            controller.run_to_exit(),
        ));
        block_on(controller.run_for_millis(500));
        controller.terminate();
        block_on(controller.run_to_exit());
    }

    #[test]
    fn sync_protocol_with_peer() {
        init();
        let file = wipe("./tests/SyncWithPeer.db");
        let name = "Alice".to_string();
        let p = peer(0);
        let (dup_a, _) = make_duplex_ref();
        let conns: Vec<(PublicKey, Pin<Box<dyn SyncConnection>>, String)> =
            vec![(p, dup_a, "Peer1".to_string())];
        let mut controller = SimpleController::new(&name, &file, conns);
        block_on(controller.run_for_millis(500));
        controller.terminate();
        block_on(controller.run_to_exit());
    }

    #[test]
    fn sync_adding_new_group() {
        init();
        let file = wipe("./tests/SyncNewGroup.db");
        let name = "Alice".to_string();
        let mut controller: SimpleController = SimpleController::new(&name, &file, vec![]);
        block_create_group(&mut controller, "NewGroup");
        // controller.done();
        // block_on(controller.run_to_output());
        // assert_group_exists(group.id, &file)
    }

    #[test]
    fn sync_adding_new_message() {
        init();
        let file = wipe("./tests/SyncNewMessage.db");
        let name = "Alice".to_string();
        let mut controller: SimpleController = SimpleController::new(&name, &file, vec![]);
        let group = controller.add_group("NewGroup".to_string());
        let msg = MessageBody {
            from: peer(0).to_string(),
            text: "Valid Message".to_string(),
            dependencies: vec![],
        };
        controller.send_message_json(group.id, to_string(&msg).unwrap());
        let message = extract_variant!(
            block_on(controller.run_to_output()),
            SimpleClientItem::Message
        );
        assert_group_exists(group.id, &file);

        // controller.done();
        block_on(controller.run_for_millis(1500));
        let mut p = storage_params(&file);
        let _msg = fetch_message(&mut p, &message.id);
        assert_msgs_sharing(&mut p, vec![message.id], true);
    }

    #[test]
    fn sync_adding_invalid_message() {
        init();
        let file = wipe("./tests/SyncInvalidMessage.db");
        let name = "Alice".to_string();
        let mut controller: SimpleController = SimpleController::new(&name, &file, vec![]);
        let group = controller.add_group("NewGroup".to_string());
        // let _out = block_on(controller.run_to_output());
        // assert_group_exists(group.id, file.clone());
        let body = "aldskjflsjfaldfkj".to_string();
        controller.send_message_json(group.id, body);
        // controller.done();
        block_on(controller.run_for_millis(1000));
        let mut p = storage_params(&file);
        assert_group_has_num_messages(&mut p, group.id, 0);
    }

    // #[allow(dead_code)]
    // fn create_message_graph0(controller: &mut SimpleController) {
    //     let out = controller.add_group("Group0".to_string());
    //     let group = extract_variant!(
    //         block_on(controller.run_to_output()),
    //         SimpleClientItem::Group
    //     );
    //     assert_eq!(&group.id, &out.id);
    // }

    // #[allow(dead_code)]
    // fn create_message_graph1(controller: &mut SimpleController) {
    //     let group = controller.add_group("Group1".to_string());
    //     let msg = message(None, "OnlyMessage", vec![]);
    //     assert_group_exists(group.id, controller.get_name().clone());
    //     controller.send_message(group.id, msg);
    // }
    fn block_create_group(controller: &mut SimpleController, group: &str) -> SimpleClientGroup {
        let group = controller.add_group(group.to_string());
        let msg = MessageBody {
            from: peer(9).to_string(),
            text: "First Message".to_string(),
            dependencies: vec![],
        };
        controller.send_message(group.id, msg);
        block_on(controller.run_to_output());
        assert_group_exists(group.id, &controller.get_file());
        group
    }
    fn block_insert_message(
        controller: &mut SimpleController,
        group: ID,
        msg: MessageBody,
    ) -> crate::simple_client::ClientMessage {
        controller.send_message(group, msg);
        let msg = extract_variant!(
            block_on(controller.run_to_output()),
            SimpleClientItem::Message
        );
        msg
    }
    fn assert_msgs_sharing_delivered(p: &mut TestParams, ids: Vec<ID>) {
        assert_msgs_sharing(p, ids.clone(), true);
        assert_msgs_delivered(p, ids, true);
    }

    #[test]
    fn create_several_message_graphs() {
        init();
        let file = wipe("./tests/SyncSeveralGraphs.db");
        let name = "Alice".to_string();
        let mut controller: SimpleController = SimpleController::new(&name, &file, vec![]);
        let c = &mut controller;
        let group1 = block_create_group(c, "Group1");
        let group2 = block_create_group(c, "Group2");
        let group3 = block_create_group(c, "Group3");
        let group4 = block_create_group(c, "Group4");
        let msg0 = block_insert_message(c, group1.id, message(None, "Blah", vec![]));
        let msg1 = block_insert_message(c, group2.id, message(None, "Blag", vec![]));
        let _msg2 = block_insert_message(c, group2.id, message(None, "Bleck", vec![msg1.id]));
        let _msg3 = block_insert_message(c, group2.id, message(None, "Blah", vec![msg1.id]));
        let _msg4 = block_insert_message(c, group3.id, message(None, "Blah", vec![]));
        let msg5 = block_insert_message(c, group3.id, message(None, "Bling", vec![]));
        let _msg6 = block_insert_message(c, group3.id, message(None, "Blarg", vec![msg5.id]));
        let msg7 = block_insert_message(c, group4.id, message(None, "Bloot", vec![]));
        let msg8 = block_insert_message(c, group4.id, message(None, "Blang", vec![msg7.id]));
        let msg9 = block_insert_message(c, group4.id, message(None, "Blort", vec![msg8.id]));
        let _msg10 = block_insert_message(c, group4.id, message(None, "Blast", vec![msg9.id]));
        let _msg11 = block_insert_message(c, group4.id, message(None, "Blenth", vec![msg9.id]));

        controller.done();
        block_on(controller.run_for_millis(1000));
        let mut p = storage_params(&file);
        assert_msgs_sharing_delivered(&mut p, vec![msg0.id, msg1.id]);
    }

    fn create_two_controllers(
        name1: &str,
        name2: &str,
    ) -> (
        SimpleController,
        TestParams,
        KeyPair,
        SimpleController,
        TestParams,
        KeyPair,
    ) {
        let file1 = wipe(format!("./tests/{}.db", name1).as_str());
        let file2 = wipe(format!("./tests/{}.db", name2).as_str());
        let key1 = key();
        let key2 = key();
        let controller1: SimpleController = SimpleController::new(name1, &file1, vec![]);
        let controller2: SimpleController = SimpleController::new(name2, &file2, vec![]);
        let p1 = storage_params(&file1);
        let p2 = storage_params(&file2);
        (controller1, p1, key1, controller2, p2, key2)
    }

    fn create_one_controller(name: &str) -> (SimpleController, TestParams, KeyPair) {
        let file = wipe(format!("./tests/{}.db", name).as_str());
        let key = key();
        let controller1: SimpleController = SimpleController::new(name, &file, vec![]);
        let p1 = storage_params(&controller1.get_file());
        (controller1, p1, key)
    }

    fn make_duplex_ref() -> (Pin<Box<Duplex>>, Pin<Box<Duplex>>) {
        let (dup_1, dup_2) = make_duplex();
        (Box::pin(dup_1), Box::pin(dup_2))
    }

    #[test]
    fn exchange_one_message() {
        unsafe { enable() }
        // try adding group both during and after sync protocol startup
        init();
        let (mut c1, mut p1, k1, mut c2, mut p2, k2) =
            create_two_controllers("SyncExchangeOne1", "SyncExchangeOne2");
        let group1 = block_create_group(&mut c1, "Group1");
        block_create_group(&mut c2, "Group1");
        let (dup_1, dup_2) = make_duplex_ref();
        c1.add_peer(k2.public(), dup_2, "SyncExchangeOne2".to_string());
        c2.add_peer(k1.public(), dup_1, "SyncExchangeOne1".to_string());
        c1.add_peer_to_group(k2.public(), &group1.id);
        c2.add_peer_to_group(k1.public(), &group1.id);
        let msg0 = block_insert_message(&mut c1, group1.id, message(None, "Blah", vec![]));
        // c1.done();
        let msg = extract_variant!(
            extract_variant!(
                block_on(futures::future::select(
                    Box::pin(c1.run_to_output()),
                    Box::pin(c2.run_to_output()),
                )),
                futures::future::Either::Right
            )
            .0,
            SimpleClientItem::Message
        );
        assert_eq!(msg.id, msg0.id);

        block_on(join(c1.run_for_millis(1000), c2.run_for_millis(1000)));

        assert_sync_state_seen(&mut p1, k2.public(), &msg0.id, true);
        assert_sync_state_seen(&mut p2, k1.public(), &msg0.id, true);
    }

    use linked_hash_map::LinkedHashMap;
    use linked_hash_set::LinkedHashSet;
    struct Network {
        devices: LinkedHashSet<String>,
        keys: HashMap<String, PublicKey>,
        controllers: HashMap<String, SimpleController>,
        stores: HashMap<String, TestParams>,
        edges: HashMap<String, HashSet<String>>,
        groups: LinkedHashMap<String, (Arc<Group>, HashSet<String>)>,
        client_cmd_counters: HashMap<String, Rc<RefCell<u8>>>,
        messages: Vec<crate::simple_client::ClientMessage>,
        records_to_read: Rc<RefCell<u32>>,
    }

    impl fmt::Display for Network {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(
                f,
                "Network
                Devices: {:?}
                Edges: {:?}
                Groups: {:?}",
                self.devices, self.edges, self.groups
            )
        }
    }

    impl Network {
        fn new(
            device_names: &Vec<&str>,
            edges: &Vec<(&str, &str)>,
            groups_memberships: &Vec<(&str, Vec<&str>)>,
        ) -> Self {
            let mut network = Self {
                devices: LinkedHashSet::new(),
                keys: HashMap::new(),
                controllers: HashMap::new(),
                stores: HashMap::new(),
                edges: HashMap::new(),
                groups: LinkedHashMap::new(),
                client_cmd_counters: HashMap::new(),
                messages: vec![],
                records_to_read: Rc::new(RefCell::new(0)),
            };

            device_names
                .into_iter()
                .for_each(|name| network.add_device(name));
            network.register_client_cmd_counters();
            edges.into_iter().for_each(|edge| network.add_edge(edge));
            groups_memberships.into_iter().for_each(|(group, members)| {
                network.add_group(group, members);
            });
            network.register_counters();
            network
        }

        fn register_counters(&mut self) {
            self.controllers.iter_mut().for_each(|cont| {
                let counter = self.records_to_read.clone();
                cont.1
                    .sync_ref()
                    .register_read_record_callback(Box::new(move || *counter.borrow_mut() -= 1));
                let counter = self.records_to_read.clone();
                cont.1
                    .sync_ref()
                    .register_written_record_callback(Box::new(move || *counter.borrow_mut() += 1));
            });
        }

        fn register_client_cmd_counters(&mut self) {
            self.controllers.values_mut().for_each(|cont| {
                let client_cmd_counter = Rc::new(RefCell::new(0));
                let client_cmd_counter_clone = client_cmd_counter.clone();
                let client_cmd_counter_clone2 = client_cmd_counter.clone();
                self.client_cmd_counters
                    .insert(cont.get_name(), client_cmd_counter);
                cont.sync_ref()
                    .register_client_cmd_recv_callback(Box::new(move |_client| {
                        *client_cmd_counter_clone.borrow_mut() -= 1;
                    }));
                cont.client_ref()
                    .register_cmd_sent_callback(Box::new(move |_client| {
                        *client_cmd_counter_clone2.borrow_mut() += 1;
                    }));
            });
        }

        fn add_device(&mut self, name: &str) {
            self.devices.insert(name.to_string());
            let (c, p, k) = create_one_controller(name);
            self.keys.insert(name.to_string(), *k.public());
            self.controllers.insert(name.to_string(), c);
            self.stores.insert(name.to_string(), p);
            self.edges.insert(name.to_string(), HashSet::new());
        }
        fn add_edge(&mut self, (peer1, peer2): &(&str, &str)) {
            let key1 = self.keys.get(*peer1).unwrap();
            let key2 = self.keys.get(*peer2).unwrap();
            let (dup_1, dup_2) = make_duplex();
            self.controllers.get_mut(*peer1).unwrap().add_peer(
                key2,
                Box::pin(dup_2),
                peer2.to_string(),
            );
            self.controllers.get_mut(*peer2).unwrap().add_peer(
                key1,
                Box::pin(dup_1),
                peer1.to_string(),
            );
            self.edges
                .get_mut(*peer1)
                .unwrap()
                .insert((*peer2).to_string());
            self.edges
                .get_mut(*peer2)
                .unwrap()
                .insert((*peer1).to_string());
        }

        // memberships must have at least one element, there are no groups with no members
        fn add_group(&mut self, name: &str, members: &Vec<&str>) -> Arc<Group> {
            let group = group(name);
            self.groups
                .insert(name.to_string(), (group.clone(), HashSet::new()));
            members
                .iter()
                .for_each(|member| self.add_peer_to_group(name, member));
            group
        }

        fn add_peer_to_group(&mut self, name: &str, peer: &str) {
            let members = &mut self.groups.get_mut(name).unwrap().1;
            let controller = self.controllers.get_mut(peer).unwrap();
            let group = block_create_group(controller, name);
            let key = self.keys.get(peer).unwrap();
            members.iter().for_each(|member| {
                let member_key = self.keys.get(member).unwrap();
                controller.add_peer_to_group(member_key, &group.id)
            });
            members.iter().for_each(|member| {
                self.controllers
                    .get_mut(member)
                    .unwrap()
                    .add_peer_to_group(key, &group.id);
            });
            members.insert(peer.to_string());
        }

        fn all_messages_fully_shared(&mut self) -> bool {
            let msgs = self.messages.clone();
            !msgs
                .into_iter()
                .any(|msg| !self.message_is_fully_shared(&msg))
        }

        fn insert_message(
            &mut self,
            peer: &str,
            group: ID,
            msg: MessageBody,
        ) -> crate::simple_client::ClientMessage {
            let msg = block_on(
                self.controllers
                    .get_mut(peer)
                    .unwrap()
                    .insert_message(group, msg),
            );
            self.messages.push(msg.clone());
            msg
        }

        fn get_group(&self, name: &str) -> Arc<Group> {
            self.groups.get(name).unwrap().0.clone()
        }

        fn network_is_active(&mut self) -> bool {
            let to_read = *self.records_to_read.as_ref().borrow() > 0;
            let client_commands = self
                .client_cmd_counters
                .values()
                .any(|rc| *rc.as_ref().borrow() > 0);
            let some_active = self
                .controllers
                .iter_mut()
                .any(|cont| cont.1.sync_ref().is_active());
            to_read || client_commands || some_active
        }

        fn print_activity(&mut self) {
            self.controllers
                .iter_mut()
                .for_each(|cont| cont.1.sync_ref().print_activity());
        }

        fn run_to_all_shared(&mut self) {
            let mut active = self.network_is_active();
            while active {
                block_on(join_all(
                    self.controllers
                        .iter_mut()
                        .map(|cont| cont.1.run_for_millis(500)),
                ));
                self.all_messages_fully_shared();
                active = self.network_is_active();
            }
        }

        fn pop_msg(&mut self, peer: &str) -> crate::simple_client::ClientMessage {
            self.controllers
                .get_mut(peer)
                .unwrap()
                .pop_message()
                .unwrap()
        }

        fn group_by_id(&self, id: &ID) -> (String, Arc<Group>, HashSet<String>) {
            let (name, (group, members)) = self
                .groups
                .iter()
                .find(|(_, (group, _))| &group.id == id)
                .unwrap();
            (name.clone(), group.clone(), members.clone())
        }

        fn message_is_fully_shared(&mut self, msg: &crate::simple_client::ClientMessage) -> bool {
            use itertools::Itertools;
            let peers = self.group_by_id(&msg.group).2;
            // For every two of the above peers, check if each knows the other has the message
            peers.iter().combinations(2).all(|pair| {
                if self.edges.get(pair[0]).unwrap().contains(pair[1]) {
                    let k2 = self.keys.get(pair[1]).unwrap();
                    let c1 = self.stores.get_mut(pair[0]).unwrap();
                    let seen1 = sync_state_is_seen(c1, &k2, &msg.id);
                    let k1 = self.keys.get(pair[0]).unwrap();
                    let c2 = self.stores.get_mut(pair[1]).unwrap();
                    let seen2 = sync_state_is_seen(c2, &k1, &msg.id);
                    seen1 && seen2
                } else {
                    true
                }
            })
        }
        fn assert_msg_fully_shared(&mut self, msg: &crate::simple_client::ClientMessage) {
            assert_eq!(self.message_is_fully_shared(msg), true);
        }
    }

    #[test]
    // #[ignore]
    fn test_network() {
        let devices = vec![];
        let edges = vec![];
        let groups_memberships = vec![];
        Network::new(&devices, &edges, &groups_memberships);
    }

    #[allow(dead_code)]
    fn matching_controller<'a>(
        controllers: &'a mut Vec<(SimpleController, TestParams, KeyPair)>,
        peer: &str,
    ) -> &'a mut (SimpleController, TestParams, KeyPair) {
        controllers
            .iter_mut()
            .find(|(c, _, _)| c.get_name() == (*peer).to_string())
            .unwrap()
    }

    #[allow(dead_code)]
    fn matching_controllers<'a>(
        controllers: &'a mut Vec<(SimpleController, TestParams, KeyPair)>,
        peers: Vec<&str>,
    ) -> Vec<&'a mut (SimpleController, TestParams, KeyPair)> {
        let mut found = controllers
            .iter_mut()
            .filter(|(c, _, _)| peers.iter().any(|name| c.get_name() == name.to_string()))
            .collect::<Vec<_>>();
        found.sort_unstable_by(|(c1, _, _), (c2, _, _)| {
            let p1 = peers
                .iter()
                .position(|name| *name == &c1.get_name())
                .unwrap();
            let p2 = peers
                .iter()
                .position(|name| *name == &c2.get_name())
                .unwrap();
            p1.partial_cmp(&p2).unwrap()
        });
        found
    }

    #[allow(dead_code)]
    fn assert_msg_fully_shared(
        controllers: &mut Vec<(SimpleController, TestParams, KeyPair)>,
        msg: &ClientMessage,
        edges: &Vec<(&str, &str)>,
        memberships: &Vec<(String, Vec<&str>)>,
        groups_names: &Vec<(String, Arc<Group>)>,
    ) {
        // for every peerI in the group of the message, assert that for every edge (peer_a, peer_b)
        // where peer_a or peer_b == peerI, that both peer_a and peer_b know that the other has seen
        // the message
        let group_name_map = groups_names
            .iter()
            .map(|item| item.clone())
            .collect::<HashMap<_, _>>();
        let peers = &memberships
            .iter()
            .find(|(group, _)| msg.group == group_name_map.get(group).unwrap().id)
            .unwrap()
            .1;
        peers.iter().for_each(|peer| {
            edges.iter().for_each(|(peer_a, peer_b)| {
                let mut conts = matching_controllers(controllers, vec![peer, peer_a, peer_b]);

                if peer_a == peer {
                    let key = *conts[2].2.public();
                    assert_sync_state_seen(&mut conts[0].1, &key, &msg.id, true);
                } else if peer_b == peer {
                    let key = *conts[1].2.public();
                    assert_sync_state_seen(&mut conts[0].1, &key, &msg.id, true);
                }
            });
        });
    }

    #[test]
    // #[ignore]
    fn exchange_several_in_small_network() {
        // Test connection between 4 peers, several messages over 3 groups
        // connections:
        //      1-2,2-3,2-4
        // alternate groups of messages as so:
        //      message M_n in G_(n%|G|)
        init();
        let names = vec!["Alice", "Bob", "Charlotte", "Drake"];
        let _names_str: Vec<_> = names.iter().map(|name| (*name).to_string()).collect();
        let edges = vec![("Alice", "Bob"), ("Charlotte", "Bob"), ("Drake", "Bob")];
        let memberships = vec![("Group", names.clone())];

        let mut network = Network::new(&names, &edges, &memberships);
        let group = network.get_group("Group").id;
        let msg0 = message(None, "Blah", vec![]);
        let msg1 = message(None, "Blag", vec![]);
        let msg2 = message(None, "Bling", vec![]);
        let msg3 = message(None, "Black", vec![]);
        let msg_a = network.insert_message("Alice", group, msg0);
        let msg_b = network.insert_message("Bob", group, msg1);
        let msg_c = network.insert_message("Charlotte", group, msg2);
        let msg_d = network.insert_message("Drake", group, msg3);

        network.run_to_all_shared();

        network.assert_msg_fully_shared(&msg_a);
        network.assert_msg_fully_shared(&msg_b);
        network.assert_msg_fully_shared(&msg_c);
        network.assert_msg_fully_shared(&msg_d);
    }

    #[test]
    fn exchange_several_between_two() {
        // Test connection between 4 peers, several messages over 3 groups
        // connections:
        //      1-2,2-3,2-4
        // alternate groups of messages as so:
        //      message M_n in G_(n%|G|)
        init();
        let names = vec!["Angela", "Bingo"];
        let _names_str: Vec<_> = names.iter().map(|name| (*name).to_string()).collect();
        let edges = vec![("Angela", "Bingo")];
        let memberships = vec![("Group", names.clone())];

        let mut network = Network::new(&names, &edges, &memberships);
        let group = network.get_group("Group").id;
        let msg0 = message(None, "Blah", vec![]);
        let msg1 = message(None, "Blag", vec![]);
        let msg2 = message(None, "Bling", vec![]);
        let msg3 = message(None, "Black", vec![]);
        let msg_a = network.insert_message("Angela", group, msg0);
        let msg_b = network.insert_message("Bingo", group, msg1);
        let msg_c = network.insert_message("Angela", group, msg2);
        let msg_d = network.insert_message("Bingo", group, msg3);

        network.run_to_all_shared();

        network.assert_msg_fully_shared(&msg_a);
        network.assert_msg_fully_shared(&msg_b);
        network.assert_msg_fully_shared(&msg_c);
        network.assert_msg_fully_shared(&msg_d);
    }
}
