//! Bramble roles

/// The role a peer plays in a protocol, either Alice or Bob.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Role {
    /// The Alice role.
    Alice,
    /// The Bob role.
    Bob,
}

impl Role {
    /// Gets the opposite role for this one.
    pub fn opposite(&self) -> Role {
        if *self == Self::Alice {
            Self::Bob
        } else {
            Self::Alice
        }
    }
}
