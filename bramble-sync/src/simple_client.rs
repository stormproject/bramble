use crate::sync::*;
use bisetmap::BisetMap;
use bramble_crypto::{Hash, PublicKey};
use log::debug;
use serde::{Deserialize, Serialize};
use serde_json::de::from_slice;
use std::sync::Arc;
use std::time::Duration;

fn simple_client_id() -> ClientId {
    ClientId {
        identifier: String::from("SimpleClient"),
        version: 0,
    }
}

pub struct SimpleClient {
    peer_groups: BisetMap<PublicKey, Hash>,
    cmd_sent_callback: Option<Box<dyn Fn(ClientId)>>,
}

impl SimpleClient {
    pub fn new() -> Self {
        Self {
            peer_groups: BisetMap::new(),
            cmd_sent_callback: None,
        }
    }
    // pub fn get_id() -> ClientId {
    //     simple_client_id()
    // }

    // pub async fn run_to_output(&mut self) -> ClientItem {
    //     while self.controller_output.is_empty() {
    //         trace!("client iteration");
    //         select!(
    //             input_opt = self.receiver.next().fuse() => {
    //                 trace!(target: "client", "client received {:?}", input_opt);
    //                 if let Some(input) = input_opt {
    //                         self.process_input(input);
    //                 }
    //             }
    //         );
    //     }
    //     let output = self.controller_output.pop_front().unwrap();
    //     trace!(target: "client_output", "client output {:?}", output);
    //     output
    // }
    // pub async fn run(&mut self) -> ! {
    //     loop {
    //         let input_opt = self.receiver.next().await;
    //         if let Some(input) = input_opt {
    //             self.process_input(input);
    //         };
    //     }
    //     !
    // }
}

#[derive(Serialize, Clone, Deserialize, Debug)]
pub struct MessageBody {
    pub from: String,
    pub text: String,
    pub dependencies: Vec<Hash>,
}

#[derive(Clone, Debug)]
pub struct ClientMessage {
    pub id: Hash,
    pub body: MessageBody,
    pub group: Hash,
    pub time: Duration,
}

#[derive(Debug)]
pub enum SimpleClientItem {
    Message(ClientMessage),
    Exited,
}

#[derive(Clone)]
pub struct SimpleClientGroup {
    pub id: Hash,
    pub desc: String,
}

impl From<SimpleClientGroup> for Group {
    fn from(value: SimpleClientGroup) -> Self {
        Group::new(&value.desc.into_bytes(), simple_client_id())
    }
}

impl SimpleClientGroup {
    pub fn new(desc: &str) -> Self {
        Arc::new(Group::new(desc.as_bytes(), simple_client_id())).into()
    }
}

impl From<Arc<Message>> for ClientMessage {
    fn from(msg: Arc<Message>) -> ClientMessage {
        Self {
            id: msg.id,
            body: from_slice(&msg.body).unwrap(),
            group: msg.group,
            time: msg.time,
        }
    }
}
impl From<Arc<Message>> for SimpleClientItem {
    fn from(msg: Arc<Message>) -> SimpleClientItem {
        SimpleClientItem::Message(ClientMessage {
            id: msg.id,
            body: from_slice(&msg.body).unwrap(),
            group: msg.group,
            time: msg.time,
        })
    }
}

impl From<Arc<Group>> for SimpleClientGroup {
    fn from(value: Arc<Group>) -> Self {
        SimpleClientGroup {
            id: value.id,
            desc: String::from_utf8(value.descriptor.clone()).unwrap(),
        }
    }
}

impl Client for SimpleClient {
    type Message = SimpleClientItem;
    type Group = SimpleClientGroup;

    fn parse_dependencies(&self, msg: &Arc<Message>) -> bramble_common::Result<Vec<Hash>> {
        let deps_res: Result<Vec<Hash>, serde_json::Error> =
            from_slice(&msg.body).map(|body: MessageBody| body.dependencies);
        match deps_res {
            Ok(deps) => Ok(deps),
            _ => Err(bramble_common::Error::InvalidType),
        }
    }

    // keep two key-value pairs stored:
    // one of group -> vec<peer>, and one of peer -> vec<group>
    fn get_sharing_peers(&self, group: &Hash) -> Vec<PublicKey> {
        self.peer_groups.rev_get(group)
    }
    fn is_sharing_peer_with_group(&self, peer: &PublicKey, group: &Hash) -> bool {
        self.peer_groups.contains(peer, group)
    }
    fn add_peer_to_group(&mut self, peer: &PublicKey, group: &Hash) {
        self.peer_groups.insert(*peer, *group);
    }
    fn remove_peer_from_group(&mut self, peer: &PublicKey, group: &Hash) {
        self.peer_groups.remove(peer, group);
    }

    fn get_sharing_groups(&self, peer: &PublicKey) -> Vec<Hash> {
        self.peer_groups.get(peer)
    }

    fn get_message_destiny(&self, _msg: &Arc<Message>) -> MessageDestiny {
        MessageDestiny::Share
    }

    fn validate_message(&self, msg: &Arc<Message>) -> bramble_common::Result<()> {
        let body: Result<MessageBody, serde_json::Error> = from_slice(&msg.body);
        match body {
            Ok(_json) => Ok(()),
            Err(e) => {
                debug!("Invalid message delivered {:?}", e);
                Err(bramble_common::Error::InvalidType)
            }
        }
    }

    // fn get_id(&self) -> ClientId {
    //     Self::get_id()
    // }
    fn get_id() -> ClientId {
        simple_client_id()
    }
}
