//! BTP keychains

use bramble_crypto::{kdf, SymmetricKey};

/// A set of keys used to encrypt BTP streams
#[derive(Copy, Clone)]
pub struct Keychain {
    tag_key: SymmetricKey,
    header_key: SymmetricKey,
}

impl Keychain {
    /// Creates a new keychain with given tag and header keys
    pub fn new(tag_key: SymmetricKey, header_key: SymmetricKey) -> Self {
        Self {
            tag_key,
            header_key,
        }
    }

    /// Gets the tag key.
    pub fn tag_key(&self) -> &SymmetricKey {
        &self.tag_key
    }

    /// Gets the header key.
    pub fn header_key(&self) -> &SymmetricKey {
        &self.header_key
    }

    /// Gets the next keychain
    pub fn next(&self, period: u64) -> Self {
        Self {
            tag_key: rotate_key(&self.tag_key, period),
            header_key: rotate_key(&self.header_key, period),
        }
    }

    /// Rotates this keychain
    pub fn rotate(&mut self, period: u64) {
        *self = self.next(period);
    }
}

fn rotate_key(key: &SymmetricKey, period: u64) -> SymmetricKey {
    kdf(ROTATE_LABEL, key, &[&period.to_be_bytes()])
}

const ROTATE_LABEL: &[u8] = b"org.briarproject.bramble.transport/ROTATE";
