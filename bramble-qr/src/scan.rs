//! BQP scan payloads

use crate::{Commit, Error, Result, BETA_VERSION, CURRENT_VERSION};
use bramble_crypto::PublicKey;
use bramble_data::{custom::array_as_bytes, Deserializer, Object, ObjectSerializer, Serializer};
use serde::{de, Deserialize, Serialize};
use std::{
    convert::{TryFrom, TryInto},
    io::{Cursor, Read, Write},
    result, slice,
};

/// A QR code scan payload
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(into = "PayloadSerial", try_from = "PayloadSerial")]
pub struct Payload {
    commit: Commit,
    transports: Vec<Descriptor>,
}

#[derive(Serialize, Deserialize)]
#[serde(transparent)]
struct PayloadSerial(Vec<PayloadEntry>);

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
enum PayloadEntry {
    Commit(Commit),
    Transport(Descriptor),
}

impl From<Payload> for PayloadSerial {
    fn from(p: Payload) -> Self {
        let mut v = vec![PayloadEntry::Commit(p.commit)];
        v.extend(p.transports.into_iter().map(PayloadEntry::Transport));
        Self(v)
    }
}

impl TryFrom<PayloadSerial> for Payload {
    type Error = Error;

    fn try_from(p: PayloadSerial) -> Result<Self> {
        let mut iter = p.0.into_iter();
        let commit = match iter.next() {
            Some(PayloadEntry::Commit(commit)) => commit,
            Some(_) => return Err(Error::InvalidCommitment),
            None => return Err(Error::EmptyPayload),
        };
        let transports = iter
            .map(|e| match e {
                PayloadEntry::Transport(d) => Ok(d),
                _ => Err(Error::InvalidTransport),
            })
            .collect::<Result<_>>()?;
        Ok(Self { commit, transports })
    }
}

impl Payload {
    /// Creates a new payload for a given public key and transports.
    pub fn new(pk: &PublicKey, transports: Vec<Descriptor>) -> Self {
        let commit = Commit::derive(pk);
        Self { commit, transports }
    }

    /// Reads a payload from a [`Read`] instance.
    pub fn read_from<R: Read>(r: &mut R) -> Result<Self> {
        let mut version = 0;
        r.read_exact(slice::from_mut(&mut version))?;
        if version == BETA_VERSION {
            return Err(Error::BetaVersion);
        }
        if version < CURRENT_VERSION {
            return Err(Error::OlderVersion);
        }
        if version > CURRENT_VERSION {
            return Err(Error::NewerVersion);
        }
        let mut de = Deserializer::new(r);
        let value = Self::deserialize(&mut de)?;
        de.end()?;
        Ok(value)
    }

    /// Reads a payload from a byte slice.
    pub fn from_bytes(mut slice: &[u8]) -> Result<Self> {
        Self::read_from(&mut slice)
    }

    /// Writes a payload to a [`Write`] instance.
    pub fn write_to<W: Write>(&self, w: &mut W) -> Result<()> {
        w.write_all(&[CURRENT_VERSION])?;
        let mut ser = Serializer::new(w);
        self.serialize(&mut ser)?;
        Ok(())
    }

    /// Writes a payload to a `Vec<u8>`.
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut c = Cursor::new(vec![]);
        self.write_to(&mut c).expect("writing to vec failed");
        c.into_inner()
    }

    /// Gets this payload's commitment.
    pub fn commit(&self) -> Commit {
        self.commit
    }

    /// Gets this payload's transport descriptors.
    pub fn transports(&self) -> &[Descriptor] {
        &self.transports
    }
}

/// A transport descriptor
#[derive(Clone, Debug, PartialEq, Serialize)]
#[serde(untagged)]
pub enum Descriptor {
    /// A Bluetooth transport descriptor
    Bluetooth(BluetoothDescriptor),
    /// A LAN transport descriptor
    Lan(LanDescriptor),
    /// An unknown transport descriptor
    Unknown(UnknownDescriptor),
}

impl Descriptor {
    /// Creates a new Bluetooth transport descriptor without a known address.
    pub fn bluetooth() -> Self {
        Self::Bluetooth(BluetoothDescriptor::new())
    }

    /// Creates a new Bluetooth transport descriptor with a known address.
    pub fn bluetooth_address(address: [u8; BLUETOOTH_ADDRESS_LEN]) -> Self {
        Self::Bluetooth(BluetoothDescriptor::with_address(address))
    }

    /// Creates a new LAN transport descriptor.
    pub fn lan(address: [u8; IPV4_ADDRESS_LEN], port: u16) -> Self {
        Self::Lan(LanDescriptor::new(address, port))
    }

    /// Creates a new unknown transport descriptor.
    pub fn unknown(id: i64, data: Vec<Object>) -> Self {
        Self::Unknown(UnknownDescriptor::new(id, data))
    }
}

/// A Bluetooth transport descriptor
#[derive(Clone, Default, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(into = "UnknownDescriptor", try_from = "UnknownDescriptor")]
pub struct BluetoothDescriptor {
    address: Option<[u8; BLUETOOTH_ADDRESS_LEN]>,
}

impl BluetoothDescriptor {
    /// Creates a new bluetooth transport descriptor without a known address.
    pub fn new() -> Self {
        Self { address: None }
    }

    /// Creates a new bluetooth transport descriptor with a known address.
    pub fn with_address(address: [u8; BLUETOOTH_ADDRESS_LEN]) -> Self {
        Self {
            address: Some(address),
        }
    }

    /// Gets the Bluetooth address, if known.
    pub fn address(&self) -> Option<[u8; BLUETOOTH_ADDRESS_LEN]> {
        self.address
    }
}

impl From<BluetoothDescriptor> for UnknownDescriptor {
    fn from(d: BluetoothDescriptor) -> Self {
        match d.address {
            Some(address) => {
                let address_obj = array_as_bytes::serialize(&address, ObjectSerializer)
                    .expect("failed to convert bluetooth address to object");
                Self::new(BLUETOOTH_ID, vec![address_obj])
            }
            None => Self::new(BLUETOOTH_ID, vec![]),
        }
    }
}

impl TryFrom<UnknownDescriptor> for BluetoothDescriptor {
    type Error = Error;

    fn try_from(d: UnknownDescriptor) -> Result<Self> {
        if d.id != BLUETOOTH_ID {
            return Err(Error::InvalidTransport);
        }

        match d.data {
            Object::List(l) if l.is_empty() => Ok(Self::new()),
            Object::List(mut l) if l.len() == 1 => Ok(Self::with_address(
                array_as_bytes::deserialize(l.remove(0))?,
            )),
            _ => Err(Error::InvalidTransport),
        }
    }
}

/// A LAN transport descriptor
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(into = "UnknownDescriptor", try_from = "UnknownDescriptor")]
pub struct LanDescriptor {
    id: i64,
    address: [u8; IPV4_ADDRESS_LEN],
    port: u16,
}

impl LanDescriptor {
    /// Creates a new LAN transport descriptor.
    pub fn new(address: [u8; IPV4_ADDRESS_LEN], port: u16) -> Self {
        Self {
            id: LAN_ID,
            address,
            port,
        }
    }

    /// Gets the IPv4 address for this transport.
    pub fn address(&self) -> [u8; IPV4_ADDRESS_LEN] {
        self.address
    }

    /// Gets the port for this transport.
    pub fn port(&self) -> u16 {
        self.port
    }
}

impl From<LanDescriptor> for UnknownDescriptor {
    fn from(d: LanDescriptor) -> Self {
        let address_obj = array_as_bytes::serialize(&d.address, ObjectSerializer)
            .expect("failed to convert IPv4 address to object");
        let port_obj = d
            .port
            .serialize(ObjectSerializer)
            .expect("failed to convert IPv4 address to object");
        Self::new(LAN_ID, vec![address_obj, port_obj])
    }
}

impl TryFrom<UnknownDescriptor> for LanDescriptor {
    type Error = Error;

    fn try_from(d: UnknownDescriptor) -> Result<Self> {
        if d.id != LAN_ID {
            return Err(Error::InvalidTransport);
        }

        let mut list = d.data.to_list()?;
        if list.len() != 2 {
            return Err(Error::InvalidTransport);
        }
        let port_obj = list.remove(1);
        let address_obj = list.remove(0);
        Ok(Self {
            id: LAN_ID,
            address: array_as_bytes::deserialize(address_obj)?,
            port: u16::deserialize(port_obj)?,
        })
    }
}

/// An unknown transport descriptor
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(into = "UnknownDescriptorSerial", try_from = "UnknownDescriptorSerial")]
pub struct UnknownDescriptor {
    id: i64,
    data: Object,
}

impl UnknownDescriptor {
    /// Creates a new unknown transport with the given identifier and described by the given data.
    pub fn new(id: i64, data: Vec<Object>) -> Self {
        Self {
            id,
            data: Object::List(data),
        }
    }

    /// Gets this transport's identifier.
    pub fn id(&self) -> i64 {
        self.id
    }

    /// Gets an object that describes this transport.
    pub fn data(&self) -> &Object {
        &self.data
    }
}

#[derive(Serialize, Deserialize)]
#[serde(transparent)]
struct UnknownDescriptorSerial(Vec<Object>);

impl From<UnknownDescriptor> for UnknownDescriptorSerial {
    fn from(d: UnknownDescriptor) -> Self {
        let mut v = vec![Object::Integer(d.id)];
        match d.data {
            Object::List(l) => v.extend(l.into_iter()),
            _ => unreachable!(),
        }
        Self(v)
    }
}

impl TryFrom<UnknownDescriptorSerial> for UnknownDescriptor {
    type Error = Error;

    fn try_from(mut d: UnknownDescriptorSerial) -> Result<Self> {
        let id = d.0.remove(0).to_integer()?;
        Ok(Self {
            id,
            data: Object::List(d.0),
        })
    }
}

impl<'de> Deserialize<'de> for Descriptor {
    fn deserialize<D>(de: D) -> result::Result<Self, D::Error>
    where
        D: de::Deserializer<'de>,
    {
        let error_map =
            |_| de::Error::invalid_type(de::Unexpected::Other("not bluetooth"), &"bluetooth");
        let unknown = UnknownDescriptor::deserialize(de)?;
        Ok(match unknown.id {
            BLUETOOTH_ID => Descriptor::Bluetooth(unknown.try_into().map_err(error_map)?),
            LAN_ID => Descriptor::Lan(unknown.try_into().map_err(error_map)?),
            _ => Descriptor::Unknown(unknown),
        })
    }
}

const BLUETOOTH_ID: i64 = 0;
const LAN_ID: i64 = 1;

const BLUETOOTH_ADDRESS_LEN: usize = 6;
const IPV4_ADDRESS_LEN: usize = 4;

#[cfg(test)]
mod test {
    use super::*;
    use bramble_data::{from_object, to_object};
    use hex_literal::hex;

    #[test]
    fn payload_read_rejects_older_versions() {
        let mut buf: &[u8] = &hex!("03");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
        let e = e.unwrap_err();
        assert!(matches!(e, Error::OlderVersion));
    }

    #[test]
    fn payload_read_rejects_beta_version() {
        let mut buf: &[u8] = &hex!("59");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
        let e = e.unwrap_err();
        assert!(matches!(e, Error::BetaVersion));
    }

    #[test]
    fn payload_read_rejects_newer_version() {
        let mut buf: &[u8] = &hex!("05");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
        let e = e.unwrap_err();
        assert!(matches!(e, Error::NewerVersion));
    }

    #[test]
    fn payload_read_rejects_non_list() {
        let mut buf: &[u8] = &hex!("04 2180");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
    }

    #[test]
    fn payload_read_rejects_empty_list() {
        let mut buf: &[u8] = &hex!("04 6080");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
    }

    #[test]
    fn payload_read_rejects_data_after_list() {
        let mut buf: &[u8] = &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 21FF 80 80 00");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
    }

    #[test]
    fn payload_read_rejects_short_commit() {
        let mut buf: &[u8] = &hex!("04 60 510F000102030405060708090A0B0C0D0E 60 21FF 80 80");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
    }

    #[test]
    fn payload_read_rejects_long_commit() {
        let mut buf: &[u8] = &hex!("04 60 5111000102030405060708090A0B0C0D0E0F10 60 21FF 80 80");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
    }

    #[test]
    fn payload_read_accepts_without_transports() {
        let expected = Commit::from(hex!("000102030405060708090A0B0C0D0E0F"));
        let mut buf: &[u8] = &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 80");
        let p = Payload::read_from(&mut buf);
        assert!(p.is_ok());
        let p = p.unwrap();
        assert_eq!(p.commit(), expected);
        assert!(p.transports().is_empty());
    }

    #[test]
    fn payload_read_rejects_bluetooth_with_bad_address() {
        let mut buf: &[u8] =
            &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2100 220101 80 80");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
    }

    #[test]
    fn payload_read_accepts_bluetooth_without_address() {
        let expected = Commit::from(hex!("000102030405060708090A0B0C0D0E0F"));
        let mut buf: &[u8] = &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2100 80 80");
        let p = Payload::read_from(&mut buf);
        assert!(p.is_ok());
        let p = p.unwrap();
        assert_eq!(p.commit(), expected);
        assert_eq!(p.transports().len(), 1);
        assert_eq!(p.transports()[0], Descriptor::bluetooth());
    }

    #[test]
    fn payload_read_accepts_bluetooth_with_address() {
        let expected = Commit::from(hex!("000102030405060708090A0B0C0D0E0F"));
        let mut buf: &[u8] =
            &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2100 5106000102030405 80 80");
        let p = Payload::read_from(&mut buf);
        assert!(p.is_ok());
        let p = p.unwrap();
        assert_eq!(p.commit(), expected);
        assert_eq!(p.transports().len(), 1);
        assert_eq!(
            p.transports()[0],
            Descriptor::bluetooth_address([0, 1, 2, 3, 4, 5])
        );
    }

    #[test]
    fn payload_read_rejects_lan_with_bad_address() {
        let mut buf: &[u8] =
            &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2101 220101 80 80");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
    }

    #[test]
    fn payload_read_rejects_lan_without_address() {
        let mut buf: &[u8] = &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2101 80 80");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
    }

    #[test]
    fn payload_read_rejects_lan_without_port() {
        let mut buf: &[u8] =
            &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2101 510401020304 80 80");
        let e = Payload::read_from(&mut buf);
        assert!(e.is_err());
    }

    #[test]
    fn payload_read_accepts_lan_with_address_and_port() {
        let expected = Commit::from(hex!("000102030405060708090A0B0C0D0E0F"));
        let mut buf: &[u8] =
            &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2101 510401020304 221020 80 80");
        let p = Payload::read_from(&mut buf);
        assert!(p.is_ok());
        let p = p.unwrap();
        assert_eq!(p.commit(), expected);
        assert_eq!(p.transports().len(), 1);
        assert_eq!(p.transports()[0], Descriptor::lan([1, 2, 3, 4], 4128));
    }

    #[test]
    fn payload_read_accepts_unknown_transports() {
        let expected = Commit::from(hex!("000102030405060708090A0B0C0D0E0F"));
        let mut buf: &[u8] =
            &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2159 2101 2102 80 80");
        let p = Payload::read_from(&mut buf);
        assert!(p.is_ok());
        let p = p.unwrap();
        assert_eq!(p.commit(), expected);
        assert_eq!(p.transports().len(), 1);
        assert_eq!(
            p.transports()[0],
            Descriptor::unknown(89, vec![Object::Integer(1), Object::Integer(2)])
        );
    }

    #[test]
    fn payload_read_accepts_multiple_transports() {
        let expected = Commit::from(hex!("000102030405060708090A0B0C0D0E0F"));
        let mut buf: &[u8] =
                &hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2101 510401020304 221020 80 60 2100 80 60 2159 220000 80 80");
        let p = Payload::read_from(&mut buf);
        assert!(p.is_ok());
        let p = p.unwrap();
        assert_eq!(p.commit(), expected);
        assert_eq!(p.transports().len(), 3);
        assert_eq!(p.transports()[0], Descriptor::lan([1, 2, 3, 4], 4128));
        assert_eq!(p.transports()[1], Descriptor::bluetooth());
        assert_eq!(
            p.transports()[2],
            Descriptor::unknown(89, vec![Object::Integer(0)])
        );
    }

    #[test]
    fn payload_to_bytes() {
        let p = Payload {
            commit: Commit::from(hex!("000102030405060708090A0B0C0D0E0F")),
            transports: vec![
                Descriptor::lan([1, 2, 3, 4], 4128),
                Descriptor::bluetooth(),
                Descriptor::bluetooth_address([1, 2, 3, 4, 5, 6]),
                Descriptor::unknown(89, vec![Object::Integer(17)]),
            ],
        };
        let expected = hex!("04 60 5110000102030405060708090A0B0C0D0E0F 60 2101 510401020304 221020 80 60 2100 80 60 2100 5106010203040506 80 60 2159 2111 80 80");
        assert_eq!(p.to_bytes(), expected);
    }

    #[test]
    fn payload_roundtrips() {
        let input = Payload {
            commit: Commit::from(hex!("000102030405060708090A0B0C0D0E0F")),
            transports: vec![
                Descriptor::lan([1, 2, 3, 4], 4128),
                Descriptor::bluetooth(),
                Descriptor::bluetooth_address([1, 2, 3, 4, 5, 6]),
                Descriptor::unknown(89, vec![Object::Integer(17)]),
            ],
        };
        let obj = to_object(input.clone()).unwrap();
        let output = from_object(obj).unwrap();

        assert_eq!(input, output);
    }
}

// TODO zero-copy serialization
