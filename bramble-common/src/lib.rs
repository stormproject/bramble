//! Common Bramble types
#![warn(missing_docs)]

mod duplex;
mod error;
mod timer;

pub mod transport;
pub use duplex::{make_duplex, make_pipe, Duplex, ReadPipe, WritePipe};
pub use error::{Error, Result};
pub use timer::{sleep, timeout};
