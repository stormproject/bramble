extern crate r2d2_sqlite;
use crate::storage::rusqlite::params;
use crate::sync::*;
use bramble_crypto::KEY_LEN;
use bramble_crypto::{Hash as ID, PublicKey};
use fallible_iterator::FallibleIterator;
use futures::channel::mpsc::{unbounded, UnboundedReceiver, UnboundedSender};
use futures::channel::oneshot::Sender;
use futures::future::Fuse;
use futures::select;
use futures::Future;
use futures::FutureExt;
use futures::{executor, StreamExt};
use log::trace;
use quick_cache::unsync::Cache;
use r2d2;
use r2d2::{Pool, PooledConnection};
use r2d2_sqlite::rusqlite;
use r2d2_sqlite::SqliteConnectionManager;
use rusqlite::types::Value;
use rusqlite::OptionalExtension;
use rusqlite::{ffi as libsqlite, Result, Row};
use std::path::Path;
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;
use threadpool::ThreadPool;
use StorageOperation::*;
type Conn = PooledConnection<SqliteConnectionManager>;

const GROUP_COLS: &str = "g.id, g.descriptor, g.client_id, g.client_version";
impl<'conn> From<&Row<'conn>> for Group {
    fn from(row: &Row<'conn>) -> Group {
        let client = ClientId::new(row.get_unwrap(2), row.get_unwrap(3));
        let desc = row.get_unwrap(1);

        Group {
            id: row.get_unwrap(0),
            descriptor: desc,
            client,
        }
    }
}

const MESSAGE_COLS: &str =
    "m.id, m.msg_group, m.body, m.time, m.sharing, m.delivered, g.client_id, g.client_version";
impl<'conn> From<&rusqlite::Row<'conn>> for Message {
    fn from(row: &Row<'conn>) -> Message {
        Message {
            id: row.get_unwrap(0),
            group: row.get_unwrap(1),
            body: row.get_unwrap(2),
            time: Duration::from_secs(row.get_unwrap(3)),
            sharing: row.get_unwrap(4),
            delivered: row.get_unwrap(5),
        }
    }
}
impl<'conn> From<&rusqlite::Row<'conn>> for ClientMessage {
    fn from(row: &Row<'conn>) -> ClientMessage {
        ClientMessage {
            message: Arc::new(Message {
                id: row.get_unwrap(0),
                group: row.get_unwrap(1),
                body: row.get_unwrap(2),
                time: Duration::from_secs(row.get_unwrap(3)),
                sharing: row.get_unwrap(4),
                delivered: row.get_unwrap(5),
            }),
            client: ClientId::new(row.get_unwrap(6), row.get_unwrap(7)),
        }
    }
}
impl<'conn> From<&rusqlite::Row<'conn>> for SyncState {
    fn from(row: &Row<'conn>) -> SyncState {
        SyncState {
            message: row.get_unwrap(0),
            peer: row.get_unwrap::<usize, [u8; KEY_LEN]>(1).into(),
            seen: row.get_unwrap(2),
            ack: row.get_unwrap(3),
            requested: row.get_unwrap(4),
            send_count: row.get_unwrap(5),
            send_time: row.get_unwrap(6),
            max_latency: row.get_unwrap(7),
        }
    }
}

//impl From<ID> for Value {
//fn from(arr: ID) -> Value {
//Value::Blob(&arr.to_vec())
//}
//}

fn key_to_sql_value(peer: &PublicKey) -> Value {
    Value::Blob(peer.as_ref().to_vec())
}
//impl ToSql for Vec<ID> {
//fn to_sql(&self) -> rusqlite::Result<rusqlite::types::ToSqlOutput<'_>> {
//self.iter().map(|id| self.0.to_sql())
//}
//}

fn ids_to_rarray(ids: &Vec<ID>) -> Rc<Vec<Value>> {
    Rc::new(
        ids.iter()
            .map(|id| Value::from(id.as_ref().to_vec()))
            .collect::<Vec<Value>>(),
    )
}
fn keys_to_rarray(keys: &Vec<PublicKey>) -> Rc<Vec<Value>> {
    Rc::new(keys.iter().map(key_to_sql_value).collect::<Vec<Value>>())
}

fn collect_ids<P: rusqlite::Params>(mut stmt: rusqlite::Statement, params: P) -> Vec<ID> {
    stmt.query(params)
        .unwrap()
        .map(|row| Ok(row.into()))
        .collect()
        .unwrap()
}

struct Caches {
    groups: Cache<ID, Arc<Group>>,
    messages: Cache<ID, ClientMessage>,
}

macro_rules! cache {
    ($opt:ident, $code:block) => {
        if let Some(ref mut $opt) = $opt.lock().unwrap().caches {
            $code
        }
    };
}

impl Caches {
    fn new() -> Self {
        Self {
            groups: Cache::new(10, 100),
            messages: Cache::new(10, 100),
        }
    }
    fn check(cache: &mut Option<Caches>, op: StorageOperation) -> Option<StorageOperation> {
        if let Some(cache) = cache {
            return match op {
                GetGroup(id, sender) => {
                    let cached = cache.get_group(&id);
                    let hit = cached.is_some();
                    if hit {
                        sender.send(cached).unwrap();
                        None
                    } else {
                        Some(GetGroup(id, sender))
                    }
                }
                GetMessage(id, sender) => {
                    let cached = cache.get_message(&id);
                    let hit = cached.is_some();
                    if hit {
                        sender.send(cached).unwrap();
                        None
                    } else {
                        Some(GetMessage(id, sender))
                    }
                }
                DeleteGroup(id, _) => {
                    cache.groups.remove(&id);
                    Some(op)
                }
                InvalidatingDelete(id, _) => {
                    cache.messages.remove(&id);
                    Some(op)
                }
                ValidDelete(id, _) => {
                    cache.messages.remove(&id);
                    Some(op)
                }
                SetMessageDelivered(id, _) => {
                    if let Some(msg) = cache.messages.get(&id) {
                        if msg.delivered {
                            None
                        } else {
                            cache.messages.remove(&id);
                            Some(op)
                        }
                    } else {
                        Some(op)
                    }
                }
                _ => Some(op),
            };
        } else {
            Some(op)
        }
    }
    fn delete_group(cache: &mut Arc<Mutex<ThreadResources>>, group: &ID) {
        // if let Some(mut cache) = get_cache(cache) {
        cache!(cache, {
            cache.groups.remove(group);
        })
        // }
    }
    fn delete_message(cache: &mut Arc<Mutex<ThreadResources>>, message: &ID) {
        cache!(cache, {
            cache.messages.remove(message);
        })
    }
    fn insert_group(cache: &mut Arc<Mutex<ThreadResources>>, group: &Arc<Group>) {
        cache!(cache, {
            cache.groups.insert(group.id, group.clone());
        })
    }
    fn insert_message(cache: &mut Arc<Mutex<ThreadResources>>, message: &ClientMessage) {
        cache!(cache, {
            cache.messages.insert(message.id, message.clone());
        })
    }
    fn insert_message_or_replace(
        cache: &mut Arc<Mutex<ThreadResources>>,
        message: &mut ClientMessage,
    ) {
        cache!(cache, {
            if let Some(cached) = cache.messages.get(&message.id) {
                *message = cached.clone();
            } else {
                cache.messages.insert(message.id, message.clone())
            }
        })
    }
    fn get_message(&self, id: &ID) -> Option<ClientMessage> {
        Some(self.messages.get(id)?.clone())
    }
    fn get_group(&self, id: &ID) -> Option<Arc<Group>> {
        Some(self.groups.get(id)?.clone())
    }
}

enum StorageAction {
    Terminate,
    Execute,
    Nothing,
}

pub struct ThreadResources {
    workers: ThreadPool,
    conn_pool: Pool<SqliteConnectionManager>,
    caches: Option<Caches>,
}

impl ThreadResources {
    fn new(file: &str, caches: Option<Caches>) -> Result<Self> {
        //println!("{:?}", std::env::current_dir());
        let exists = Path::new(file).exists();
        let manager = SqliteConnectionManager::file(file);
        let conn_pool = Pool::builder().build(manager).unwrap();
        if !exists {
            let conn = conn_pool.get().unwrap();
            Self::init_db(conn);
        }
        rusqlite::vtab::array::load_module(&conn_pool.get().unwrap())?;
        Ok(Self {
            workers: ThreadPool::new(MAX_WORKERS),
            conn_pool,
            caches,
        })
    }
    pub fn init_db(conn: Conn) {
        match conn.execute(
            "
        CREATE TABLE msg_groups (
                id BLOB PRIMARY KEY, 
                descriptor BLOB,
                client_id TEXT,
                client_version INTEGER
                );",
            (),
        ) {
            Ok(_res) => {}
            Err(_) => {}
        }
        let _ = conn.execute(
            "CREATE INDEX by_client ON msg_groups (client_id, client_version);",
            (),
        );
        let _ = conn.execute(
            "
        CREATE TABLE messages (
                id BLOB PRIMARY KEY, 
                msg_group BLOB,
                body BLOB,
                time DATETIME,
                sharing BOOLEAN,
                delivered BOOLEAN,
                FOREIGN KEY(msg_group) REFERENCES msg_groups(id) ON DELETE CASCADE
                );",
            (),
        );
        let _ = conn.execute("CREATE INDEX by_group ON messages (msg_group, time);", ());
        let _ = conn.execute(
            "
        CREATE TABLE edges (
                dependent BLOB,
                dependency BLOB
                );",
            (),
        );
        let _ = conn.execute("CREATE INDEX dependents ON edges (dependent);", ());
        let _ = conn.execute("CREATE INDEX dependencies ON edges (dependency);", ());
        let _ = conn.execute(
            "
        CREATE TABLE sync_states (
                message BLOB,
                peer BLOB,
                seen BOOLEAN,
                ack BOOLEAN,
                requested BOOLEAN,
                send_count INTEGER,
                send_time DATETIME,
                eta DATETIME,
                PRIMARY KEY(message, peer),
                FOREIGN KEY(message) REFERENCES messages(id) ON DELETE CASCADE
                );",
            (),
        );
        let _ = conn.execute(
            "CREATE INDEX send_time_reached ON sync_states (seen, send_time);",
            (),
        );
        let _ = conn.execute("CREATE INDEX eta_reduced ON sync_states (seen, eta);", ());
    }
}

pub struct ClientStorage<C: Client> {
    sender: UnboundedSender<ClientStorageOperation<C>>,
}

macro_rules! client_storage_op {
    ($storage:expr, $operation:path, $($arg:tt)+) => {
        {
        use ClientStorageOperation::*;
        let (send, recv) = futures::channel::oneshot::channel();
        $storage.sender.unbounded_send($operation($($arg)+, send)).unwrap();
        recv.await.unwrap()
        }
    };
    ($storage:expr, $operation:path) => {
        {
        use ClientStorageOperation::*;
        let (send, recv) = futures::channel::oneshot::channel();
        $storage.sender.unbounded_send( $operation(send)).unwrap();
        recv.await.unwrap()
        }
    };
}

impl<C: Client> ClientStorage<C> {
    pub async fn get_groups(&mut self) -> Vec<C::Group> {
        client_storage_op!(self, GetGroups)
    }
    pub async fn get_group(&mut self, id: ID) -> Option<C::Group> {
        client_storage_op!(self, GetGroup, id)
    }
    pub async fn get_group_top_messages(&mut self, id: ID) -> Vec<C::Message> {
        client_storage_op!(self, GetGroupTopMessages, id)
    }
    pub async fn get_message_descendents(&mut self, id: ID, limit: u32) -> Vec<C::Message> {
        client_storage_op!(self, GetMessageDescendents, id, limit)
    }
}

pub struct SqliteClientStorage<C: Client> {
    thread_resources: Arc<Mutex<ThreadResources>>,
    client_receiver: UnboundedReceiver<ClientStorageOperation<C>>,
}
impl<C: Client + 'static> SqliteClientStorage<C>
where
    <C as Client>::Group: Send,
    <C as Client>::Message: Send,
{
    async fn run(mut self) {
        loop {
            select! {
                op_opt = self.client_receiver.next().fuse() => {
                    if let Some(op) = op_opt {
                        self.execute_client_operation(op);
                    }else {
                        break
                    }
                }
            }
        }
    }
    fn execute_client_operation(&mut self, op: ClientStorageOperation<C>) {
        use ClientStorageOperation::*;
        let resources_copy = self.thread_resources.clone();
        let resources = self.thread_resources.lock().unwrap();

        // if let Some(op) = Caches::check(&mut resources.caches, op) {
        resources.workers.execute(move || {
            let conn;
            {
                conn = resources_copy.lock().unwrap().conn_pool.get().unwrap();
                conn.busy_handler(Some(|_| true)).unwrap();
                rusqlite::vtab::array::load_module(&conn).unwrap();
            }
            // let cache = &mut resources_copy;
            match op {
                GetGroups(sender) => Self::get_client_groups(conn, sender),
                GetGroup(id, sender) => Self::get_client_group(conn, id, sender),
                GetGroupTopMessages(id, sender) => {
                    Self::get_client_group_top_messages(conn, id, sender)
                }
                GetMessageDescendents(id, limit, sender) => {
                    Self::get_client_message_descendents(conn, id, limit, sender)
                }
            }
            .unwrap();
        })
        // }
    }
    fn get_client_groups(conn: Conn, sender: Sender<Vec<C::Group>>) -> rusqlite::Result<()> {
        let client = C::get_id();
        let mut stmt = conn.prepare(
            "
        SELECT * FROM msg_groups
        WHERE client_id = ? AND client_version = ?;",
        )?;
        let groups: Vec<C::Group> = stmt
            .query_map(params![client.identifier, client.version], |row| {
                Ok(Arc::new(Group::from(row)))
            })?
            .map(|res| C::Group::from(res.unwrap()))
            .collect();
        let _ = sender.send(groups);
        Ok(())
    }
    fn get_client_group(
        conn: Conn,
        id: ID,
        sender: Sender<Option<C::Group>>,
    ) -> rusqlite::Result<()> {
        let client = C::get_id();
        let opt_group = conn
            .query_row(
                "
        SELECT * FROM msg_groups
        WHERE id = ? AND client_id = ? AND client_version = ?;",
                params![id, client.identifier, client.version],
                |row| Ok(C::Group::from(Arc::new(row.into()))),
            )
            .optional()?;
        let _ = sender.send(opt_group);
        Ok(())
    }
    fn get_client_group_top_messages(
        conn: Conn,
        id: ID,
        sender: Sender<Vec<C::Message>>,
    ) -> rusqlite::Result<()> {
        let client = C::get_id();
        // TODO: This runs in O(|m|) time, better to have edges with null dependencies, and query with
        // an index on edges
        // This would be faster where the number of messages with dependencies is larger than the total
        // number of messages
        let mut stmt = conn.prepare(&format!(
            "
SELECT {MESSAGE_COLS} FROM msg_groups as g
JOIN messages as m
    ON g.id = m.msg_group
WHERE 
    NOT EXISTS
    (
        SELECT * FROM edges as e
        WHERE dependency = m.id
    )
    AND 
        g.id = ?
    AND
        client_id = ? AND client_version = ?;"
        ))?;
        let messages: Vec<C::Message> = stmt
            .query_map(params![id, client.identifier, client.version], |row| {
                Ok(Arc::new(Message::from(row)).into())
            })?
            .map(|res| res.unwrap())
            .collect();
        let _ = sender.send(messages);
        Ok(())
    }
    fn get_client_message_descendents(
        conn: Conn,
        id: ID,
        limit: u32,
        sender: Sender<Vec<C::Message>>,
    ) -> rusqlite::Result<()> {
        // Ordering by time, get up to LIMIT messages that are children of the ID message, also
        // those messages descendents and ancestors
        // HOW?
        // select limit entries from messages order by time, where exists parent with ID.
        //  - this will get the children
        // join
        let client = C::get_id();
        let children_query = if limit > 0 {
            format!(
                "
WITH Children as (
    SELECT {MESSAGE_COLS}
    FROM messages as m
    JOIN edges as e
        ON e.dependent = m.id
    WHERE e.dependency = ?
    ORDER BY m.time desc
    LIMIT ?
)
"
            )
        } else {
            format!(
                "
WITH Children as (
    SELECT {MESSAGE_COLS}
    FROM messages as m
    JOIN edges as e
        ON e.dependent = m.id
    WHERE e.dependency = ?
)
"
            )
        };
        // This following query is fucked. By far the most complex SQL I have written
        let mut stmt = conn.prepare(&format!(
            "
{children_query}

WITH RECURSIVE descendents(x) AS (
    SELECT * FROM Children
    UNION
    SELECT dependent
    FROM edges
    JOIN descendents ON dependency = x
)

WITH RECURSIVE ancestors(x) AS (
    SELECT * FROM Children
    UNION
    SELECT dependency
    FROM edges
    JOIN ancestors ON dependent = x
)
SELECT {MESSAGE_COLS}
FROM (
    SELECT * FROM ancestors
    UNION
    SELECT * FROM descendents
)
JOIN messages as m
    ON x = m.id
JOIN msg_groups as g
    ON g.id = m.msg_group
WHERE 
        client_id = ? AND client_version = ?
ORDER BY m.time ASC;"
        ))?;
        let messages: Vec<C::Message> = if limit > 0 {
            stmt.query_map(
                params![id, limit, client.identifier, client.version],
                |row| Ok(Arc::new(Message::from(row)).into()),
            )?
            .map(|res| res.unwrap())
            .collect()
        } else {
            stmt.query_map(params![id, client.identifier, client.version], |row| {
                Ok(Arc::new(Message::from(row)).into())
            })?
            .map(|res| res.unwrap())
            .collect()
        };
        let _ = sender.send(messages);
        Ok(())
    }
}

pub struct SqliteSyncStorage {
    thread_resources: Arc<Mutex<ThreadResources>>,
}

fn fuse_if<F: Future>(condition: bool, f: F) -> Fuse<F> {
    if condition {
        Fuse::terminated()
    } else {
        f.fuse()
    }
}

const MAX_WORKERS: usize = 10;
impl SqliteSyncStorage {
    pub fn new(file: &str, use_cache: bool) -> rusqlite::Result<Self> {
        let caches = use_cache.then(Caches::new);
        let thread_resources = Arc::new(Mutex::new(ThreadResources::new(file, caches)?));
        Ok(Self { thread_resources })
    }
    pub fn init_client<C: Client + 'static>(&mut self) -> ClientStorage<C>
    where
        <C as Client>::Group: Send,
        <C as Client>::Message: Send,
    {
        let (sender, client_receiver) = unbounded();
        let thread_resources = self.thread_resources.clone();
        let _ = thread::spawn(move || {
            let storage: SqliteClientStorage<C> = SqliteClientStorage {
                thread_resources,
                client_receiver,
            };
            executor::block_on(storage.run())
        });
        ClientStorage { sender }
    }
    pub fn init(
        self,
    ) -> (
        UnboundedSender<StorageOperation>,
        std::thread::JoinHandle<()>,
    ) {
        let (sync_sender, sync_receiver) = unbounded();
        let storage_handle = thread::spawn(move || {
            executor::block_on(self.run(sync_receiver));
        });
        (sync_sender, storage_handle)
    }
    async fn run(mut self, mut recv: UnboundedReceiver<StorageOperation>) {
        loop {
            select! {
                op_opt = recv.next().fuse() => {
                    if let Some(op) = op_opt {
                        if op.is_terminate() {
                            self.thread_resources.lock().unwrap().workers.join();
                            break
                        }
                        self.execute_operation(op);
                    }
                }
            }
        }
    }
    fn execute_operation(&mut self, op: StorageOperation) {
        trace!(target: "storage", "{}",&op.desc());
        use StorageOperation::*;
        let mut resources_copy = self.thread_resources.clone();
        let mut resources = self.thread_resources.lock().unwrap();

        if let Some(op) = Caches::check(&mut resources.caches, op) {
            resources.workers.execute(move || {
                let conn;
                {
                    conn = resources_copy.lock().unwrap().conn_pool.get().unwrap();
                    conn.busy_handler(Some(|_| true)).unwrap();
                    rusqlite::vtab::array::load_module(&conn).unwrap();
                }
                let cache = &mut resources_copy;
                match op {
                    // GetMessageGroup(id) => get_message_group(conn, id),
                    GetGroup(id, send) => get_group(cache, conn, id, send),
                    InsertMessage(msg, ids, delivered, send) => {
                        insert_message(cache, conn, msg, &ids, delivered, send)
                    }
                    NewGroup(group, send) => new_group(cache, conn, &group, send),
                    DeleteGroup(id, send) => delete_group(cache, conn, &id, send),
                    DeleteGroupSyncStatesForPeer(id, peer, send) => {
                        delete_group_sync_state_for_peer(conn, &id, &peer, send)
                    }
                    // GetReadySyncStates(peer, latency) => get_ready_sync_states(conn, peer, latency),
                    GetSyncState(id, peer, send) => get_sync_state(conn, &id, &peer, send),
                    // GetSyncStates(ids, peer) => get_sync_states(conn, ids, peer),
                    UpdateSyncState(state, send) => update_sync_state(conn, &state, send),
                    CreateSyncState(state, send) => create_sync_state(conn, &state, send),
                    GetMessage(id, send) => get_message(cache, conn, &id, send),

                    GetAllGroupMessages(id, send) => get_all_group_messages(conn, &id, send),
                    //GetGroupSharingMessages(id) => get_group_sharing_messages(conn, id),
                    //GetGroupSharingMessages(id) => {}
                    // GetGroupsSharingMessages(ids, sender) => {
                    //     get_groups_sharing_messages(conn, ids, *sender)
                    // }
                    //GetAllGroupsSharingMessages => {}
                    // SetSyncStateAck(id, peer, acked) => set_sync_state_ack(conn, id, peer, acked),
                    // SetSyncStateSeen(id, peer, seen) => set_sync_state_seen(conn, id, peer, seen),
                    // SetSyncStateRequested(id, peer, requested) => {
                    //     set_sync_state_requested(conn, id, peer, requested)
                    // }
                    // SetSyncStateSendData(state) => set_sync_state_send_data(conn, state),
                    // HasMessages(ids) => has_messages(conn, ids),
                    InvalidatingDelete(id, send) => invalidating_delete(cache, conn, &id, send),
                    ValidDelete(id, send) => valid_delete(cache, conn, &id, send),
                    HasAllDependenciesDelivered(id, send) => {
                        has_all_dependencies_delivered(conn, &id, send)
                    }
                    GetDependents(id, send) => get_dependents(conn, &id, send),
                    SetMsgSharing(id, sharing, send) => set_msg_sharing(conn, &id, &sharing, send),
                    // SetMsgsSharing(ids) => set_msgs_sharing(conn, ids),
                    SetMessageDelivered(id, send) => set_msg_delivered(conn, &id, send),
                    AllDependenciesShared(id, send) => all_deps_shared(conn, &id, send),
                    // GetNonSharingDependencies(id) => get_non_sharing_dependencies(conn, id),
                    // FullyShared(peers) => get_is_fully_shared(&conn, peers),
                    Terminate => {
                        panic!()
                    }
                    GetGroupsSharingMessagesUnseenByPeer(ids, peer, send) => {
                        get_groups_messages_unseen_by_peer(cache, conn, ids, peer, send)
                    }
                    GetDeliverableMessages(send) => get_deliverable_messages(cache, conn, send),
                }
                .unwrap();
            });
        }
    }
}

fn get_deliverable_messages(
    cache: &mut Arc<Mutex<ThreadResources>>,
    conn: Conn,
    sender: Sender<Vec<ClientMessage>>,
) -> std::result::Result<(), rusqlite::Error> {
    // Get messages that have all dependencies delivered and which have not themselves been
    // delivered
    let mut stmt = conn.prepare(&format!(
        "
        SELECT
            {MESSAGE_COLS}
        FROM
            messages as m
        JOIN edges
            ON dependent = m.id 
        JOIN messages as m2
            ON dependency = m2.id
        JOIN msg_groups as g
            ON m.msg_group = g.id;
        WHERE m.delivered = 0 
        WHERE m2.delivered = 1
    "
    ))?;
    let messages: Vec<ClientMessage> = stmt
        .query_map([], |row| Ok(ClientMessage::from(row)))?
        .map(|res| res.unwrap())
        .map(|mut message| {
            Caches::insert_message_or_replace(cache, &mut message);
            message
        })
        .collect();
    sender.send(messages).unwrap();
    Ok(())
}

fn get_groups_messages_unseen_by_peer(
    cache: &mut Arc<Mutex<ThreadResources>>,
    conn: PooledConnection<SqliteConnectionManager>,
    ids: Vec<ID>,
    peer: PublicKey,
    sender: Sender<Vec<ClientMessage>>,
) -> std::result::Result<(), rusqlite::Error> {
    // get messages in the groups that are sharing, and which don't have seen on a sync state
    let ids_rarray = ids_to_rarray(&ids);
    let mut stmt = conn.prepare(&format!(
        "
        SELECT
            {MESSAGE_COLS}
        FROM 
            messages as m
        JOIN msg_groups as g
            ON g.id = m.msg_group
        LEFT JOIN sync_states as s
            ON ( s.message = m.id AND 
                 s.peer = ? )
        WHERE
            ( s.seen != 1 OR s.seen IS NULL ) AND
            m.sharing = 1 AND
            g.id IN rarray(?)
        ;"
    ))?;
    let messages: Vec<ClientMessage> = stmt
        .query_map(params![peer, &ids_rarray], |row| {
            Ok(ClientMessage::from(row))
        })?
        .map(|res| res.unwrap())
        .map(|mut message| {
            Caches::insert_message_or_replace(cache, &mut message);
            message
        })
        .collect();
    sender.send(messages).unwrap();
    Ok(())
}

// fn get_message_group(conn: Conn, id: &ID) -> rusqlite::Result<()> {
//     let opt = conn.query_row(
//         "SELECT * FROM msg_groups AS g JOIN messages AS m ON g.id = m.msg_group WHERE m.id = ?;",
//         [id],
//         |row| Ok(Group::from(row)),
//     )
//         .optional()?;
//     //return Ok(SyncTypes::Group(group.into()));
//     return match opt {
//         Some(group) => Ok(group.into()),
//         None => {
//             debug!("Missing groups for message {:?}", id);
//             Ok(SyncTypes::Missing)
//         }
//     };
// }
fn get_group(
    cache: &mut Arc<Mutex<ThreadResources>>,
    conn: Conn,
    id: ID,
    sender: Sender<Option<Arc<Group>>>,
) -> rusqlite::Result<()> {
    let group_opt = conn
        .query_row("SELECT * FROM msg_groups WHERE id = (?);", [id], |row| {
            Ok(Group::from(row).into())
        })
        .optional()?;
    if let Some(group) = &group_opt {
        Caches::insert_group(cache, group);
    }
    sender.send(group_opt).unwrap();
    Ok(())
}
fn is_constraint_violation(error: &rusqlite::Error) -> bool {
    println!("error: {}", error);
    if let rusqlite::Error::SqliteFailure(sqlite_error, _) = error {
        println!("sqlite failure: {}", sqlite_error);
        return matches!(sqlite_error.code, libsqlite::ErrorCode::ConstraintViolation);
    }
    return false;
}
fn insert_message(
    cache: &mut Arc<Mutex<ThreadResources>>,
    mut conn: Conn,
    msg: ClientMessage,
    ids: &Vec<ID>,
    delivered: bool,
    sender: Sender<bramble_common::Result<()>>,
) -> rusqlite::Result<()> {
    let tx = conn.transaction()?;

    let stmt = tx.prepare("INSERT INTO messages VALUES (?, ?, ?, ?, ?, ?);");
    let msg_insert_res = stmt?.insert(params![
        msg.id,
        msg.group,
        msg.body,
        msg.time.as_secs(),
        msg.sharing,
        delivered
    ]);
    Result::from_iter(ids.iter().map(|dependency| -> Result<()> {
        let stmt = tx.prepare("INSERT INTO edges VALUES (?, ?);");
        stmt?.insert(params![msg.id, dependency,])?;
        Ok(())
    }))?;
    tx.commit()?;
    if let Err(e) = msg_insert_res {
        if is_constraint_violation(&e) {
            println!("ConstraintViolation {}", msg.id);
            sender.send(Err(bramble_common::Error::Duplicate)).unwrap();
        } else {
            return Err(e);
        }
    } else {
        sender.send(Ok(())).unwrap();
    }
    Caches::insert_message(cache, &msg);
    Ok(())

    // return Ok(SyncTypes::Message(Arc::clone(msg)));
}
fn new_group(
    cache: &mut Arc<Mutex<ThreadResources>>,
    conn: Conn,
    group: &Arc<Group>,
    sender: Sender<()>,
) -> rusqlite::Result<()> {
    let stmt = conn.prepare("INSERT INTO msg_groups VALUES (?, ?, ?, ?);");
    stmt?.insert(params![
        group.id,
        group.descriptor,
        group.client.identifier,
        group.client.version,
    ])?;
    // TODO: What if its a duplicate? What if this fails?
    sender.send(()).unwrap();
    Caches::insert_group(cache, group);
    Ok(())
}
fn delete_group(
    cache: &mut Arc<Mutex<ThreadResources>>,
    conn: Conn,
    id: &ID,
    sender: Sender<()>,
) -> rusqlite::Result<()> {
    let stmt = conn.prepare("DELETE FROM msg_groups WHERE id = ?;");
    stmt?.execute(params![id])?;
    sender.send(()).unwrap();
    Caches::delete_group(cache, id);

    // TODO: What if its a duplicate? What if this fails?
    Ok(())
}
fn delete_group_sync_state_for_peer(
    conn: Conn,
    id: &ID,
    peer: &PublicKey,
    sender: Sender<()>,
) -> rusqlite::Result<()> {
    let stmt = conn.prepare(
        "
        DELETE FROM sync_states as s
        JOIN messages as m
            ON s.message = m.id
        JOIN msg_groups as g
            ON g.id = m.msg_group
        WHERE g.id = ? and s.peer = ?;",
    );
    stmt?.execute(params![id, peer])?;
    sender.send(()).unwrap();
    Ok(())
}

// fn get_ready_sync_states(conn: Conn, peer: &PublicKey, latency: &u32) -> rusqlite::Result<()> {
//     let stmt = conn.prepare("SELECT * FROM sync_states WHERE peer = ? AND seen = 0 AND (send_time < unixepoch() OR eta > unixepoch() + ?);");
//     let mut res = stmt?;
//     let states = res
//         .query(params![peer.as_ref(), latency])?
//         .map(|row| Ok(SyncState::from(row)))
//         .collect()?;
//     // TODO: What if its a duplicate? What if this fails?
//     return Ok(SyncTypes::SyncStates(states));
// }
fn get_sync_state(
    conn: Conn,
    id: &ID,
    peer: &PublicKey,
    sender: Sender<Option<SyncState>>,
) -> rusqlite::Result<()> {
    let opt = conn
        .query_row(
            "SELECT * FROM sync_states WHERE message = ? AND peer = ?;",
            params![id, peer.as_ref()],
            |row| Ok(SyncState::from(row)),
        )
        .optional()?;
    sender.send(opt).unwrap();
    Ok(())
}
// fn get_sync_states(conn: Conn, ids: &Vec<ID>, peer: &PublicKey) -> rusqlite::Result<()> {
//     rusqlite::vtab::array::load_module(&conn)?;
//     let mut stmt =
//         conn.prepare("SELECT * FROM sync_states WHERE peer = ? AND message IN rarray(?);")?;
//     let values = ids_to_rarray(&ids);
//     //Result::from_iter(ids.into_iter().map(|dependency| -> Result<()> {
//     //let stmt = tx.prepare("INSERT INTO edges VALUES (?, ?);");
//     //let res = stmt?.insert(params![msg.id, dependency,]);
//     //Ok(())
//     //}))?;
//     let states: Vec<SyncState> =
//         Result::from_iter(stmt.query_map(params![peer.as_ref(), values], |row| {
//             Ok(SyncState::from(row))
//         })?)?;
//     //.map(|res| res.unwrap())
//     //.collect();
//     // TODO: Handle miss
//     return Ok(states.into());
// }
fn update_sync_state(
    conn: Conn,
    state: &SyncState,
    sender: Sender<Option<()>>,
) -> rusqlite::Result<()> {
    let res = conn.execute(
        "
        UPDATE sync_states
        SET 
            seen = ?, 
            ack = ?, 
            requested = ?,
            send_count = ?, 
            send_time = ?, 
            eta = ? 
        WHERE message = ? AND peer = ? 
        ;",
        params![
            state.seen,
            state.ack,
            state.requested,
            state.send_count,
            state.send_time,
            state.max_latency,
            state.message,
            state.peer.as_ref(),
        ],
    );
    match res {
        Ok(_) => sender.send(Some(())).unwrap(),
        Err(_) => sender.send(None).unwrap(),
    }
    Ok(())
}
fn create_sync_state(
    conn: Conn,
    state: &SyncState,
    sender: Sender<SyncState>,
) -> rusqlite::Result<()> {
    let stmt = conn.prepare("INSERT INTO sync_states VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
    stmt?.insert(params![
        state.message,
        state.peer.as_ref(),
        state.seen,
        state.ack,
        state.requested,
        state.send_count,
        state.send_time,
        state.max_latency,
    ])?;
    // TODO: What if its a duplicate? What if this fails?
    sender.send(*state).unwrap();
    Ok(())
}
fn get_message(
    cache: &mut Arc<Mutex<ThreadResources>>,
    conn: Conn,
    id: &ID,
    sender: Sender<Option<ClientMessage>>,
) -> rusqlite::Result<()> {
    let mut message = conn
        .query_row(
            &format!(
                "
SELECT {MESSAGE_COLS}
FROM messages as m
JOIN msg_groups as g 
    ON g.id = m.msg_group
WHERE m.id = ?;"
            ),
            [id],
            |row| Ok(ClientMessage::from(row)),
        )
        .optional()?;
    if let Some(message) = &mut message {
        Caches::insert_message_or_replace(cache, message)
    };
    sender.send(message).unwrap();
    Ok(())

    // return match message {
    //     Some(msg) => Ok(SyncTypes::Message(Arc::new(msg))),
    //     None => Ok(SyncTypes::Missing),
    // };
}
fn get_all_group_messages(
    conn: Conn,
    id: &ID,
    sender: Sender<Vec<ClientMessage>>,
) -> rusqlite::Result<()> {
    let mut stmt = conn.prepare(&format!(
        "
        SELECT
            {MESSAGE_COLS}
        FROM 
            messages as m
        JOIN msg_groups as g
            ON g.id = m.msg_group
        WHERE g.id = ?;",
    ))?;
    let messages: Vec<ClientMessage> = stmt
        .query_map([id], |row| Ok(ClientMessage::from(row)))?
        .map(|res| res.unwrap()) // this unwrap should be fine
        .collect();
    sender.send(messages).unwrap();
    Ok(())
}
// Currently unused
//fn get_group_sharing_messages( id: ID) -> rusqlite::Result<()>{
//let (sender, pool, op) = self.get_thread_vars();
//self.workers.execute(move || {
//let conn = pool.get()?;
//let mut stmt = conn
//.prepare(
//"SELECT * FROM messages WHERE msg_group = ? AND sharing = ?;",
//)
//?;
//let messages = stmt
//.query_map(params![id, true], |row| Ok(Message::from(row)))
//?
//.map(|res| res?)
//.collect();
//// TODO: Handle miss
//return SyncTypes::MessageIDs(states);
//});
//}
// fn get_groups_sharing_messages(
//     conn: Conn,
//     ids: &Vec<ID>,
//     sender: Sender<Vec<ID>>,
// ) -> rusqlite::Result<()> {
//     // use more queries
//
//     let stmt = conn.prepare(
//         "SELECT id FROM messages WHERE sharing = ? AND msg_group IN
//             rarray(?);",
//     )?;
//     let values = ids_to_rarray(&ids);
//     let messages = collect_ids(stmt, params![true, values]);
//     Ok(SyncTypes::MessageIDs(messages))
// }
// Unused below
//fn get_all_groups_sharing_messages(&mut self) -> rusqlite::Result<()>{}
// fn set_sync_state_ack(
//     conn: Conn,
//     id: &ID,
//     peer: &PublicKey,
//     acked: &bool,
// ) -> rusqlite::Result<()> {
//     let updated = conn.execute(
//         "
//         UPDATE sync_states
//         SET
//             ack = ?
//         WHERE message = ? AND peer = ?
//         ;",
//         params![acked, id, peer.as_ref(),],
//     )?;
//     return match updated {
//         1 => Ok(SyncTypes::Success),
//         _ => Ok(SyncTypes::Missing),
//     };
// }
//
// fn set_sync_state_seen(
//     conn: Conn,
//     id: &ID,
//     peer: &PublicKey,
//     seen: &bool,
// ) -> rusqlite::Result<()> {
//     let updated = conn.execute(
//         "
//         UPDATE sync_states
//         SET
//             seen = ?
//         WHERE message = ? AND peer = ?
//         ;",
//         params![seen, id, peer.as_ref(),],
//     )?;
//     return match updated {
//         1 => Ok(SyncTypes::Success),
//         _ => Ok(SyncTypes::Missing),
//     };
// }
// fn set_sync_state_requested(
//     conn: Conn,
//     id: &ID,
//     peer: &PublicKey,
//     seen: &bool,
// ) -> rusqlite::Result<()> {
//     let updated = conn.execute(
//         "
//         UPDATE sync_states
//         SET
//             requested = ?
//         WHERE message = ? AND peer = ?
//         ;",
//         params![seen, id, peer.as_ref(),],
//     )?;
//     return match updated {
//         1 => Ok(SyncTypes::Success),
//         _ => Ok(SyncTypes::Missing),
//     };
// }
// fn set_sync_state_send_data(conn: Conn, state: &SyncState) -> rusqlite::Result<()> {
//     let updated = conn.execute(
//         "
//         UPDATE sync_states
//         SET
//             send_count = ?,
//             send_time = ?,
//             eta = ?
//         WHERE message = ? AND peer = ?
//         ;",
//         params![
//             state.send_count,
//             state.send_time,
//             state.max_latency,
//             state.message,
//             state.peer.as_ref(),
//         ],
//     )?;
//     return match updated {
//         1 => Ok(SyncTypes::Success),
//         _ => Ok(SyncTypes::Missing),
//     };
// }
// fn has_messages(conn: Conn, ids: &Vec<ID>) -> rusqlite::Result<()> {
//     let mut stmt = conn.prepare(
//         "SELECT id FROM messages WHERE id IN
//             rarray(?);",
//     )?;
//     let values = Rc::new(ids.iter().map(|id| Value::from(id)).collect::<Vec<Value>>());
//     let has_ids: HashSet<ID> = stmt
//         .query_map([values], |row| row.get(0))?
//         .map(|res| res.unwrap()) // this unwrap should be fine
//         .collect();
//     let bools = ids.iter().map(|id| has_ids.contains(id)).collect();
//     return Ok(SyncTypes::HasMessages(bools));
// }
fn invalidating_delete(
    cache: &mut Arc<Mutex<ThreadResources>>,
    mut conn: Conn,
    id: &ID,
    sender: Sender<Vec<ID>>,
) -> rusqlite::Result<()> {
    let tx = conn.transaction()?;
    let deleting_nodes = tx.prepare(
        "
        WITH RECURSIVE nodes(x) AS (
            SELECT ?
            UNION
            SELECT dependent
                FROM edges
                JOIN nodes ON dependency = x
        )
        SELECT x FROM nodes
        ;",
    )?;
    let ids = collect_ids(deleting_nodes, [id]);
    let deleting_messages = tx.prepare(
        "
        DELETE FROM messages
        WHERE id IN rarray(?)
        ;",
    );
    let deleting_edges = tx.prepare(
        "
        DELETE FROM edges
        WHERE 
            dependency IN rarray(?)
            OR
            dependent IN rarray(?)
        ;",
    );
    let ids_rarray = ids_to_rarray(&ids);
    deleting_messages?.execute([Rc::clone(&ids_rarray)])?;
    deleting_edges?.execute([Rc::clone(&ids_rarray), ids_rarray])?;
    tx.commit()?;
    ids.iter().for_each(|id| Caches::delete_message(cache, id));
    sender.send(ids).unwrap();
    Ok(())
}
fn valid_delete(
    cache: &mut Arc<Mutex<ThreadResources>>,
    mut conn: Conn,
    id: &ID,
    sender: Sender<()>,
) -> rusqlite::Result<()> {
    let tx = conn.transaction()?;
    let transitive_dependents = tx.prepare(
        "
        WITH RECURSIVE nodes(x) AS (
            SELECT ?
            UNION
            SELECT dependent
                FROM edges
                JOIN messages ON id = x
                JOIN nodes ON dependency = x
                WHERE sharing = 1
        )
        SELECT x FROM nodes
        ;",
    );
    let ids = collect_ids(transitive_dependents?, [id]);
    // delete this msg (remove its body)
    // set dependents to not share
    let set_dependents_not_sharing = tx.prepare(
        "
        UPDATE messages
        SET
            sharing = 0
        WHERE id IN rarray(?)
        ;",
    );
    let delete_message = tx.prepare("DELETE FROM messages WHERE id = ?;");
    let ids = ids_to_rarray(&ids);
    set_dependents_not_sharing?.execute([ids])?;
    delete_message?.execute([id])?;
    tx.commit()?;
    sender.send(()).unwrap();
    Caches::delete_message(cache, id);
    Ok(())
}

fn has_all_dependencies_delivered(
    conn: Conn,
    id: &ID,
    sender: Sender<bool>,
) -> rusqlite::Result<()> {
    let undelivered: Option<ID> = conn
        .query_row(
            "
        SELECT id FROM edges
        JOIN messages ON id = dependency
        WHERE dependent = ? AND delivered = 0
        ;",
            [id],
            |row| row.get(0),
        )
        .optional()?;
    let all_deps_delivered = undelivered.is_none();
    sender.send(all_deps_delivered).unwrap();
    Ok(())
}
fn get_dependents(conn: Conn, id: &ID, sender: Sender<Vec<ID>>) -> rusqlite::Result<()> {
    let stmt = conn.prepare("SELECT dependent FROM edges WHERE dependency = ?;")?;
    let ids = collect_ids(stmt, [id]);
    sender.send(ids).unwrap();
    Ok(())
}
fn set_msg_sharing(
    conn: Conn,
    id: &ID,
    sharing: &bool,
    sender: Sender<()>,
) -> rusqlite::Result<()> {
    conn.query_row(
        "
        UPDATE messages
        SET 
            sharing = ?
        WHERE id = ?
        RETURNING *
        ;",
        params![sharing, id],
        |_| Ok(()),
    )?;
    sender.send(()).unwrap();
    Ok(())
}
// fn set_msgs_sharing(conn: Conn, ids: &Vec<ID>) -> rusqlite::Result<()> {
//     rusqlite::vtab::array::load_module(&conn)?;
//     let update = conn.prepare(
//         "
//         UPDATE messages
//         SET
//             sharing = 1
//         WHERE id IN rarray(?)
//         ;",
//     );
//     let values: Rc<Vec<Value>> = ids_to_rarray(&ids);
//     update?.execute([values])?;
//     //let messages = collect_ids(update, params![true, values]);
//     Ok(SyncTypes::Success)
// }
fn set_msg_delivered(conn: Conn, id: &ID, sender: Sender<()>) -> rusqlite::Result<()> {
    conn.query_row(
        "
        UPDATE messages
        SET
            delivered = 1
        WHERE id = ?
        RETURNING *
        ;",
        params![id],
        |_| Ok(()),
    )?;
    sender.send(()).unwrap();
    Ok(())
}

fn all_deps_shared(conn: Conn, id: &ID, sender: Sender<bool>) -> rusqlite::Result<()> {
    let exists_unshared = conn
        .query_row(
            "
        SELECT * FROM messages as m1
        JOIN edges as e ON e.dependent = m1.id
        JOIN messages as m2 ON e.dependency = m2.id
        WHERE m2.sharing = 0 AND m1.id = ?;
    ",
            params![id],
            |_| Ok(()),
        )
        .optional()?;
    sender.send(exists_unshared.is_none()).unwrap();
    Ok(())
}
// fn get_non_sharing_dependencies(conn: Conn, id: &ID) -> rusqlite::Result<()> {
//     let stmt = conn.prepare(
//         // Not sure this would work:
//         "
//         WITH RECURSIVE nodes(x) AS (
//             SELECT ?
//             UNION
//             SELECT dependency
//                 FROM edges
//                 JOIN messages ON dependency = id
//                 JOIN nodes ON dependent = x
//                 WHERE sharing = 0
//         )
//         SELECT x FROM nodes
//         ;",
//     );
//     let ids = collect_ids(stmt?, [id]);
//     Ok(SyncTypes::MessageIDs(ids))
// }
// fn get_is_fully_shared(conn: &Conn, peers: &Vec<PublicKey>) -> rusqlite::Result<()> {
//     let opt = conn
//         .query_row(
//             "
//         SELECT * FROM sync_states
//         WHERE peer IN rarray(?);",
//             [keys_to_rarray(peers)],
//             |row| Ok(SyncState::from(row)),
//         )
//         .optional()?;
//     Ok(SyncTypes::Bool(opt.is_none()))
// }
// fn set_msg_delivered(
// fn get_groups_messages_unseen_by_peer(conn: Conn, id: &ID, sender: Sender<()>) -> rusqlite::Result<()> {
//                     }
//                     GetDeliverableMessages(send) => get_deliverable_messages(send),

struct DBInput {
    id: u32,
    query: String,
}
#[derive(Debug)]
pub enum StorageOperation {
    // GetMessageGroup(ID), // only gets the group
    GetGroup(ID, Sender<Option<Arc<Group>>>),
    InsertMessage(
        ClientMessage,
        Vec<ID>,
        bool,
        Sender<bramble_common::Result<()>>,
    ), // must have sharing field
    NewGroup(Arc<Group>, Sender<()>),
    DeleteGroup(ID, Sender<()>),
    DeleteGroupSyncStatesForPeer(ID, PublicKey, Sender<()>),
    // The following should get ids of messages
    // 1) Request is true and send_count is 0, or
    // 2) seen is false, and either
    // 3) send_time has passed, or
    // 4) max_latency has passed or is 0
    // ex. java impl:
    // https://code.briarproject.org/briar/briar/-/blob/master/bramble-core/src/main/java/org/briarproject/bramble/db/JdbcDatabase.java#L2327
    // GetReadySyncStates(PublicKey, u32), // get the message with those that are requested
    GetGroupsSharingMessagesUnseenByPeer(Vec<ID>, PublicKey, Sender<Vec<ClientMessage>>),
    GetDeliverableMessages(Sender<Vec<ClientMessage>>),
    GetSyncState(ID, PublicKey, Sender<Option<SyncState>>),
    // GetSyncStates(Vec<ID>, PublicKey),
    UpdateSyncState(SyncState, Sender<Option<()>>), // Success
    CreateSyncState(SyncState, Sender<SyncState>),  // Err or the new state
    GetMessage(ID, Sender<Option<ClientMessage>>),  // Message - With Group
    GetAllGroupMessages(ID, Sender<Vec<ClientMessage>>),
    //GetGroupSharingMessages(ID),       // MessageIDs
    // GetGroupsSharingMessages(Vec<ID>, Sender<Option<GroupMessages>>), // GroupsSharingMessages
    //GetAllGroupsSharingMessages,
    // SetSyncStateAck(ID, PublicKey, bool),
    // SetSyncStateSeen(ID, PublicKey, bool),
    // SetSyncStateRequested(ID, PublicKey, bool),
    // SetSyncStateSendData(SyncState),
    // HasMessages(Vec<ID>), // Vec<bool>
    InvalidatingDelete(ID, Sender<Vec<ID>>),
    ValidDelete(ID, Sender<()>),
    HasAllDependenciesDelivered(ID, Sender<bool>),
    GetDependents(ID, Sender<Vec<ID>>),
    SetMsgSharing(ID, bool, Sender<()>), // must set all transitive dependencies sharing as well. Should return
    // SetMsgsSharing(Vec<ID>),
    SetMessageDelivered(ID, Sender<()>),
    AllDependenciesShared(ID, Sender<bool>),
    // GetNonSharingDependencies(ID),
    // FullyShared(Vec<PublicKey>),

    // those MessageIDs only of those that were not already being shared
    Terminate,
}

impl StorageOperation {
    pub fn desc(&self) -> String {
        match self {
            // GetMessageGroup(id) => format!("GetMessageGroup {}", &id),
            GetGroup(id, _) => format!("GetGroup {}", &id),
            InsertMessage(msg, _, _, _) => format!("InsertMessage {}", &msg.id),
            NewGroup(group, _) => format!("NewGroup {}", &group.id),
            DeleteGroup(id, _) => format!("DeleteGroup {}", &id),
            DeleteGroupSyncStatesForPeer(id, peer, _) => {
                format!("DeleteGroupSyncStatesForPeer {} {}", &id, peer)
            }
            // GetReadySyncStates(_, _) => format!("GetReadySyncStates"),
            GetSyncState(_, _, _) => "GetSyncState".to_string(),
            // GetSyncStates(_, _) => format!("GetSyncStates"),
            UpdateSyncState(state, _) => {
                let mut update = format!("UpdateSyncState {} {} ", state.peer, state.message);
                if state.seen {
                    update.push_str("SEEN ");
                }
                if state.requested {
                    update.push_str("REQ ");
                }
                if state.send_count > 0 {
                    update.push_str(&format!("SEND_NUM {}", state.send_count));
                }
                update
            }
            CreateSyncState(state, _) => {
                format!("CreateSyncState {} {}", state.peer, state.message)
            }
            GetMessage(id, _) => format!("GetMessage {}", &id),
            GetAllGroupMessages(id, _) => format!("GetAllGroupMessages {}", &id),
            // GetGroupsSharingMessages(_ids, _) => format!("GetGroupsSharingMessages"),
            // SetSyncStateAck(_, _, _) => format!("SetSyncStateAck"),
            // SetSyncStateRequested(_, _, _) => format!("SetSyncStateRequested"),
            // SetSyncStateSendData(_) => format!("SetSyncStateSendData"),
            // SetSyncStateSeen(_, _, _) => format!("SetSyncStateSeen"),
            // HasMessages(_) => format!("HasMessages"),
            InvalidatingDelete(id, _) => format!("InvalidatingDelete {}", &id),
            ValidDelete(id, _) => format!("ValidDelete {}", &id),
            HasAllDependenciesDelivered(id, _) => {
                format!("HasAllDependenciesDelivered {}", &id)
            }
            GetDependents(id, _) => format!("GetDependents {}", &id),
            SetMsgSharing(_, _, _) => "SetMsgSharing".to_string(),
            // SetMsgsSharing(_) => format!("SetMsgsSharing"),
            SetMessageDelivered(id, _) => format!("SetMessageDelivered {}", &id),
            AllDependenciesShared(id, _) => format!("AllDependenciesShared {}", &id),
            // GetNonSharingDependencies(id) => format!("GetNonSharingDependencies {}", &id),
            Terminate => "Terminate".to_string(),
            // FullyShared(_) => String::new(),
            GetGroupsSharingMessagesUnseenByPeer(_groups, peer, _) => {
                format!("GetGroupsSharingMessagesUnseenByPeer {}", &peer)
            }
            GetDeliverableMessages(_) => "GetDeliverableMessages".to_string(),
        }
    }
    pub fn is_terminate(&self) -> bool {
        use StorageOperation::*;
        matches!(self, Terminate)
    }
}

pub enum ClientStorageOperation<C: Client> {
    GetGroups(Sender<Vec<C::Group>>),
    GetGroup(ID, Sender<Option<C::Group>>),
    GetGroupTopMessages(ID, Sender<Vec<C::Message>>),
    GetMessageDescendents(ID, u32, Sender<Vec<C::Message>>),
}

enum StorageError {
    Missing,
    Duplicate,
}

enum InputMode {
    CacheOnly,
    DBOnly,
    All,
}

// fn send_ok_output(sender: UnboundedSender<AsyncData>, op: u32, output: SyncTypes) {
//     sender
//         .unbounded_send(AsyncData {
//             id: op,
//             data: Ok(output),
//         })
//         .unwrap();
// }
//
// fn send_err_output(
//     sender: UnboundedSender<AsyncData>,
//     op_id: u32,
//     err: rusqlite::Error,
//     op: StorageOperation,
// ) {
//     error!(target: "storage", "Sending storage error: {}", err);
//     error!(target: "storage","Operation: {:?}", op);
//     sender
//         .unbounded_send(AsyncData {
//             id: op_id,
//             data: Err(bramble_common::Error::Other(err.to_string())),
//         })
//         .unwrap();
// }

#[cfg(test)]
pub mod tests {
    use super::SqliteSyncStorage;
    use super::StorageOperation;
    use crate::storage::*;
    use crate::test::*;
    use bramble_crypto::Hash as ID;
    use bramble_crypto::HASH_LEN;
    use futures::channel::mpsc::UnboundedSender;
    use futures::channel::oneshot::channel;
    use futures::executor;
    use std::path::Path;

    fn id(num: u8) -> ID {
        ID::from([num; HASH_LEN])
    }

    fn send_terminate(sender: UnboundedSender<StorageOperation>) {
        sender.unbounded_send(StorageOperation::Terminate).unwrap();
    }

    fn create_group(params: &mut TestParams, group: Arc<Group>) {
        test_storage_op!(params, NewGroup, group);
        // sender
        //     .unbounded_send(StorageOperation::NewGroup(Arc::clone(&group)))
        //     .unwrap();
        // let res = executor::block_on(receiver.next());
        // let binding = res.unwrap();
        // let res2 = binding.data.as_ref().unwrap();
        // if let SyncTypes::Group(grp) = res2 {
        //     assert_eq!(group.id, grp.id);
        // } else {
        //     panic!();
        // }
    }

    //fn get_group_from_cache(
    //TestParams {
    //sender,
    //receiver,
    //handle,
    //}: &mut TestParams,
    //group: Arc<Group>,
    //) {
    //executor::block_on(sender.send(StorageInput {
    //id: 0,
    //request: StorageOperation::GetGroup(id(0)),
    //}));
    //let res = executor::block_on(receiver.next());
    //let binding = res.unwrap();
    //let res2 = binding.result.as_ref().unwrap();
    //if let SyncTypes::Group(group) = res2 {
    //assert_eq!(group.id, id(0));
    //} else {
    //panic!();
    //}
    //}
    fn create_message(p: &mut TestParams, msg: ClientMessage) {
        insert_message(p, msg, vec![]);
        //let res = exec_operation(p, StorageOperation::InsertMessage(Arc::clone(&msg), vec![]));
    }
    // fn fetch_message_group(p: &mut TestParams, msg: &Arc<Message>, group: Arc<Group>) {
    //     let res = exec_operation(p, StorageOperation::GetMessageGroup(msg.id));
    //     if let SyncTypes::Group(grp) = res {
    //         assert_eq!(group.id, grp.id);
    //     } else {
    //         panic!();
    //     }
    // }
    pub fn fetch_message(p: &mut TestParams, msg: &ID) -> ClientMessage {
        test_storage_op!(p, GetMessage, *msg).unwrap()
        // let msg_type = exec_operation(p, StorageOperation::GetMessage(*msg));
        // if let SyncTypes::Message(message) = msg_type {
        //     assert_eq!(*msg, message.id);
        //     return message;
        // } else {
        //     panic!();
        // }
    }
    #[test]
    //#[ignore]
    fn test_creating_database() {
        let file = wipe("./tests/test.db");
        let (sender, handle) =
            SqliteSyncStorage::init(SqliteSyncStorage::new(&file, false).unwrap());
        send_terminate(sender);
        handle.join().unwrap();
        assert_eq!(Path::new(&file).exists(), true);
    }

    fn create_sync_state(p: &mut TestParams, state: SyncState) {
        let res = test_storage_op!(p, CreateSyncState, state);
        assert_eq!(state.message, res.message);
        assert_eq!(state.peer, res.peer);
        // p.sender
        //     .unbounded_send(StorageOperation::CreateSyncState(state))
        //     .unwrap();
        // let res = executor::block_on(p.receiver.next());
        // let binding = res.unwrap();
        // let res2 = binding.data.as_ref().unwrap();
        // if let SyncTypes::SyncState(opt) = res2 {
        //     let res = opt;
        // } else {
        //     panic!();
        // }
    }

    fn get_sync_states(p: &mut TestParams, ids: Vec<ID>, peer: PublicKey) {
        ids.into_iter().for_each(|id| {
            test_storage_op!(p, GetSyncState, id, peer).unwrap();
        });
        //
        // let mut copy = vec![];
        // ids.iter().for_each(|id| copy.push(*id));
        // p.sender
        //     .unbounded_send(StorageOperation::GetSyncStates(ids, peer))
        //     .unwrap();
        // let res = executor::block_on(p.receiver.next());
        // let binding = res.unwrap();
        // let res2 = binding.data.as_ref().unwrap();
        // if let SyncTypes::SyncStates(states) = res2 {
        //     assert!(copy
        //         .iter()
        //         .all(|id| states.iter().any(|state| state.message == *id)));
        //     assert!(states.iter().all(|state| state.peer == peer));
        // } else {
        //     panic!();
        // }
    }
    fn update_sync_states(p: &mut TestParams) {
        let mut state = SyncState {
            message: id(4),
            peer: dummy_peer(5),
            seen: false,
            ack: false,
            requested: false,
            send_count: 0,
            send_time: 0,
            max_latency: 0,
        };
        create_message(p, dummy_msg(4, 2));
        test_storage_op!(p, CreateSyncState, state);

        state.seen = false;
        state.send_count = 1917;
        test_storage_op!(p, UpdateSyncState, state);
        let res = test_storage_op!(p, GetSyncState, state.message, state.peer).unwrap();
        assert_eq!(res, state);
        // } else {
        //     panic!();
        // }
        // let res = exec_operation(p, StorageOperation::GetSyncState(state.message, state.peer));
        // if let SyncTypes::SyncState(res) = res {
        //     assert_eq!(res.seen, state.seen);
        //     assert_eq!(res.ack, state.ack);
        // } else {
        //     panic!();
        // }
    }
    // fn get_ready(p: &mut TestParams, state1: SyncState, state2: SyncState) {
    //     let res = exec_operation(p, StorageOperation::GetReadySyncStates(state1.peer, 100));
    //     if let SyncTypes::SyncStates(states) = res {
    //         assert_eq!(states.len(), 1);
    //         assert_eq!(states[0].message, state1.message);
    //     } else {
    //         panic!();
    //     }
    //
    //     let res = exec_operation(p, StorageOperation::GetReadySyncStates(state2.peer, 100));
    //     if let SyncTypes::SyncStates(states) = res {
    //         assert_eq!(states.len(), 1);
    //         assert_eq!(states[0].message, state2.message);
    //     } else {
    //         panic!();
    //     }
    // }

    #[test]
    fn test_creating_and_fetching_group() {
        let file = wipe("./tests/testGroup.db");
        let mut params = storage(&file);
        let group = Arc::new(Group {
            id: id(0),
            descriptor: vec![0],
            client: ClientId {
                identifier: String::from("Identifier"),
                version: 0,
            },
        });

        create_group(&mut params, Arc::clone(&group));
        //get_group_from_cache(&mut params, Arc::clone(&group));
        crate::test::get_group(&mut params, group.id);
        send_terminate(params.sender);
        params.handle.join().unwrap();
    }

    fn dummy_group(num: u8, client: &str) -> Arc<Group> {
        Arc::new(Group {
            id: id(num),
            descriptor: vec![num],
            client: ClientId {
                identifier: String::from(client),
                version: 0,
            },
        })
    }

    fn dummy_msg(num: u8, num2: u8) -> ClientMessage {
        ClientMessage::new(
            Arc::new(Message {
                id: id(num),
                group: id(num2),
                body: vec![],
                time: Duration::new(0, 0),
                sharing: Some(true),
                delivered: false,
            }),
            ClientId {
                identifier: "TestClient".to_string(),
                version: 0,
            },
        )
    }
    #[test]
    fn test_create_message() {
        let file = wipe("./tests/testMsg.db");
        let mut params = storage(&file);
        let group = dummy_group(2, "client");
        let msg = dummy_msg(2, 2);
        create_group(&mut params, Arc::clone(&group));
        create_message(&mut params, msg.clone());
        // fetch_message_group(&mut params, &msg, Arc::clone(&group));
        fetch_message(&mut params, &msg.id);

        send_terminate(params.sender);
        params.handle.join().unwrap();
    }

    fn dummy_peer(num: u8) -> PublicKey {
        [num; KEY_LEN].into()
    }

    #[test]
    fn test_sync_states() {
        // Test creating
        // Test getting one and multiple
        // Test getting ready states
        // Test updating
        let file = wipe("./tests/testSyncStates.db");
        let mut params = storage(&file);
        let group = dummy_group(2, "client");
        let msg = dummy_msg(2, 2);
        let msg2 = dummy_msg(3, 2);
        let peer = dummy_peer(0);
        let peer2 = dummy_peer(1);
        let state1 = SyncState {
            // should send
            message: msg.id,
            peer,
            seen: false,
            ack: false,
            requested: false,
            send_count: 0,
            send_time: 0,
            max_latency: 0,
        };
        let state2 = SyncState {
            // should not send
            message: msg.id,
            peer: peer2,
            seen: true,
            ack: false,
            requested: false,
            send_count: 0,
            send_time: 0,
            max_latency: 0,
        };
        let state3 = SyncState {
            // should not send
            message: msg2.id,
            peer,
            seen: false,
            ack: false,
            requested: false,
            send_count: 0,
            send_time: 2222222222, // This will fail in the year 2040
            max_latency: 0,
        };
        let state4 = SyncState {
            // should send, high latency
            message: msg2.id,
            peer: peer2,
            seen: false,
            ack: false,
            requested: false,
            send_count: 0,
            send_time: 2222222222,
            max_latency: u32::MAX,
        };
        create_group(&mut params, Arc::clone(&group));
        create_message(&mut params, msg.clone());
        create_message(&mut params, msg2.clone());
        create_sync_state(&mut params, state1);
        create_sync_state(&mut params, state2);
        create_sync_state(&mut params, state3);
        create_sync_state(&mut params, state4);
        fetch_sync_state(&mut params, &state1.get_key());
        get_sync_states(&mut params, vec![msg.id, msg2.id], peer);
        // get_ready(&mut params, state1, state4);
        update_sync_states(&mut params);

        send_terminate(params.sender);
        params.handle.join().unwrap();
    }

    fn get_groups_sharing_messages(p: &mut TestParams, ids: Vec<ID>) -> Vec<ClientMessage> {
        let peer = peer(9);
        test_storage_op!(p, GetGroupsSharingMessagesUnseenByPeer, ids, peer)
    }
    fn set_msg_sharing(p: &mut TestParams, id: ID, sharing: bool) {
        test_storage_op!(p, SetMsgSharing, id, sharing);
    }
    fn assert_has_messages(p: &mut TestParams, ids: Vec<ID>) {
        ids.into_iter().for_each(|id| {
            test_storage_op!(p, GetMessage, id).unwrap();
        });
        // let res = test_storage_op!(p, HasMessages,ids);
        // extract_variant!(res, SyncTypes::HasMessages)
    }

    #[test]
    fn test_sharing() {
        let file = wipe("./tests/testSharing.db");
        let mut params = storage(&file);
        let groups = vec![
            dummy_group(0, "client"),
            dummy_group(1, "client"),
            dummy_group(2, "client"),
            dummy_group(3, "client"),
        ];
        groups
            .iter()
            .for_each(|group| create_group(&mut params, Arc::clone(&group)));
        let msg = dummy_msg(0, 0);
        let msg2 = dummy_msg(1, 0);
        let msg3 = dummy_msg(2, 1);
        let msg4 = dummy_msg(3, 1);
        let msg5 = dummy_msg(4, 2);
        let _peer = dummy_peer(0);
        let _peer2 = dummy_peer(1);
        create_message(&mut params, msg.clone());
        create_message(&mut params, msg2);
        create_message(&mut params, msg3);
        create_message(&mut params, msg4);
        create_message(&mut params, msg5);
        println!(
            "{}",
            get_groups_sharing_messages(&mut params, vec![groups[0].id]).len(),
        );
        assert_eq!(
            get_groups_sharing_messages(&mut params, vec![groups[0].id]).len(),
            2
        );
        assert_eq!(
            get_groups_sharing_messages(&mut params, vec![groups[0].id, groups[2].id]).len(),
            3
        );
        set_msg_sharing(&mut params, msg.id, false);
        assert_eq!(
            get_groups_sharing_messages(&mut params, vec![groups[0].id]).len(),
            1
        );
        assert_has_messages(&mut params, vec![id(0), id(4), id(2)]);
        send_terminate(params.sender);
    }

    pub fn assert_message_not_exists(p: &mut TestParams, msg: ID) {
        assert_eq!(true, test_storage_op!(p, GetMessage, msg).is_none())
    }
    fn assert_sync_state_exists(p: &mut TestParams, msg: &ID, peer: &PublicKey) {
        test_storage_op!(p, GetSyncState, *msg, *peer).unwrap();
    }
    fn assert_sync_state_not_exists(p: &mut TestParams, msg: &ID, peer: &PublicKey) {
        assert_eq!(
            false,
            test_storage_op!(p, GetSyncState, *msg, *peer).is_some(),
        );
    }
    fn insert_message(p: &mut TestParams, msg: ClientMessage, deps: Vec<ID>) {
        test_storage_op!(p, InsertMessage, msg, deps, false).unwrap();
    }
    fn create_msg_tree(
        p: &mut TestParams,
    ) -> (
        Vec<ClientMessage>,
        Vec<Arc<Group>>,
        Vec<SyncState>,
        Vec<PublicKey>,
    ) {
        let groups = vec![
            dummy_group(0, "client"),
            dummy_group(1, "client"),
            dummy_group(2, "client"),
            dummy_group(3, "client"),
        ];
        groups
            .iter()
            .for_each(|group| create_group(p, Arc::clone(&group)));
        let msg0 = dummy_msg(0, 0);
        let msg1 = dummy_msg(1, 0);
        let msg2 = dummy_msg(2, 1);
        let msg3 = dummy_msg(3, 1);
        let msg4 = dummy_msg(4, 2);
        let peer = dummy_peer(0);
        let peer2 = dummy_peer(1);
        insert_message(p, msg0.clone(), vec![]);
        insert_message(p, msg1.clone(), vec![]);
        insert_message(p, msg2.clone(), vec![msg1.id]);
        insert_message(p, msg3.clone(), vec![msg2.id]);
        insert_message(p, msg4.clone(), vec![msg2.id]);
        let states = vec![
            SyncState::new(&msg0.id, &peer),
            SyncState::new(&msg1.id, &peer),
            SyncState::new(&msg2.id, &peer),
            SyncState::new(&msg0.id, &peer2),
            SyncState::new(&msg2.id, &peer2),
            SyncState::new(&msg3.id, &peer2),
        ];
        create_sync_state(p, states[0]);
        create_sync_state(p, states[1]);
        create_sync_state(p, states[2]);
        create_sync_state(p, states[3]);
        create_sync_state(p, states[4]);
        create_sync_state(p, states[5]);

        (
            vec![msg0, msg1, msg2, msg3, msg4],
            groups,
            states,
            vec![peer, peer2],
        )
    }
    #[test]
    //#[ignore]
    fn test_invalidating_delete() {
        let file = wipe("./tests/testInvalidDelete.db");
        let mut p = storage(&file);
        let (msgs, _, _, peers) = create_msg_tree(&mut p);

        // InvalidDeleting message 2 should delete msg 3 and 4, and their sync states
        test_storage_op!(&mut p, InvalidatingDelete, msgs[1].id);
        assert_message_not_exists(&mut p, msgs[1].id);
        assert_message_not_exists(&mut p, msgs[2].id);
        assert_message_not_exists(&mut p, msgs[3].id);
        assert_message_not_exists(&mut p, msgs[4].id);
        assert_sync_state_exists(&mut p, &msgs[0].id, &peers[1]);
        assert_sync_state_not_exists(&mut p, &msgs[3].id, &peers[1]);
        fetch_message(&mut p, &msgs[0].id);
        send_terminate(p.sender);
    }
    #[test]
    fn test_delete_message() {
        let file = wipe("./tests/testValidDelete.db");
        let mut p = storage(&file);
        let (msgs, _, _, peers) = create_msg_tree(&mut p);

        // InvalidDeleting message 2 should delete msg 3 and 4, and their sync states
        test_storage_op!(&mut p, ValidDelete, msgs[1].id);
        assert_message_not_exists(&mut p, msgs[1].id);
        fetch_message(&mut p, &msgs[2].id);
        fetch_message(&mut p, &msgs[3].id);
        fetch_message(&mut p, &msgs[4].id);
        fetch_message(&mut p, &msgs[0].id);
        assert_sync_state_exists(&mut p, &msgs[0].id, &peers[1]);
        assert_sync_state_exists(&mut p, &msgs[2].id, &peers[1]);
        assert_sync_state_not_exists(&mut p, &msgs[1].id, &peers[1]);
        send_terminate(p.sender);
    }
    fn get_dependents(p: &mut TestParams, id: ID) -> Vec<ID> {
        test_storage_op!(p, GetDependents, id)
    }
    #[test]
    fn test_get_dependents() {
        let file = wipe("./tests/testDependents.db");
        let mut p = storage(&file);
        let (msgs, _groups, _states, _peers) = create_msg_tree(&mut p);
        let ids = get_dependents(&mut p, msgs[1].id);
        assert_eq!(ids.len(), 1);
        assert_eq!(true, ids.contains(&msgs[2].id));

        let ids = get_dependents(&mut p, msgs[0].id);
        assert_eq!(ids.len(), 0);

        let ids = get_dependents(&mut p, msgs[2].id);
        assert_eq!(ids.len(), 2);
        assert_eq!(true, ids.contains(&msgs[3].id));
        assert_eq!(true, ids.contains(&msgs[4].id));
        send_terminate(p.sender);
    }

    #[test]
    fn test_deliver() {
        let file = wipe("./tests/testDeliver.db");
        let mut p = storage(&file);
        let (msgs, _, _, _) = create_msg_tree(&mut p);
        test_storage_op!(&mut p, SetMessageDelivered, msgs[0].id);
        let msg = fetch_message(&mut p, &msgs[0].id);
        assert_eq!(true, msg.delivered);
        send_terminate(p.sender);
    }
    fn has_all_dependencies_delivered(p: &mut TestParams, id: ID) -> bool {
        test_storage_op!(p, HasAllDependenciesDelivered, id)
    }
    #[test]
    fn test_has_all_deps_delivered() {
        let file = wipe("./tests/testHasAllDepsDelivered.db");
        let mut p = storage(&file);
        let (msgs, _, _, _) = create_msg_tree(&mut p);
        test_storage_op!(&mut p, SetMessageDelivered, msgs[1].id);
        // test has no dependencies
        assert_eq!(true, has_all_dependencies_delivered(&mut p, msgs[1].id));
        // test has one delivered dependency
        assert_eq!(true, has_all_dependencies_delivered(&mut p, msgs[2].id));
        // test has one undelivered dependency
        assert_eq!(false, has_all_dependencies_delivered(&mut p, msgs[3].id));
        send_terminate(p.sender);
    }

    #[test]
    fn test_delete_group() {
        let file = wipe("./tests/testDeleteGroup.db");
        // Need the db to have no cache, because deleting a group will delete that group from the
        // cache, but not its messages
        let mut p = storage(&file);
        let (msgs, groups, _states, _peers) = create_msg_tree(&mut p);
        // test has no dependencies
        test_storage_op!(&mut p, DeleteGroup, groups[1].id);
        // If the messages don't exist, then they were deleted on cascade
        assert_message_not_exists(&mut p, id(2));
        assert_message_not_exists(&mut p, id(3));
        fetch_message(&mut p, &msgs[1].id);
        send_terminate(p.sender);
    }

    // #[test]
    // fn test_get_non_sharing_dependencies() {
    //     let file = "./tests/testGetNonSharingDependencies.db";
    //     if Path::new(file).exists() {
    //         std::fs::remove_file(file).unwrap();
    //     }
    //     let mut p = TestParams::new(SqliteSyncStorage::init(String::from(file), false));
    //     let (msgs, _groups, _states, _peers) = create_msg_tree(&mut p);
    //     // test has no dependencies
    //     set_msg_sharing(&mut p, msgs[1].id, false);
    //     set_msg_sharing(&mut p, msgs[2].id, false);
    //     set_msg_sharing(&mut p, msgs[3].id, false);
    //     let res = exec_operation(
    //         &mut p,
    //         StorageOperation::GetNonSharingDependencies(msgs[3].id),
    //     );
    //     let ids = extract_variant!(res, SyncTypes::MessageIDs);
    //     // If the messages don't exist, then they were deleted on cascade
    //     assert!(ids.contains(&msgs[1].id));
    //     assert!(ids.contains(&msgs[2].id));
    //     assert!(ids.contains(&msgs[3].id));
    //     assert!(ids.len() == 3);
    // }
    pub fn assert_msgs_sharing(p: &mut TestParams, ids: Vec<ID>, sharing: bool) {
        for id in ids {
            let msg = fetch_message(p, &id);
            assert_eq!(msg.sharing.unwrap(), sharing);
        }
    }
    #[test]
    fn test_set_msgs_sharing() {
        let file = wipe("./tests/testSetMsgsSharing.db");
        let mut p = storage(&file);
        let (msgs, _, _, _) = create_msg_tree(&mut p);
        let ids = vec![msgs[1].id, msgs[3].id, msgs[2].id];
        set_msg_sharing(&mut p, msgs[1].id, false);
        set_msg_sharing(&mut p, msgs[2].id, false);
        set_msg_sharing(&mut p, msgs[3].id, false);
        assert_msgs_sharing(&mut p, ids.clone(), false);
        ids.iter()
            .for_each(|id| test_storage_op!(&mut p, SetMsgSharing, *id, true));
        assert_msgs_sharing(&mut p, ids.clone(), true);
    }

    pub fn assert_group_has_num_messages(p: &mut TestParams, id: ID, num: usize) {
        let res = test_storage_op!(p, GetAllGroupMessages, id);
        assert_eq!(num, res.len());
    }
    pub fn assert_msgs_delivered(p: &mut TestParams, ids: Vec<ID>, delivered: bool) {
        for id in ids {
            let msg = fetch_message(p, &id);
            assert_eq!(msg.delivered, delivered);
        }
    }

    #[ignore]
    #[test]
    fn test_get_deliverable() {
        todo!()
    }
}
