//! BTP time management

/// A clock used for track the periods in BTP
pub trait Clock {
    /// Creates a new clock with the given period length.
    fn new(period: u32) -> Self;

    /// Gets the current period number.
    fn now(&self) -> u64;
}

/// A UTC-based clock
pub struct Utc {
    period_len: u32,
}

impl Clock for Utc {
    fn new(period_len: u32) -> Self {
        Self { period_len }
    }

    fn now(&self) -> u64 {
        let secs = chrono::Utc::now().timestamp() as u64;
        secs / self.period_len as u64
    }
}
