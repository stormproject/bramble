//! BQP key agreement protocol

use crate::{Error, Payload, Result, BETA_VERSION, CURRENT_VERSION};
use bramble_crypto::{
    hash, kdf, kex, mac, KeyPair, Mac, PublicKey, Role, SymmetricKey, KEY_LEN, MAC_LEN,
};
use bramble_data::custom::array_as_bytes;
use futures::{AsyncRead, AsyncWrite};
use serde::{Deserialize, Serialize};
use std::{
    fmt::{self, Debug},
    pin::Pin,
    slice,
};

/// Perform the QR code key agreement protocol over the given transport
pub async fn perform_key_agreement<T>(
    transport: T,
    our_payload: Payload,
    our_kp: KeyPair,
    their_payload: Payload,
) -> Result<(SymmetricKey, Role)>
where
    T: AsyncRead + AsyncWrite,
{
    let our_bytes = our_payload.to_bytes();
    let their_bytes = their_payload.to_bytes();
    let role = if our_bytes < their_bytes {
        Role::Alice
    } else {
        Role::Bob
    };
    let protocol = Protocol {
        transport: Box::pin(transport),
        role,
        our_payload,
        our_kp,
        their_payload,
    };
    Ok((protocol.perform().await?, role))
}

struct Protocol<T>
where
    T: AsyncRead + AsyncWrite,
{
    transport: Pin<Box<T>>,
    role: Role,
    our_payload: Payload,
    our_kp: KeyPair,
    their_payload: Payload,
}

impl<T> Protocol<T>
where
    T: AsyncRead + AsyncWrite,
{
    pub async fn perform(mut self) -> Result<SymmetricKey> {
        match self.perform_inner().await {
            Err(e) => {
                self.send_abort(&e).await?;
                Err(e)
            }
            ok => ok,
        }
    }

    async fn perform_inner(&mut self) -> Result<SymmetricKey> {
        let their_pk = if self.role == Role::Alice {
            self.send_key().await?;
            self.receive_key().await?
        } else {
            let their_pk = self.receive_key().await?;
            self.send_key().await?;
            their_pk
        };

        let ss = self.derive_shared_secret(&their_pk)?;
        if self.role == Role::Alice {
            self.send_confirm(&ss, &their_pk).await?;
            self.receive_confirm(&ss, &their_pk).await?;
        } else {
            self.receive_confirm(&ss, &their_pk).await?;
            self.send_confirm(&ss, &their_pk).await?;
        }
        Ok(self.derive_master_secret(&ss))
    }

    async fn send_key(&mut self) -> Result<()> {
        use futures::io::AsyncWriteExt;

        let mut buf = [0u8; KEY_RECORD_LEN];
        let (version, slice) = buf.split_at_mut(1);
        let (typ, slice) = slice.split_at_mut(1);
        let (len, key) = slice.split_at_mut(2);
        version.copy_from_slice(&CURRENT_VERSION.to_be_bytes());
        typ.copy_from_slice(&KEY_RECORD_TYPE.to_be_bytes());
        len.copy_from_slice(&(KEY_LEN as u16).to_be_bytes());
        key.copy_from_slice(self.our_kp.public().as_ref());
        self.transport.write_all(&buf).await?;
        Ok(())
    }

    async fn receive_key(&mut self) -> Result<PublicKey> {
        use futures::io::AsyncReadExt;

        let _version = self.read_version().await?;

        let mut record_type = 0u8;
        self.transport
            .read_exact(slice::from_mut(&mut record_type))
            .await?;
        if record_type != KEY_RECORD_TYPE {
            return Err(Error::InvalidRecord);
        }

        let mut len_bytes = [0u8; 2];
        self.transport.read_exact(&mut len_bytes).await?;
        let len = u16::from_be_bytes(len_bytes);
        if len as usize != KEY_LEN {
            return Err(Error::InvalidKey);
        }

        let mut pk_bytes = [0u8; KEY_LEN];
        self.transport.read_exact(&mut pk_bytes).await?;
        let pk = PublicKey::from(pk_bytes);
        let expected = Commit::derive(&pk);
        if self.their_payload.commit() == expected {
            Ok(pk)
        } else {
            Err(Error::InvalidKey)
        }
    }

    async fn send_confirm(&mut self, ss: &SymmetricKey, their_pk: &PublicKey) -> Result<()> {
        use futures::io::AsyncWriteExt;

        let confirm = self.derive_confirmation_code(ss, their_pk, self.role);
        let mut buf = [0u8; CONFIRM_RECORD_LEN];
        let (version, slice) = buf.split_at_mut(1);
        let (typ, slice) = slice.split_at_mut(1);
        let (len, key) = slice.split_at_mut(2);
        version.copy_from_slice(&CURRENT_VERSION.to_be_bytes());
        typ.copy_from_slice(&CONFIRM_RECORD_TYPE.to_be_bytes());
        len.copy_from_slice(&(MAC_LEN as u16).to_be_bytes());
        key.copy_from_slice(confirm.as_ref());
        self.transport.write_all(&buf).await?;
        Ok(())
    }

    async fn receive_confirm(&mut self, ss: &SymmetricKey, their_pk: &PublicKey) -> Result<()> {
        use futures::io::AsyncReadExt;

        let _version = self.read_version().await?;

        let mut record_type = 0u8;
        self.transport
            .read_exact(slice::from_mut(&mut record_type))
            .await?;
        if record_type != CONFIRM_RECORD_TYPE {
            return Err(Error::InvalidRecord);
        }

        let mut len_bytes = [0u8; 2];
        self.transport.read_exact(&mut len_bytes).await?;
        let len = u16::from_be_bytes(len_bytes);
        if len as usize != MAC_LEN {
            return Err(Error::InvalidConfirmation);
        }

        let mut confirm_bytes = [0u8; MAC_LEN];
        self.transport.read_exact(&mut confirm_bytes).await?;

        let confirm = Mac::from(confirm_bytes);
        let expected = self.derive_confirmation_code(ss, their_pk, self.role.opposite());
        confirm
            .verify(&expected)
            .map_err(|_| Error::InvalidConfirmation)
    }

    async fn send_abort(&mut self, _e: &Error) -> Result<()> {
        use futures::io::AsyncWriteExt;

        let mut buf = [0u8; ABORT_RECORD_LEN];
        let (version, slice) = buf.split_at_mut(1);
        let (typ, len) = slice.split_at_mut(1);
        version.copy_from_slice(&CURRENT_VERSION.to_be_bytes());
        typ.copy_from_slice(&ABORT_RECORD_TYPE.to_be_bytes());
        len.copy_from_slice(&0u16.to_be_bytes());
        self.transport.write_all(&buf).await?;
        Ok(())
    }

    async fn read_version(&mut self) -> Result<u8> {
        use futures::io::AsyncReadExt;

        let mut version = 0u8;
        self.transport
            .read_exact(slice::from_mut(&mut version))
            .await?;
        if version == BETA_VERSION {
            return Err(Error::BetaVersion);
        }
        if version < CURRENT_VERSION {
            return Err(Error::OlderVersion);
        }
        if version > CURRENT_VERSION {
            return Err(Error::NewerVersion);
        }
        Ok(version)
    }

    fn derive_shared_secret(&self, their_pk: &PublicKey) -> Result<SymmetricKey> {
        let (alice_pk, bob_pk) = match self.role {
            Role::Alice => (self.our_kp.public(), their_pk),
            Role::Bob => (their_pk, self.our_kp.public()),
        };
        kex(
            SHARED_SECRET_LABEL,
            self.our_kp.secret(),
            their_pk,
            &[&[CURRENT_VERSION], alice_pk.as_ref(), bob_pk.as_ref()],
        )
    }

    fn derive_master_secret(&self, ss: &SymmetricKey) -> SymmetricKey {
        kdf(MASTER_SECRET_LABEL, ss, &[])
    }

    fn derive_confirmation_code(
        &self,
        ss: &SymmetricKey,
        their_pk: &PublicKey,
        target: Role,
    ) -> Mac {
        let confirmation_key = kdf(CONFIRMATION_KEY_LABEL, ss, &[]);
        let our_payload = self.our_payload.to_bytes();
        let their_payload = self.their_payload.to_bytes();
        let inputs = if self.role == target {
            [
                &our_payload,
                self.our_kp.public().as_ref(),
                &their_payload,
                their_pk.as_ref(),
            ]
        } else {
            [
                &their_payload,
                their_pk.as_ref(),
                &our_payload,
                self.our_kp.public().as_ref(),
            ]
        };
        mac(CONFIRMATION_MAC_LABEL, &confirmation_key, &inputs)
    }
}

/// A public key commitment
#[derive(Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Commit(#[serde(with = "array_as_bytes")] [u8; COMMIT_LEN]);

impl AsRef<[u8]> for Commit {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl From<[u8; COMMIT_LEN]> for Commit {
    fn from(buf: [u8; COMMIT_LEN]) -> Self {
        Self(buf)
    }
}

impl Debug for Commit {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}", hex::encode(self.0))
    }
}

impl Commit {
    /// Derives a new public key commitment
    pub fn derive(pk: &PublicKey) -> Self {
        let h = hash(COMMIT_LABEL, &[pk.as_ref()]);
        let mut buf = [0u8; COMMIT_LEN];
        buf.copy_from_slice(&h.as_ref()[..COMMIT_LEN]);
        Self::from(buf)
    }
}

const COMMIT_LEN: usize = 16;
const COMMIT_LABEL: &[u8] = b"org.briarproject.bramble.keyagreement/COMMIT";
const MASTER_SECRET_LABEL: &[u8] = b"org.briarproject.bramble.keyagreement/MASTER_SECRET";
const SHARED_SECRET_LABEL: &[u8] = b"org.briarproject.bramble.keyagreement/SHARED_SECRET";
const CONFIRMATION_KEY_LABEL: &[u8] = b"org.briarproject.bramble.keyagreement/CONFIRMATION_KEY";
const CONFIRMATION_MAC_LABEL: &[u8] = b"org.briarproject.bramble.keyagreement/CONFIRMATION_MAC";

const RECORD_HEADER_LEN: usize = 1 + 1 + 2;
const KEY_RECORD_LEN: usize = RECORD_HEADER_LEN + KEY_LEN;
const CONFIRM_RECORD_LEN: usize = RECORD_HEADER_LEN + MAC_LEN;
const ABORT_RECORD_LEN: usize = RECORD_HEADER_LEN;
const KEY_RECORD_TYPE: u8 = 0;
const CONFIRM_RECORD_TYPE: u8 = 1;
const ABORT_RECORD_TYPE: u8 = 2;

#[cfg(test)]
mod test {
    use super::*;
    use crate::Descriptor;
    use bramble_common::make_duplex;
    use futures::{executor::block_on, try_join};
    use rand::thread_rng;

    #[test]
    fn both_sides_compute_the_same_master_secret() {
        let transport = Descriptor::unknown(89, vec![]);

        let mut rng = thread_rng();
        let kp1 = KeyPair::generate(&mut rng);
        let pk1 = kp1.public();
        let payload1 = Payload::new(&pk1, vec![transport.clone()]);

        let mut rng = thread_rng();
        let kp2 = KeyPair::generate(&mut rng);
        let pk2 = kp2.public();
        let payload2 = Payload::new(&pk2, vec![transport.clone()]);

        let (transport1, transport2) = make_duplex();

        let fut1 = perform_key_agreement(transport1, payload1.clone(), kp1, payload2.clone());
        let fut2 = perform_key_agreement(transport2, payload2, kp2, payload1);

        let ((master_key1, role1), (master_key2, role2)) =
            block_on(async { try_join!(fut1, fut2) }).unwrap();

        assert_eq!(master_key1, master_key2);
        assert_ne!(role1, role2);
    }
}
