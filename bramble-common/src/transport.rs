//! Transport metadata

/// A transport with an identifier
pub trait Id {
    /// This transport's identifier
    const ID: &'static [u8];
}

/// A transport with known maximum latency
pub trait Latency {
    /// This transport's maximum latency in seconds
    fn max_latency(&self) -> u32;
}
