//! BTP tags

use crate::CURRENT_VERSION;
use bramble_crypto::{prf, SymmetricKey};
use std::fmt::{self, Debug};

/// A stream tag
#[derive(PartialEq, Eq, Copy, Clone)]
pub struct Tag([u8; TAG_LEN]);

impl From<[u8; TAG_LEN]> for Tag {
    fn from(bytes: [u8; TAG_LEN]) -> Self {
        Self(bytes)
    }
}

impl AsRef<[u8]> for Tag {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl Debug for Tag {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}", hex::encode(self.0))
    }
}

impl Tag {
    /// Crates a tag for a given stream with a given tag key
    pub fn new(tag_key: &SymmetricKey, stream_number: u64) -> Self {
        let mut message = [0u8; 2 + 8];
        let (version, number) = message.split_at_mut(2);
        version.copy_from_slice(&CURRENT_VERSION.to_be_bytes());
        number.copy_from_slice(&stream_number.to_be_bytes());
        let mut buf = [0u8; TAG_LEN];
        buf.copy_from_slice(&prf(tag_key, &message)[..TAG_LEN]);
        Self(buf)
    }
}

const TAG_LEN: usize = 16;
