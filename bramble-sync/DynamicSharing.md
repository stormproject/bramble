
#How to dynamically determine a message's sharing status
It is desirable for a sync client to be able to dynamically determine what groups/messages to share and what messages to delete in response to changing conditions, particularly: the content of delivered messages, and which peers are online. This is necessary in order to use bramble-sync as part of a platform for arbitrary distributed applications. The various roles within a distributed network will need to be able to precicely determine 

For example, consider a mailbox implementation:
- The mailbox is a device in a group which will receive and send messages as a proxy for another device
- compatible peers will sync messages with the mailbox if and only if the owner has not seen the message and is not online
- The recipient will receive the messages from the mailbox when it is next online
- The mailbox will sync all messages with the owner, but will only sync with other peers when the owner is offline
Requirments for the mailbox
- Peers must sync messages based on what peers are online.
- Peers must sync messages with one peer based on what other peers have seen.

Ideas:
- for each message, send a list of conditions. These conditions will be checked when attempting to share a message
	- Ex. "IF msgA seenBy peerC AND ( peerD online OR peerE offline) THEN share msgB with peerF"
	- This basically means creating a DSL for bramble-sync, which is mega-overkill
- each message SyncState will have a sharing boolean and an expiry group. The client can send an expiry group command, which will indicate that all messages of that group must have their sharing status re-evaluated.
	- ex. for the mailbox, when the owner is disconnected, the client will send an expiry command for all messages shared with the owner but not shared with the mailbox. How can this be expressed? Is it tangibly different from the previous example?

Counterpoint:
Is it even necessary to dynamically share at all? Different roles can still be occupied by different nodes in a distributed system when all nodes have access to the same information. For the mailbox, it would be simpler, though less efficient, to have the owner register the mailbox with its peers saying "send to mailbox everything you would send to me". The mailbox would then receive messages that the owner already has, but it would also not be necessary to evaluate whether the owner is or isn't online and what messages it has or hasn't received. What is the larger cost: complexity with efficiency, or simplicity with inefficiency?

#Methods for creating message SyncStates
The protocol spec states that a message must have a SyncState recorded for each message that a device is sharing a peer. When should this sync state be created?
- Eagerly: Whenever a message is received regardless of whether the peer is online or not:
- Lazily: SyncStates are only created when a the peer is online
These are both the same when a message is received and a peer is online, but when the peer is offline, should the protocol do nothing, or create the SyncState record?

If a sync state is eagerly created, then when a known peer is joined, then only existing sync states need to be referenced to determine what to share. If the states are lazily created

And what about storing the states of messages for peers whose sharing status may change? Must the state be deleted every time the share status is made negative? Or could the spec be interpreted as meaning that a SyncState can be maintained for all peers that a message may be shared with, regardless of whether it is currently shared.

Proposal:
- Every time a peer joins, ask what groups to share
	- Do this instead of maintaining a ShareGroupPeer table
	- accept commands from clients to begin or cease sharing a group with a peer
- For every peer, when it joins, for all the groups shared with it, parse the groups message graph
	- for every message, how to determine whether to share?
		- sharing is irreversible, non-sharing is not: for all non-shared msgs, ask to share, if yes, create SyncState, it will never be deleted (until the message is)
		- sharing and non-sharing are irreversible: store whether a msg is shared or not-shared with each peer and never ask again
	ex: 
		For startup
		for group in groups
			for msg in group:
				for peer in group.sharingPeers:
					if getSyncState(msg, peer) exists:
						sync(msg, peer)
					else if group.client.willShare(msg, peer):
						sync(msg, peer)
		This algo runs in |msgs||peer| time
		OR
		for group in client.getSharingGroups(peer):
			for msg in group:
				if getSyncState(msg, peer) exists:
					sync(msg, peer)
				else if group.client.willShare(msg, peer):
					sync(msg, peer)
		NOTE:
			- The above algorithms are incorrect
			- It is not necessary to check every message; if a messages dependencies are not shared then it will not be shared, so if so, they don't need to be checked.
- For every message in the sharing groups that doesn't have a SyncState already, ask whether to share it.
	- won't this result in a lot of repeated asking for peers that leave and re-enter? Since SyncState is not stored for messages that aren't shared, on every re-connection the protocol will need to re-ask the client whether to share every non-shared message. Solution: don't store a full SyncState, but do store a "sharing" flag.

# Permanent storage schema
Models:
Group - Contain pointers to the dependency-less messages in the graph
Node - Compose the graph, contain pointers to dependents and dependencies
	- Have deleted flag
Message - Contains only the message, dependency and other data is held elsewhere
SyncState - indexed by MsgID and Peer



