//! BDF objects

use crate::{Error, Map, Result};

mod de;
mod ser;

pub use de::from_object;
pub use ser::{to_object, Serializer};

/// A BDF object
#[derive(Debug, PartialEq, Clone)]
pub enum Object {
    /// The null object
    Null,
    /// A boolean
    Boolean(bool),
    /// An integer
    Integer(i64),
    /// A floating-point number
    Float(f64),
    /// A string
    String(std::string::String),
    /// A binary blob
    Raw(Vec<u8>),
    /// A list of BDF objects
    List(Vec<Object>),
    /// A map of strings to BDF objects
    Map(Map),
}

impl Object {
    /// Returns the value of this null object, if it is one.
    pub fn as_null(&self) -> Result<()> {
        match self {
            Object::Null => Ok(()),
            _ => Err(Error::WrongType),
        }
    }

    /// Returns the value of this boolean object, if it is one.
    pub fn as_boolean(&self) -> Result<bool> {
        match self {
            Object::Boolean(b) => Ok(*b),
            _ => Err(Error::WrongType),
        }
    }

    /// Returns the value of this integer object, if it is one.
    pub fn as_integer(&self) -> Result<i64> {
        match self {
            Object::Integer(i) => Ok(*i),
            _ => Err(Error::WrongType),
        }
    }

    /// Returns the value of this float object, if it is one.
    pub fn as_float(&self) -> Result<f64> {
        match self {
            Object::Float(f) => Ok(*f),
            _ => Err(Error::WrongType),
        }
    }

    /// Returns the value of this string object, if it is one.
    pub fn as_string(&self) -> Result<&str> {
        match self {
            Object::String(s) => Ok(s),
            _ => Err(Error::WrongType),
        }
    }

    /// Returns the value of this raw object, if it is one.
    pub fn as_raw(&self) -> Result<&[u8]> {
        match self {
            Object::Raw(r) => Ok(r),
            _ => Err(Error::WrongType),
        }
    }

    /// Returns the value of this list object, if it is one.
    pub fn as_list(&self) -> Result<&[Object]> {
        match self {
            Object::List(l) => Ok(l),
            _ => Err(Error::WrongType),
        }
    }

    /// Returns the value of this map object, if it is one.
    pub fn as_map(&self) -> Result<&Map> {
        match self {
            Object::Map(d) => Ok(d),
            _ => Err(Error::WrongType),
        }
    }

    /// Consumes this null object and returns its value, if it is one.
    pub fn to_null(self) -> Result<()> {
        self.as_null()
    }

    /// Consumes this boolean object and returns its value, if it is one.
    pub fn to_boolean(self) -> Result<bool> {
        self.as_boolean()
    }

    /// Consumes this integer object and returns its value, if it is one.
    pub fn to_integer(self) -> Result<i64> {
        self.as_integer()
    }

    /// Consumes this float object and returns its value, if it is one.
    pub fn to_float(self) -> Result<f64> {
        self.as_float()
    }

    /// Consumes this string object and returns its value, if it is one.
    pub fn to_string(self) -> Result<String> {
        match self {
            Object::String(s) => Ok(s),
            _ => Err(Error::WrongType),
        }
    }

    /// Consumes this raw object and returns its value, if it is one.
    pub fn to_raw(self) -> Result<Vec<u8>> {
        match self {
            Object::Raw(r) => Ok(r),
            _ => Err(Error::WrongType),
        }
    }

    /// Consumes this list object and returns its value, if it is one.
    pub fn to_list(self) -> Result<Vec<Object>> {
        match self {
            Object::List(l) => Ok(l),
            _ => Err(Error::WrongType),
        }
    }

    /// Consumes this map object and returns its value, if it is one.
    pub fn to_map(self) -> Result<Map> {
        match self {
            Object::Map(d) => Ok(d),
            _ => Err(Error::WrongType),
        }
    }
}
