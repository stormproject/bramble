//! BRP protocol implementation

use crate::{Rendezvous, Result, CURRENT_VERSION};
use bramble_common::{sleep, timeout};
use bramble_crypto::{dh, hash, kdf, KeyPair, PublicKey, Role, SymmetricKey};
use futures::{select, FutureExt};
use std::time::Duration;

/// Perform the rendezvous over the given transport
pub async fn perform_rendezvous<T>(
    transport: T,
    our_kp: KeyPair,
    their_pk: PublicKey,
) -> Result<(T::Connection, SymmetricKey, Role)>
where
    T: Rendezvous,
{
    let our_bytes = our_kp.public().as_ref();
    let their_bytes = their_pk.as_ref();
    let role = if our_bytes < their_bytes {
        Role::Alice
    } else {
        Role::Bob
    };
    let protocol = Protocol {
        transport,
        role,
        our_kp,
        their_pk,
    };
    let (conn, key) = protocol.perform().await?;
    Ok((conn, key, role))
}

struct Protocol<T>
where
    T: Rendezvous,
{
    transport: T,
    role: Role,
    our_kp: KeyPair,
    their_pk: PublicKey,
}

impl<T> Protocol<T>
where
    T: Rendezvous,
{
    pub async fn perform(mut self) -> Result<(T::Connection, SymmetricKey)> {
        let raw_secret = dh(self.our_kp.secret(), &self.their_pk);
        let (pk_a, pk_b) = if self.role == Role::Alice {
            (self.our_kp.public(), &self.their_pk)
        } else {
            (&self.their_pk, self.our_kp.public())
        };

        let static_master_key = SymmetricKey::from(hash(
            STATIC_MASTER_KEY_LABEL,
            &[raw_secret.as_ref(), pk_a.as_ref(), pk_b.as_ref()],
        ));
        let rendezvous_key = kdf(
            RENDEZVOUS_KEY_LABEL,
            &static_master_key,
            &[&CURRENT_VERSION.to_be_bytes()],
        );
        let stream_key = kdf(STREAM_KEY_LABEL, &rendezvous_key, &[T::ID]);
        self.transport.prepare_endpoints(stream_key, self.role);
        let listen_fut = self.transport.listen();
        let connect_fut = timeout(MAX_CONNECTION_ATTEMPT_TIME, async {
            loop {
                if let Ok(conn) = self.transport.connect().await {
                    break Ok(conn);
                }
                sleep(CONNECTION_ATTEMPT_INTERVAL).await;
            }
        });
        let conn = async {
            select! {
                c = listen_fut.fuse() => c,
                c = connect_fut.fuse() => c,
            }
        }
        .await?;
        Ok((conn, static_master_key))
    }
}

const STATIC_MASTER_KEY_LABEL: &[u8] = b"org.briarproject.bramble.transport/STATIC_MASTER_KEY";
const RENDEZVOUS_KEY_LABEL: &[u8] = b"org.briarproject.bramble.transport/RENDEZVOUS_KEY";
const STREAM_KEY_LABEL: &[u8] = b"org.briarproject.bramble.transport/KEY_MATERIAL";
const CONNECTION_ATTEMPT_INTERVAL: Duration = Duration::from_secs(60);
const MAX_CONNECTION_ATTEMPT_TIME: Duration = Duration::from_secs(48 * 60 * 60);
