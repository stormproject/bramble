//! Bramble rendezvvous protocol, version 0
#![warn(missing_docs)]

mod error;
mod protocol;
mod transport;

pub use error::{Error, Result};
pub use protocol::perform_rendezvous;
pub use transport::Rendezvous;

const CURRENT_VERSION: u8 = 0;
