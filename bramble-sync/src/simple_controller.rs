use crate::simple_client::*;
use crate::sync::*;
use bramble_crypto::Hash as ID;
use bramble_crypto::PublicKey;
use futures::select;
use futures::FutureExt;
use futures::StreamExt;
use futures_timer::Delay;
use log::trace;
use std::collections::VecDeque;
use std::pin::Pin;
use std::time::Duration;

pub struct SimpleController {
    name: String,
    file: String,
    client: SyncClient<SimpleClient>,
    sync: SyncProtocol,
    messages: VecDeque<SimpleClientItem>,
}

impl SimpleController {
    pub fn new(
        name: &str,
        file: &str,
        // device: KeyPair,
        conns: Vec<(PublicKey, Pin<Box<dyn SyncConnection>>, String)>,
    ) -> Self {
        let mut builder = SyncProtocolBuilder::new(file);
        let client = builder.register_client(SimpleClient::new());
        let sync = builder.base_send_time(4.0).build(&name, conns);
        Self {
            name: name.into(),
            file: file.into(),
            client,
            sync,
            messages: VecDeque::new(),
        }
    }
    pub fn pop_message(&mut self) -> Option<crate::simple_client::ClientMessage> {
        self.messages
            .pop_front()
            .map(|msg| extract_variant!(msg, SimpleClientItem::Message))
    }
    pub fn get_name(&self) -> String {
        self.name.clone()
    }
    pub fn get_file(&self) -> String {
        self.file.clone()
    }
    pub async fn run_for_millis(&mut self, millis: u64) {
        // self.sync.set_until_terminated();
        select! {
            _ = Delay::new(Duration::from_millis(millis)).fuse() => {},
            _ = async {

                loop {

                    select! {
                        _ = self.client.next().fuse() => {}
                        _ = self.sync.next().fuse() => {}
                    }
                }
            }.fuse() => {}
        }
    }
    pub async fn run_to_exit(&mut self) {
        // self.run_to_inactive().await
        loop {
            select!(
            // _ = collect_client(&mut self.client, &mut self.messages).fuse() => {
            // mut res = self.client.collect::<VecDeque<SimpleClientItem>>().fuse() => {
            //     // let items: VecDeque<SimpleClientItem> = res.into();
            //     // self.messages.append(&mut items);
            //     self.messages.append(&mut res);
            //     trace!("Client exited");
            // }
            // _ = self.sync.collect::<Vec<ControllerNotification>>().fuse() => {
            //     trace!("SyncProtocol exited");
            // }
            _ = self.sync.next().fuse() => {
                break;
            }
            _ = self.client.next().fuse() => {
            })
        }
        // );
        // println!("Exiting {}", self.name);
    }
    async fn push_client_output(&mut self) -> bool {
        let next = self.client.next().await;
        let done = next.is_none();
        if let Some(item) = next {
            self.messages.push_back(item);
        }
        done
    }
    async fn collect_client(&mut self) {
        let mut done = false;
        while !done {
            done = self.push_client_output().await;
        }
    }
    // pub fn has_active_writers(&self) -> bool {
    //     self.sync.has_active_writers()
    // }

    //pub fn client_is_active(&self) -> bool {
    //self.client.is_active()
    //}
    // pub async fn run_to_inactive(&mut self) {
    //     self.sync.set_while_active();
    //     let mut active = true;
    //     while active {
    //         select!(
    //             _ = self.sync.next().fuse() => {
    //                 trace!(target: "controller","{} SyncProtocol inactive", self.name);
    //                 active = false;
    //             }
    //             _ = self.client.next().fuse() => {
    //                 trace!(target: "controller","{} Client output to controller", self.name);
    //             }
    //         )
    //     }
    // }
    pub fn sync_ref(&mut self) -> &mut SyncProtocol {
        &mut self.sync
    }
    pub fn client_ref(&mut self) -> &mut SyncClient<SimpleClient> {
        &mut self.client
    }
    pub async fn insert_message(
        &mut self,
        group: ID,
        body: MessageBody,
    ) -> crate::simple_client::ClientMessage {
        self.send_message(group, body);
        loop {
            select!(
                _ = self.sync.next().fuse() => {
                    trace!(target: "controller","SyncProtocol inactive");
                }
                output = self.client.next().fuse() => {
                    trace!(target: "controller","Client output to controller");
                    if let SimpleClientItem::Message(msg) =  output.unwrap() {
                        return msg;
                    }
                }
            )
        }
    }
    pub async fn run_to_output(&mut self) -> ControllerInput {
        // self.sync.set_until_terminated();
        select!(
            _ = self.sync.next().fuse() => {
                trace!(target: "controller","{} SyncProtocol inactive", self.name);
                ControllerInput::Exited
            }
            output = self.client.next().fuse() => {
                trace!(target: "controller","{} Client output to controller", self.name);
                output.unwrap()
            }
        )
    }
    pub fn done(&mut self) {
        self.client.done()
    }
    pub fn terminate(&mut self) {
        self.sync.terminate()
    }
    pub fn send_message(&mut self, group: ID, body: MessageBody) {
        self.client
            .send_message(group, serde_json::to_string(&body).unwrap());
    }
    pub fn send_message_json(&mut self, group: ID, body: String) {
        self.client.send_message(group, body);
    }
    pub fn add_group(&mut self, desc: String) -> SimpleClientGroup {
        let group = SimpleClientGroup::new(&desc);
        self.client.add_group(group.clone());
        group
    }
    pub fn add_peer_to_group(&mut self, peer: &PublicKey, group: &ID) {
        self.client.add_peer_to_group(peer, group);
    }
    pub fn add_peer(&mut self, peer: &PublicKey, conn: Pin<Box<dyn SyncConnection>>, name: String) {
        self.sync.add_conn((*peer, conn, name));
    }
    pub fn remove_peer_from_group(&mut self, peer: &PublicKey, group: &ID) {
        self.client.remove_peer_from_group(peer, group);
    }
}

// async fn collect_client(client: &mut SimpleClient, messages: &mut VecDeque<SimpleClientItem>) {
//     while let Some(item) = client.next().await {
//         messages.push_back(item);
//     }
// }

type ControllerInput = crate::simple_client::SimpleClientItem;
