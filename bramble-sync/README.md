# Rust Implementation of Bramble Synchronization Protocol

I wanted to develop an app based on the Briar stack, but the entire project is written in Java and is not organized or documented in a way that encourages extension. I have therefore decided to rewrite the project in Rust. This package is an implementation of the BSP component of the Briar stack.

## Description
Describe What BSP is and why its useful.

## Roadmap
What are the steps for working on BSP? For the whole STORM?

1) Implement Sync connection loop
	- loop that establishes connections over the provided transport
	- async reads over that connection, and the provided clients
	- takes an action when a connection message is received, then re-enters loop
	- takes an action when a new client message is received, then re-enters loop
2) Implement wire protocol
3) Implement a storage layer
4) implement a simple client
5) Implement the controller
	- implement the TCP-over-Tor connection that transport must use
	- implement a simple frontent with FRB, that uses multithreading

## Concerns

What should be the read/write interface between sync and the clients, and the clients and the frontend?

1) Default interface should be single-threaded, any threading is part of controller abstraction
- so for flutter_rust_bridge, it creates a thread to handle initialization and run sync and the clients
- subsequent FRB-generated threads just comminicate with the main thread, which will then fulfill a request
- will need to make custom FRB handler, so that the means of thread communication are passed into each thread
- in controller thread, need an async loop that AsyncReads from the consumer part of the channel
	1. Sync running its read loop
	2. Controller polls consumer end of channel
	3. pause sync loop, controller give new msg to correct client (either sync or async, not sure)
	4. Client executes internal logic, then publishes the message for Sync
	5. Controller re-enters async channel read
	6. Sync AsyncReads the client at next poll
	7. Sync processes new message, the re-enter connection read loop


The controller abstracts the entire platform from the frontend, though the frontend must still be able to handle displaying messages specific to each client. When the app starts, the frontend initializes the controller, which starts the Sync thread, passing the clients to it. The second thread is Sync, and handles reading from and writing to each connection, and storage.

What other ways of handling this problem are there?

2) frontend communicated directly with clients, not abstracted by controller

## Authors and acknowledgment
Thanks to R. Martinho Fernandes for implementing the rest of the Bramble protocols

## License
GNU

