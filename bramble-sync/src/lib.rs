#![feature(future_join)]
#![feature(pin_deref_mut)]
#![feature(array_chunks)]
#![feature(mutex_unlock)]
#![feature(drain_filter)]
#![feature(hash_drain_filter)]
#![feature(type_alias_impl_trait)]
#![feature(let_chains)]
#![feature(iter_collect_into)]
#![feature(core_panic)]
#[allow(unused_macros)]
#[macro_use]
mod misc;
#[allow(dead_code)]
#[macro_use]
pub mod sync;
#[allow(dead_code)]
#[macro_use]
pub mod storage;
extern crate slice_as_array;
#[allow(dead_code)]
pub mod simple_client;
#[allow(dead_code)]
mod simple_controller;
mod statements;
#[macro_use]
#[allow(dead_code)]
#[allow(unused_imports)]
pub mod test;
