use crate::simple_client::*;
use crate::storage::*;
use crate::sync::*;
use bramble_crypto::Hash as ID;
use bramble_crypto::*;
use futures::channel::mpsc::{UnboundedReceiver, UnboundedSender};
use futures::channel::oneshot::channel;
use futures::executor;
use futures::StreamExt;
use rand::thread_rng;
use std::path::Path;
use std::sync::Arc;

pub fn key() -> KeyPair {
    KeyPair::generate(&mut thread_rng())
}
pub fn init() {
    let _ = env_logger::builder().is_test(true).try_init();
}
pub struct TestParams {
    pub sender: UnboundedSender<StorageOperation>,
    pub handle: std::thread::JoinHandle<()>,
}
impl TestParams {
    pub fn new(
        (sender, handle): (
            UnboundedSender<StorageOperation>,
            std::thread::JoinHandle<()>,
        ),
    ) -> Self {
        Self { sender, handle }
    }
}
pub fn group(name: &str) -> Arc<Group> {
    Arc::new(Group::new(name.as_bytes(), SimpleClient::get_id()))
}
pub fn assert_group_exists(id: ID, file: &str) {
    let mut p = storage(file);
    get_group(&mut p, id).unwrap();
}
pub fn storage(file: &str) -> TestParams {
    TestParams::new(SqliteSyncStorage::init(
        SqliteSyncStorage::new(file, false).unwrap(),
    ))
}
pub fn assert_sync_state_seen(p: &mut TestParams, peer: &PublicKey, msg: &ID, seen: bool) {
    assert_eq!(sync_state_is_seen(p, peer, msg), seen);
}
pub fn sync_state_is_seen(p: &mut TestParams, peer: &PublicKey, msg: &ID) -> bool {
    match fetch_sync_state(p, &(*peer, *msg)) {
        Some(state) => state.seen,
        None => false,
    }
}
pub fn get_group(p: &mut TestParams, id: ID) -> Option<Arc<Group>> {
    test_storage_op!(p, GetGroup, id)
}
pub fn fetch_sync_state(p: &mut TestParams, key: &SyncStateKey) -> Option<SyncState> {
    test_storage_op!(p, GetSyncState, key.1, key.0)
}
pub fn test_file(name: String) -> String {
    format!("./tests/{}.db", name)
}

// pub fn exec_operation(p: &mut TestParams, input: StorageOperation) -> SyncTypes {
//     let (send, recv) = channel();
//     p.sender.unbounded_send(input).unwrap();
//     executor::block_on(p.receiver.next()).unwrap().data.unwrap()
// }
pub fn storage_params(file: &str) -> TestParams {
    TestParams::new(SqliteSyncStorage::init(
        SqliteSyncStorage::new(file, false).unwrap(),
    ))
}
pub fn wipe(file: &str) -> String {
    let testpath = Path::new("./tests/");
    if !testpath.exists() {
        let _ = std::fs::create_dir(testpath);
    }
    if Path::new(file).exists() {
        println!("removing file {}", file);
        std::fs::remove_file(file).unwrap();
    }
    assert!(!std::path::Path::new(file).exists());
    file.to_string()
}
pub fn message(from_opt: Option<PublicKey>, text: &str, deps: Vec<ID>) -> MessageBody {
    let from = match from_opt {
        Some(peer) => peer.to_string(),
        None => peer(0).to_string(),
    };

    MessageBody {
        from,
        text: text.to_string(),
        dependencies: deps,
    }
}
pub fn peer(num: u8) -> PublicKey {
    [num; KEY_LEN].into()
}
